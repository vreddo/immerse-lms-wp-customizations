This plugin requires the following plugins to be previously installed and activated:

advanced-custom-fields-pro
bbpress
boss-learndash
bp-remove-profile-links-master
bp-xprofile-wp-user-sync
buddyboss-media
buddyboss-one-click
buddyboss-updater
buddyboss-wall
buddypress
buddypress-global-search
buddypress-learndash
ld-visual-customizer
learndash-notes
learndash-notifications
learndash-propanel
learndash_woocommerce
learndash_zapier
sfwd-lms
uncanny-learndash-toolkit
uncanny-toolkit-pro
user-role-editor