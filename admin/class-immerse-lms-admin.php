<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       pedrops.com
 * @since      1.0.0
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/admin
 * @author     Pedro Germani <pedro.germani@toptal.com>
 */
class Immerse_Lms_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->options = get_option($this->plugin_name . '_options');

		add_shortcode( 'class_content', array($this, 'class_content_shortcode') );
		add_shortcode( 'consolidated_quiz_report', array($this, 'consolidated_quiz_report') );
		

		if (!$this->options['immerse_lms_field_enable_vr']) return;
		else {
			/**
			 * The class responsible for defining all actions that occur in the admin area.
			 */
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-immerse-lms-cpt.php';
		}
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Immerse_Lms_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Immerse_Lms_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/immerse-lms-admin.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Immerse_Lms_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Immerse_Lms_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/immerse-lms-admin.js', array( 'jquery' ), $this->version, false );
		//wp_enqueue_script( $this->plugin_name."_vreddo_content", plugin_dir_url( __FILE__ ) . 'js/immerse-lms-vreddo-content.js', array( 'jquery' ), $this->version, false );
	}

	/*
	* Get user classes. Used initially by boss-learndash/learndash templates
	*/
	public function immerse_lms_get_user_classes($user_id, $course_id){
		
		//$classes = get_field('course_class',  $course_id);
		
		$args = array(
			'post_type' => 'course-class',
			'meta_query'=> array(
				'relation' => 'AND',
				array(
					'key' => 'course_class',
					'value' => sprintf(':"%s";', $course_id),
					'compare' => 'LIKE'
				),
				array(
					'key' => 'class_students',
					'value' => sprintf(':"%s";', $user_id),
					'compare' => 'LIKE'
				),
			)
		);
		$classes = new WP_Query($args);
		
		$user_classes = array();
		
		// TODO: remove nested loops:
		if ( $classes ) {
			foreach ($classes->posts as $key => $class) {
				$students = get_field('class_students', $class->ID);
				foreach ($students as $key => $student) {
					if ($user_id == $student['ID']) {
						$user_classes[] = $class;	
					}
				}
			}
		} 
		return $user_classes;
	}

	/*
	* Filter students field only by users enrolled on the Course related to Class
	*/
	function immerse_lms_class_students_filter( $args, $field, $post_id ) {
		$course = get_field('class_course', $post_id);

		if ($course) {
			$course_id = $course[0]->ID;
			
			$course_users_query = learndash_get_users_for_course($course_id, array(), false);
			$user_ids = $course_users_query->get_results();
			
			$args['include'] = $user_ids;
		}
		
		// return
		return $args;
	}

	/*
	* Adds Class Content shortcode
	*/
	function class_content_shortcode( $atts ,  $content = "") {
		// get shortcode class
		$class_id = $atts['course_class'];
		$args = array('p' => $class_id, 'post_type' => 'course-class');
		$classes = new WP_Query($args);
		$classes = $classes->posts;
		$class = $classes[0];

		// get current user
		$user = wp_get_current_user();

		// get Class Students ID list
		$class_users = get_field('class_students', $class_id);
		foreach ($class_users as $class_user) {
			$class_user_ids[] = $class_user['ID'];
		}
		
		// verifies if user is in class
		if (in_array($user->ID, $class_user_ids))
			return "{$content}";
		else return null;
	}

	/**
	 * Adds consolidated quiz export shortcode.
	 */
	function consolidated_quiz_report() {
		$users = get_users(array(
			'fields' => array('ID')
		));
		
		$users = array_map(function($user) {
			return $user->ID;
		}, $users);
		
		return '
		<div class="consolidated-quiz-report-form">
			<button type="button" class="button">Export</button><br /><br />
			<div style="height: 8px; background-color: white; border-radius: 10px;" class="progress-bar-container">
				<div style="width: 50%; height: 8px; background-color: green; border-radius: 10px;" class="progress-bar"></div>
			</div>
			<div class="progress-text"></div>
			<script>
				var ALL_STUDENTS = ' . json_encode($users) . ';
			</script>
		</div>
		';
	}

	/*
	* Add class information to User profile
	*/

	function extra_user_profile_fields( $user ) { ?>
	<h3>
		<?php _e("Course Classes", "blank"); ?>
	</h3>
	<?php 
			$args = array(
				'post_type' => 'course-class',
				'meta_query'=> array(
					array(
						'key' => 'class_students',
						'value' => sprintf(':"%s";', $user->ID),
						'compare' => 'LIKE'
					),
				)
			);
			$classes = new WP_Query($args);

			if ($classes->posts) {
				foreach ($classes->posts as $key => $class) {
					
					
					echo get_field('class_public_title', $class->ID) . ' | ' . get_field('vr_class_datetime_start', $class->ID);;
					if (end($classes) != $class ) echo '<br/>';
				}
			}
		
	} 
	
	// create Classes options menu
	public function immerse_lms_class_options_create_menu() {
		// create new top-level menu
		add_menu_page( 
			__( 'Immerse LMS', 'textdomain' ),
			'Immerse LMS',
			'manage_options',
			$this->plugin_name,
			array($this, 'immerse_lms_settings_page'),
			plugin_dir_url( dirname( __FILE__ ) ) . 'admin/images/immerse-lms-icon-small.png',
			2
		); 
		
		if (!$this->options['immerse_lms_field_enable_vr']) return;

		// global $submenu;
		// unset($submenu['edit.php?post_type=course-class'][10]);
		
		add_menu_page( 
			__( 'VReddo', 'textdomain' ),
			'VReddo',
			'manage_options',
			'vreddo',
			array($this, 'immerse_lms_class_vreddo_main'),
			plugin_dir_url( dirname( __FILE__ ) ) . 'admin/images/vr-icon.png',
			3
		); 
		
		add_submenu_page(
			'vreddo',
			__( 'VR Classes', 'textdomain' ),
			__( 'VR Classes', 'textdomain' ),
			'manage_options',
			'/edit.php?post_type=course-class'
		);

		add_submenu_page(
			'vreddo',
			__( 'Library', 'textdomain' ),
			__( 'Library', 'textdomain' ),
			'manage_options',
			'vreddo_library',
			array($this, 'immerse_lms_class_vreddo_content')
		);

		add_submenu_page(
			'vreddo',
			__( 'User Content', 'textdomain' ),
			__( 'User Content', 'textdomain' ),
			'manage_options',
			'/edit.php?post_type=vr_user_content'
		);

		
		
		// add_submenu_page(
		// 	'vreddo',
		// 	__( 'Settings', 'textdomain' ),
		// 	__( 'Settings', 'textdomain' ),
		// 	'manage_options',
		// 	'vr-class-options',
		// 	array($this, 'immerse_lms_class_options_settings_page')
		// );
			
		
	}
	
	public function immerse_lms_vr_class_menu_redirect () {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}
		wp_redirect(site_url());
	}
		
	public function register_immerse_lms_settings() {
		//register our settings
		register_setting( $this->plugin_name, $this->plugin_name.'_options', array( $this, 'immerse_lms_validate_settings' ) );

		// register a new section in the plugin page
		add_settings_section(
			'immerse_lms_section_settings',
			__( 'VR', $this->plugin_name ),
			array( $this ,'immerse_lms_section_settings_cb'),
			$this->plugin_name
		);

		// register a new field in the "immerse_lms_section_settings" section, inside the plugin page
		add_settings_field(
			'immerse_lms_field_enable_vr',
			__( 'Enable VR CMS', $this->plugin_name ),
			array( $this ,'immerse_lms_field_enable_vr_cb'),
			$this->plugin_name,
			'immerse_lms_section_settings',
			[
				'label_for' => 'immerse_lms_field_enable_vr',
				'class' => 'immerse_lms_row',
				'immerse_lms_custom_data' => 'custom',
			]
		);

		// register a new field in the "immerse_lms_section_notification_settings" section, inside the plugin page
		add_settings_field(
			'immerse_lms_field_vreddo_store_url',
			__( 'VReddo Store URL', $this->plugin_name ),
			array( $this ,'immerse_lms_field_vreddo_store_url_cb'),
			$this->plugin_name,
			'immerse_lms_section_settings',
			[
				'label_for' => 'immerse_lms_field_vreddo_store_url',
				'class' => 'immerse_lms_row',
				'immerse_lms_custom_data' => 'custom',
			]
		);

		// register a new section in the plugin page
		add_settings_section(
			'immerse_lms_section_notification_settings',
			__( 'Online Notification Settings', $this->plugin_name ),
			array( $this ,'immerse_lms_section_settings_cb'),
			$this->plugin_name
		);

		// register a new field in the "immerse_lms_section_notification_settings" section, inside the plugin page
		add_settings_field(
			'immerse_lms_field_enable_online_notification',
			__( 'Enable Online User Notification', $this->plugin_name ),
			array( $this ,'immerse_lms_field_enable_online_notification_cb'),
			$this->plugin_name,
			'immerse_lms_section_notification_settings',
			[
				'label_for' => 'immerse_lms_field_enable_online_notification',
				'class' => 'immerse_lms_row',
				'immerse_lms_custom_data' => 'custom',
			]
		);

		// register a new field in the "immerse_lms_section_notification_settings" section, inside the plugin page
		add_settings_field(
			'immerse_lms_field_online_notification',
			__( 'Online User Notification', $this->plugin_name ),
			array( $this ,'immerse_lms_field_online_notification_cb'),
			$this->plugin_name,
			'immerse_lms_section_notification_settings',
			[
				'label_for' => 'immerse_lms_field_online_notification',
				'class' => 'immerse_lms_row',
				'immerse_lms_custom_data' => 'custom',
			]
		);

		// register a new field in the "immerse_lms_section_notification_settings" section, inside the plugin page
		add_settings_field(
			'immerse_lms_field_online_diploma_notification',
			__( 'Online User Notification (Diploma)', $this->plugin_name ),
			array( $this ,'immerse_lms_field_online_diploma_notification_cb'),
			$this->plugin_name,
			'immerse_lms_section_notification_settings',
			[
				'label_for' => 'immerse_lms_field_online_diploma_notification',
				'class' => 'immerse_lms_row',
				'immerse_lms_custom_data' => 'custom',
			]
		);

		// register a new field in the "immerse_lms_section_notification_settings" section, inside the plugin page
		add_settings_field(
			'immerse_lms_field_tutor_online_notification',
			__( 'Online Tutor Notification', $this->plugin_name ),
			array( $this ,'immerse_lms_field_tutor_online_notification_cb'),
			$this->plugin_name,
			'immerse_lms_section_notification_settings',
			[
				'label_for' => 'immerse_lms_field_tutor_online_notification',
				'class' => 'immerse_lms_row',
				'immerse_lms_custom_data' => 'custom',
			]
		);
	}

	/**
	 * Developer Callback (see https://developer.wordpress.org/plugins/settings/custom-settings-page/)
	 *
	 * @since    1.0.0
	 */
	function immerse_lms_section_settings_cb( $args ) {
		// enqueue style and js for admin settings page
		// wp_enqueue_style( 'wp-color-picker' );
		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/breaking-news-toptal-admin.css', array(), $this->version, 'all' );
		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/breaking-news-toptal-admin.js', array( 'jquery', 'wp-color-picker' ), $this->version, false );	
		?>
	<p id="<?php echo esc_attr( $args['id'] ); ?>">
		<?php esc_html_e( 'Edit your settings and save.', $this->plugin_name ); ?>
	</p>
	<?php
	}

	/**
	 * Callback to print field Title.
	 *
	 * @since    1.0.0
	 */
	function immerse_lms_field_enable_vr_cb( $args ) {
		// output the field
		$enable_vr = $this->options[esc_attr($args['label_for'])];
		if($enable_vr){ 
			$checked = "checked=\"checked\""; }
		else{ $checked = "";}
		?>
		<input type="checkbox" id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['immerse_lms_custom_data'] ); ?>"
		    name="<?php echo $this->plugin_name.'_options[' . esc_attr($args['label_for']). ']' ?>" value=1 class="" <?php echo $checked;
		    ?> />

		<p class="description">
			<?php esc_html_e( 'Enables Immerse LMS VR features', $this->plugin_name ); ?>
		</p>
		<?php
	}

	/**
	 * Callback to print field Title.
	 *
	 * @since    1.0.0
	 */
	function immerse_lms_field_vreddo_store_url_cb( $args ) {
		// output the field
		$message = $this->options[esc_attr($args['label_for'])];
		?>
		<input type="name" id="<?= esc_attr($args['label_for']) ?>" value="<?= esc_attr($message) ?>" name="<?php echo $this->plugin_name.'_options[' . esc_attr($args['label_for']). ']' ?>" />
		<p class="description">
			<?php esc_html_e( 'VReddo Website to use (developer mode) ', $this->plugin_name ); ?>
		</p>
		<?php
	}

	/**
	 * Callback to print field Title.
	 *
	 * @since    1.0.0
	 */
	function immerse_lms_field_enable_online_notification_cb( $args ) {
		// output the field
		$enabled = $this->options[esc_attr($args['label_for'])];
		if($enabled){ 
			$checked = "checked=\"checked\""; }
		else{ $checked = "";}
		?>
		<input type="checkbox" id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['immerse_lms_custom_data'] ); ?>"
		    name="<?php echo $this->plugin_name.'_options[' . esc_attr($args['label_for']). ']' ?>" class="" <?php echo $checked;
		    ?> />

		<p class="description">
			<?php esc_html_e( 'Enables the notification for when a user comes online', $this->plugin_name ); ?>
		</p>
		<?php
	}

	/**
	 * Callback to print field Title.
	 *
	 * @since    1.0.0
	 */
	function immerse_lms_field_online_notification_cb( $args ) {
		// output the field
		$message = $this->options[esc_attr($args['label_for'])];
		?>
		<input type="name" id="<?= esc_attr($args['label_for']) ?>" value="<?= esc_attr($message) ?>" name="<?php echo $this->plugin_name.'_options[' . esc_attr($args['label_for']). ']' ?>" />
		<p class="description">
			<?php esc_html_e( 'Notification for when a user comes online for the first time (Use {name} as a placeholder)', $this->plugin_name ); ?>
		</p>
		<?php
	}

	/**
	 * Callback to print field Title.
	 *
	 * @since    1.0.0
	 */
	function immerse_lms_field_online_diploma_notification_cb( $args ) {
		// output the field
		$message = $this->options[esc_attr($args['label_for'])];
		?>
		<input type="name" id="<?= esc_attr($args['label_for']) ?>" value="<?= esc_attr($message) ?>" name="<?php echo $this->plugin_name.'_options[' . esc_attr($args['label_for']). ']' ?>" />
		<p class="description">
			<?php esc_html_e( 'Notification for when a user enrolled to a diploma course comes online for the first time (Use {name} as a placeholder)', $this->plugin_name ); ?>
		</p>
		<?php
	}

	/**
	 * Callback to print field Title.
	 *
	 * @since    1.0.0
	 */
	function immerse_lms_field_tutor_online_notification_cb( $args ) {
		// output the field
		$message = $this->options[esc_attr($args['label_for'])];
		?>
		<input type="name" id="<?= esc_attr($args['label_for']) ?>" value="<?= esc_attr($message) ?>" name="<?php echo $this->plugin_name.'_options[' . esc_attr($args['label_for']). ']' ?>" />
		<p class="description">
			<?php esc_html_e( 'Notification displayed to a user when they first log on, signifying that a tutor is online (Use {name} as a placeholder)', $this->plugin_name ); ?>
		</p>
		<?php
	}

	public function immerse_lms_class_options_settings_page () {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-classes-settings.php';	
	}	

	public function immerse_lms_class_vreddo_main () {
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-vreddo-main-display.php';	
	}

	public function immerse_lms_class_vreddo_content () {
		
		$args = array(
			'post_type' => 'attachment',
			'post_status' => 'inherit', 
			'meta_query'=> array(
				array(
					'key' => '_is_vr_content',
					'value' => '1',
					'compare' => '='
				),
				
			)
		);

		$vr_content = new WP_Query($args);

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-vreddo-content-display.php';	
	}

	/**
	 * Prints plugin options page
	 *
	 * @since    1.0.0
	 */
	public function immerse_lms_settings_page () {
		// check user capabilities
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		}

		// add error/update messages
		// check if the user have submitted the settings
		// wordpress will add the "settings-updated" $_GET parameter to the url
		if ( isset( $_GET['settings-updated'] ) ) {
			// add settings saved message with the class of "updated"
			add_settings_error( 'immerse_lms_messages', 'immerse_lms_message', __( 'Settings Saved', $this->plugin_name ), 'updated' );
		}
		
		// show error/update messages		
		settings_errors( 'immerse_lms_messages' );	
		
		// include settings page html
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-settings.php';	
		
		?>

			<?php
	}

	/**
	 * Callback to validate plugin settings.
	 *
	 * @since    1.0.0
	 */
	public function immerse_lms_validate_settings( $fields ) { 
		
		$valid_fields = array();
		
		// Validate Title Field
		$enable_vr = $fields['immerse_lms_field_enable_vr'];
		if (empty($enable_vr)) {
			// Set the error message
			add_settings_error( 'immerse_lms_messages', 'immerse_lms_message', __('Attention: VR Features Disabled!', $this->plugin_name), 'notice' ); // $setting, $code, $message, $type
			// Get previous value
			$valid_fields['immerse_lms_field_enable_vr'] = false;
		} else {
			$valid_fields['immerse_lms_field_enable_vr'] = (bool) $enable_vr;
		}

		if ($fields['immerse_lms_field_vreddo_store_url'] != '') {
			$vreddo_url = esc_url($fields['immerse_lms_field_vreddo_store_url']);
			if ($vreddo_url) {
				$valid_fields['immerse_lms_field_vreddo_store_url'] = $vreddo_url;
			}
		}
		else {
			$valid_fields['immerse_lms_field_vreddo_store_url'] = 'https://staging.vreddo.com';
			add_settings_error( 'immerse_lms_messages', 'immerse_lms_message', __('VReddo Store URL set to default', $this->plugin_name), 'notice' );
		}
		
		$valid_fields['immerse_lms_field_online_notification'] = $fields['immerse_lms_field_online_notification'];
		$valid_fields['immerse_lms_field_online_diploma_notification'] = $fields['immerse_lms_field_online_diploma_notification'];
		$valid_fields['immerse_lms_field_tutor_online_notification'] = $fields['immerse_lms_field_tutor_online_notification'];

		return apply_filters( 'immerse_lms_validate_settings', $valid_fields, $fields);
	}
	

	// public function immerse_lms_add_submenu_ld ($addsubmenu) {
	// 	remove_menu_page('edit.php?post_type=course-class');
 	// 	return $addsubmenu;
	// }

	public function get_course_classes_details ($course_id = 0, $user_id = 0) {
		$args = array(
			'post_type' => 'course-class',
			'meta_key' => 'vr_class_datetime_start',
			'orderby' => array (
				'meta_value' => 'ASC',
			)
		);
		$classes = new WP_Query($args);

		if ($classes->posts) {
			foreach ($classes->posts as $key => $class) {
				$course = get_field('course_class', $class->ID);

				$classes_list[$key] = array(
					'ID' => $class->ID,
					'title' => get_the_title($class->ID),
					'status' => get_post_status($class->ID), 
					'public_title' => get_field('class_public_title', $class->ID),
					'date_time' => get_field('vr_class_datetime_start', $class->ID),
					'course_id' => $course[0]->ID,
					'course_title' => $course[0]->post_title,

				);
			}
		}
		return $classes_list;
	}

	/**
	 * Add Course - VR Content meta box
	 *
	 * @param post $post The post object
	 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/add_meta_boxes
	 */
	public function course_add_meta_boxes( $post ){
		add_meta_box( 'vr_content_meta_box', 'Custom VR Content', array($this, 'vr_content_build_meta_box'), 'sfwd-courses' );
	}
	

	/**
	 * Build custom field meta box
	 *
	 * @param post $post The post object
	 */
	public function vr_content_build_meta_box( $post ){
		$class_args = array(
			'post_type' 	=> 'course-class',
			'fields'		=> 'ids',
			'meta_query'	=> array(
				'relation' => 'AND',
					array(
						'key' => 'course_class',
						'value' => sprintf(':"%s";', $post->ID),
						'compare' => 'LIKE'
					),
			)
		);
		$classes = new WP_Query($class_args);
		$classes_ids = $classes->posts;
		if ($classes) {
			foreach ($classes_ids as $class_id) {
				$vr_content_class_num[$class_id] = (int) get_post_meta($class_id, 'vr_content', ARRAY_A);
			}
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-course-vr-content-metabox.php';

		}

		$lessons = learndash_get_course_lessons_list( $post->ID );

		if ($lessons) { 
			foreach ($lessons as $lesson) {
				$lesson_id = $lesson['post']->ID;
				$vr_content_lesson_num[$lesson_id] = (int) get_post_meta($lesson_id, 'vr_content', ARRAY_A);
				$lesson_topics[$lesson_id] = learndash_get_topic_list( $lesson_id, $post->ID );
			}
			foreach ($lesson_topics as $lesson_key => $lesson_topics) {
				foreach ($lesson_topics as $topic) {
					$vr_content_topic_num = get_post_meta($topic->ID, 'vr_content');
					if ( $vr_content_topic_num[0] > 0 ) {
						$vr_content_topic_number[$lesson_key][$topic->ID] = $vr_content_topic_num[0];
					}
				}
			}
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-course-vr-content-metabox-lessons.php';
		}
		
	}

	public function immerse_lms_marker_students_field($user) {
		if (!user_can($user->ID, 'marker')) {
			return;
		}

		$selected_count = get_the_author_meta( 'marker_students_count', $user->ID );
		$selected = array();

		if ($selected_count) {
			for ($i = 0; $i < $selected_count; $i++) {
				$selected[] = get_the_author_meta( 'marker_students_' . $i, $user->ID );
			}
		}

		?>
		<h3>Students this user can grade</h3>
		<div class="immerse_lms_marker_students immerselms-binary-selector" data="{}" data-selected="<?= implode(',', $selected) ?>">
			<input type="hidden" name="marker_students" class="selectedinput" />
			<table class="immerselms-binary-selector-table">
				<tbody>
					<tr>
						<td class="immerselms-binary-selector-section immerselms-binary-selector-section-left">
							<input placeholder="Search All Students" type="text" class="immerselms-binary-selector-search immerselms-binary-selector-search-left">
							<select multiple="multiple" class="immerselms-binary-selector-items immerselms-binary-selector-items-left">
								<?php
								$users = get_users(array(
									'role' => 'subscriber'
								));

								foreach ($users as $user) {
									?>
									<option class="immerselms-binary-selector-item" value="<?= $user->ID ?>"><?= $user->display_name ?></option>
									<?php
								}
								?>
							</select>
						</td>
						<td class="immerselms-binary-selector-section immerselms-binary-selector-section-middle">
							<a href="#" class="immerselms-binary-selector-button-add"><img src="//redmako-dev.localhost.com/wp-content/plugins/sfwd-lms/assets/images/arrow_right.png"></a><br>
							<a href="#" class="immerselms-binary-selector-button-remove"><img src="//redmako-dev.localhost.com/wp-content/plugins/sfwd-lms/assets/images/arrow_left.png"></a>
						</td>
						<td class="immerselms-binary-selector-section immerselms-binary-selector-section-right">
							<input placeholder="Search Graded Students" type="text" class="immerselms-binary-selector-search immerselms-binary-selector-search-right">
							<select multiple="multiple" class="immerselms-binary-selector-items immerselms-binary-selector-items-right"></select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<?php
	}

	public function immerse_lms_save_marker_students_field($user_id) {
		global $wpdb;

		if (!current_user_can('edit_user')) {
			return;
		}

		$wpdb->query(
			$wpdb->prepare(
				"
				DELETE FROM
					$wpdb->usermeta
				WHERE
					user_id = %d AND
					meta_key LIKE 'marker_students_%'
				",
				$user_id
			)
		);

		$marker_students = $_POST['marker_students'] ? explode(',', $_POST['marker_students']) : array();
		update_user_meta( $user_id, 'marker_students_count', count($marker_students) );

		foreach ($marker_students as $i => $student) {
			update_user_meta( $user_id, 'marker_students_' . $i, $student );
		}
	}

	/**
	 * Add Timezone field on Xprofile fields
	 *
	 */
	public function bp_add_custom_xprofile_timezone_list() {
 
		if ( !xprofile_get_field_id_from_name('Timezone') ) {

			$timezone_list_args = array(
				'field_group_id'  => 1,
				'name'            => 'Timezone',
				'description'	 => 'Please select your timezone',
				'can_delete'      => true,
				'field_order' 	 => 2,
				'is_required'     => false,
				'type'            => 'selectbox',
				'order_by'	 => 'custom'
			);

			$timezone_list_id = xprofile_insert_field( $timezone_list_args );

			if ( $timezone_list_id ) {
				$utc = new DateTimeZone('UTC');
				$dt = new DateTime('now', $utc);
				$timezones = \DateTimeZone::listIdentifiers();
				  
				foreach (  $timezones as $tz ) {
					$current_tz = new \DateTimeZone($tz);
					$transition = $current_tz->getTransitions($dt->getTimestamp(), $dt->getTimestamp());
					$abbr = $transition[0]['abbr'];

					xprofile_insert_field( array(
						'field_group_id'	=> 1,
						'parent_id'			=> $timezone_list_id,
						'type'				=> 'option',
						'name'				=> $tz .' ('. $abbr . ')',
						'option_order'   	=> $i++
					));
					  
				}
	   
			}
		}
	}

	/**
	 * Add HeadsetId field on Xprofile fields
	 *
	 */
	public function bp_add_custom_xprofile_headset_id() {
 
		if ( !xprofile_get_field_id_from_name('VR Headset ID') ) {

			$headset_args = array(
				'field_group_id'  => 1,
				'name'            => 'VR Headset ID',
				'description'	 => '',
				'can_delete'      => true,
				'field_order' 	 => 2,
				'is_required'     => false,
				'type'            => 'textbox',
				'order_by'	 => 'custom'
			);
			xprofile_insert_field( $headset_args);
		}
	}
	
	/**
	 * Add initial bp activity so new users can be searchable since it's registered.
	 */
	public function immerse_lms_user_registration_save ($user_id) {
		if ( !function_exists( 'bp_activity_add' ) ) return false;

		$args = array(
			'user_id' => $user_id,
			'action' => 'Immerse LMS - New User Registered',
			'component' => 'members',
			'type' => 'last_activity'
		);

		$activity_id = bp_activity_add( $args );
		$s = 1;
	}

	/**
	 * Send the unsent email notifications for late courses.
	 */
	public function immerse_lms_send_late_course_notifications() {
		error_log('Late course notification check...');

		// Retrieve all courses.
		$courses = get_posts(array(
			'post_type' => 'sfwd-courses',
			'numberposts' => -1
		));

		// Iterate through all courses.
		foreach ($courses as $course) {
			// Get the deadlines.
			$deadlines = get_field('student_deadlines', $course);

			foreach ($deadlines as $deadline) {
				try
				{
					$student = get_user_by('ID', $deadline['student']['ID']);
					$tutor = get_field('assigned_tutor', $course);
					$deadline = strtotime($deadline['deadline']);
					$deadline_string = date(get_option('date_format'), $deadline);
					$deadline_plus_six_string = date(get_option('date_format'), $deadline + (6 * 24 * 60 * 60));
					$already_sent = get_user_meta($student->ID, '_course_' . $course->ID . '_sent_notification', true);

					$course_hierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $course->ID, $student->ID);
					$incomplete = false;
					
					foreach ($course_hierarchy as $lesson) {
						foreach ($lesson['children'] as $child) {
							if ($child->post_type == 'sfwd-quiz') {
								$statistic_ref = apply_filters('immerse_lms_get_last_statistic_ref_id', $student->ID, apply_filters('immerse_lms_get_quiz_pro_id', $child->ID));

								if (!$statistic_ref) {
									$incomplete = true;
								}
							}
						}
					}

					if ($incomplete) {
						if (!$already_sent) {
							$already_sent = array(false, false, false);
						}

						// Check the difference.
						$difference = time() - $deadline;

						// Set up the email data.
						$email_data = array(
							'user_name' => $student->display_name,
							'deadline' => $deadline_string,
							'deadline_plus_six' => $deadline_plus_six_string,
							'tutor_phone_number' => get_user_meta($tutor['ID'], 'billing_phone', true),
							'course_title' => $course->post_title
						);

						// If it's larger than -7 days (i.e. past 7 days before the due date)
						if ($difference > -7 * 24 * 60 * 60 && !$already_sent[0]) {
							// Send the first reminder to the student.
							Immerse_Lms_Email::send_email($student->user_email, 'course_upcoming_deadline', $email_data);
							
							echo 'Sent first reminder to ' . $student->display_name . ' (' . $student->user_email . ')<br />';
							$already_sent[0] = true;
						}

						// If it's larger than 2 days (i.e. past 2 days after the due date)
						if ($difference > 2 * 24 * 60 * 60 && !$already_sent[1]) {
							// Send the second reminder to the student.
							Immerse_Lms_Email::send_email($student->user_email, 'course_passed_deadline', $email_data);

							echo 'Sent second reminder to ' . $student->display_name . ' (' . $student->user_email . ')<br />';
							$already_sent[1] = true;
						}

						// If it's larger than 7 days (i.e. past 7 days after the due date)
						if ($difference > 7 * 24 * 60 * 60 && !$already_sent[2]) {
							// Send the third reminder to the school and parents.
							$school_email = get_field('school_email', 'user_' . $student->ID);
							$parent_email = get_field('parent_email', 'user_' . $student->ID);

							if ($school_email) {
								Immerse_Lms_Email::send_email($school_email, 'course_passed_deadline_school', $email_data);
								echo 'Sent third reminder to ' . $student->display_name . '\'s school.<br />';
							}

							if ($parent_email) {
								Immerse_Lms_Email::send_email($parent_email, 'course_passed_deadline_parent', $email_data);
								echo 'Sent third reminder to ' . $student->display_name . '\'s parent.<br />';
							}

							$already_sent[2] = true;
						}
					}

					update_user_meta($student->ID, '_course_' . $course->ID . '_sent_notification', $already_sent);
				} catch (\Exception $e) {
					error_log('Fail for user ' . $deadline['student']['ID'] . ': ' . $e->getMessage());
				}
			}
		}
	}

	/**
	 * Redirects away from the "Submitted Essays" screen to the assessment
	 * grading list instead.
	 */
	public function immerse_lms_redirect_from_essays_screen($screen) {
		global $wpdb;

		// Redirect submitted essays grid page.
		if ($screen && $screen->base == 'edit' && $screen->post_type == 'sfwd-essays') {
			header('Location: /submitted-quizzes/');
			die();
		}

		// Redirect single essay edit page.
		if ($screen && $screen->base == 'post' && $screen->id == 'sfwd-essays') {
			$post_id = $_GET['post'];
			$wpdb->statistic = $wpdb->prefix . 'wp_pro_quiz_statistic';

			// Get the statistic ref ID.
			$result = $wpdb->get_col(
				$wpdb->prepare("
					SELECT
						statistic_ref_id
					FROM
						$wpdb->statistic
					WHERE
						answer_data = %s
					",
					'{"graded_id":' . $post_id . '}'
				)
			);

			if (count($result) > 0) {
				$statistic_ref_id = $result[0];

				// Get the assessment grading sheet for this statistic ref ID.
				$assessment_grading = get_posts(array(
					'numberposts' => 1,
					'post_status' => 'any',
					'meta_key' => '_statistic-ref-id',
					'meta_value' => $statistic_ref_id,
					'post_type' => 'assessment_grading'
				));

				if (count($assessment_grading)) {
					$assessment_grading = $assessment_grading[0];

					// Redirect to that grading sheet.
					header('Location: /assessment-grading-sheet/?gid=' . $assessment_grading->ID);
					die();
				}
			}
		}
	}

	public function immerse_lms_run_script() {
		if (isset($_GET['script'])) {
			$file = __DIR__ . '/../scripts/script-immerse-lms-' . $_GET['script'] . '.php';

			if (file_exists($file)) {
				require_once($file);
			}
		}

		die();
	}

	public function immerse_lms_upload_mimes($mime_types) {
		$mime_types['obj'] = 'text/plain';
		$mime_types['mtl'] = 'text/plain';

		return $mime_types;
	}

	public function immerse_lms_validate_vr_video($errors, $file, $attachment, $field) {
		if ($file['type'] === 'mp4' && $file['width'] === 0 && $file['height'] === 0) {
			// We need to get the width and height for the file being uploaded.
			$result = exec('ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of csv=p=0 ' . $attachment['tmp_name']);
			$dimensions = explode(',', $result);

			if (count($dimensions) == 2 && is_numeric($dimensions[0]) && is_numeric($dimensions[1])) {
				$file['width'] = $dimensions[0];
				$file['height'] = $dimensions[1];
			}
		}

		if ($field['name'] == 'vr_content_file_video') {
			// 2D videos can have a max width of 1920.
			if ($file['width'] > 1920) {
				$errors[] = 'Max horizontal resolution for 360 videos is 1920.';
			}
		}

		if ($field['name'] == 'vr_content_file_video_360') {
			// 3D videos can have a max width of 4096.
			if ($file['width'] > 4096) {
				$errors[] = 'Max horizontal resolution for 360 videos is 4096.';
			}

			if ($file['width'] / $file['height'] != 2/1) {
				$errors[] = '360 videos must have a 2:1 aspect ratio.';
			}
		}

		return $errors;
	}

	public function immerse_lms_filter_parent_course_dropdown($args) {
		if ($args['post_type'] == 'sfwd-courses') {
			$args['depth'] = 1;
		}

		return $args;
	}

}

