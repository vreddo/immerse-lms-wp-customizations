<?php
/*
* Register Course Class Custom Post Type
*/
if ( ! function_exists('rm_courses_class_cpt') ) {

	// Register Custom Post Type
	function rm_courses_class_cpt() {
	
		$labels = array(
			'name'                  => _x( 'VR Classes', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'VR Class', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'VR Classes', 'text_domain' ),
			'name_admin_bar'        => __( 'VR Classes', 'text_domain' ),
			'archives'              => __( 'VR Class Archives', 'text_domain' ),
			'attributes'            => __( 'VR Class Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent VR Class:', 'text_domain' ),
			'all_items'             => __( 'VR Classes', 'text_domain' ),
			'add_new_item'          => __( 'Add New VR Class', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New VR Class', 'text_domain' ),
			'edit_item'             => __( 'Edit VR Class', 'text_domain' ),
			'update_item'           => __( 'Update VR Class', 'text_domain' ),
			'view_item'             => __( 'View VR Class', 'text_domain' ),
			'view_items'            => __( 'View VR Classes', 'text_domain' ),
			'search_items'          => __( 'Search VR Class', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'VR Classes list', 'text_domain' ),
			'items_list_navigation' => __( 'VR Classes list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'VR Classes', 'text_domain' ),
			'description'           => __( 'Courses Classes', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'page-attributes' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => false,
			'menu_position'         => 2,
			'menu_icon'           => plugin_dir_url( dirname( __FILE__ ) ) . 'admin/images/vr-icon.png',
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => false,
		);
		register_post_type( 'course-class', $args );
	
	}
	add_action( 'init', 'rm_courses_class_cpt', 0 );

}

/*
* Register VR User Content Custom Post Type
*/
if ( ! function_exists('rm_vr_user_content') ) {

	// Register Custom Post Type
	function rm_vr_user_content() {
	
		$labels = array(
			'name'                  => _x( 'VR User Contents', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'VR User Content', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'VR User Content', 'text_domain' ),
			'name_admin_bar'        => __( 'VR User Content', 'text_domain' ),
			'archives'              => __( 'VR User Content Archives', 'text_domain' ),
			'attributes'            => __( 'VR User Content Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent VR User Content:', 'text_domain' ),
			'all_items'             => __( 'All VR User Contents', 'text_domain' ),
			'add_new_item'          => __( 'Add New VR User Content', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New VR User Content', 'text_domain' ),
			'edit_item'             => __( 'Edit VR User Content', 'text_domain' ),
			'update_item'           => __( 'Update VR User Content', 'text_domain' ),
			'view_item'             => __( 'View VR User Content', 'text_domain' ),
			'view_items'            => __( 'View VR User Contents', 'text_domain' ),
			'search_items'          => __( 'Search VR User Content', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'VR User Contents list', 'text_domain' ),
			'items_list_navigation' => __( 'VR User Contents list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'VR User Content', 'text_domain' ),
			'description'           => __( 'User notes, audio and screenshots.', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'author' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => false,
			'menu_position'         => 100,
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => false,
			'can_export'            => false,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capabilities' => array(
				'edit_post' => 'edit_vr_user_content',
				'edit_posts' => 'edit_vr_user_contents',
				'edit_others_posts' => 'edit_other_vr_user_contents',
				'publish_posts' => 'publish_vr_user_contents',
				'read_post' => 'read_vr_user_content',
				'read_private_posts' => 'read_private_vr_user_contents',
				'delete_post' => 'delete_vr_user_content',
				'delete_others' => 'delete_vr_user_content'
			),
		);
		register_post_type( 'vr_user_content', $args );
	
	}
	add_action( 'init', 'rm_vr_user_content', 0 );
	
}

/*
* Register Email Templates CPT
*/
if ( ! function_exists('rm_email_template_cpt') ) {

	// Register Custom Post Type
	function rm_email_template_cpt() {
	
		$labels = array(
			'name'                  => _x( 'Email Templates', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Email Template', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Email Templates', 'text_domain' ),
			'name_admin_bar'        => __( 'Email Templates', 'text_domain' ),
			'archives'              => __( 'Email Template Archives', 'text_domain' ),
			'attributes'            => __( 'Email Template Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Email Template:', 'text_domain' ),
			'all_items'             => __( 'Email Templates', 'text_domain' ),
			'add_new_item'          => __( 'Add New Email Template', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Email Template', 'text_domain' ),
			'edit_item'             => __( 'Edit Email Template', 'text_domain' ),
			'update_item'           => __( 'Update Email Template', 'text_domain' ),
			'view_item'             => __( 'View Email Template', 'text_domain' ),
			'view_items'            => __( 'View Email Templates', 'text_domain' ),
			'search_items'          => __( 'Search Email Template', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Email Templates list', 'text_domain' ),
			'items_list_navigation' => __( 'Email Templates list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Email Templates', 'text_domain' ),
			'description'           => __( 'Email Templates', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 2,
			'menu_icon'           => 'dashicons-email-alt',
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => false,
		);
		register_post_type( 'rm-email-template', $args );
	
	}
	add_action( 'init', 'rm_email_template_cpt', 0 );

	// Disable creating new templates. Templates are auto-generated if they don't already exist.
	function rm_email_template_disable_new() {
		// Hide sidebar link
		global $submenu;
		unset($submenu['edit.php?post_type=rm-email-template'][10]);
		
		// Hide link on listing page
		if (isset($_GET['post_type']) && $_GET['post_type'] == 'rm-email-template') {
			echo '<style type="text/css">
				.page-title-action, .row-actions { display:none; }
			</style>';
		}
	}

	add_action('admin_menu', 'rm_email_template_disable_new');

	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array (
			'key' => 'group_5b7a1b6341f96',
			'title' => 'Email Template Fields',
			'fields' => array (
				array (
					'key' => 'field_5b7a1b75add5b',
					'label' => 'Email Subject',
					'name' => 'email_subject',
					'type' => 'text',
					'instructions' => '',
					'required' => 1,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'rm-email-template',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'acf_after_title',
			'style' => 'seamless',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
		
	endif;
}

add_action('admin_menu', 'et_add_pages');

function et_add_pages() {

    add_submenu_page('edit.php?post_type=rm-email-template', __('Header Settings','text_domain'), __('Header Settings','text_domain'), 'manage_options', 'etheadersettings', 

'et_header_settings_page');

    add_action( 'admin_init', 'et_header_settings_page_options' );

}

function et_header_settings_page_options() {
	register_setting( 'et_header_settings_page_options_group', 'et_hs_from_to_email' );
	register_setting( 'et_header_settings_page_options_group', 'et_hs_from_to_name' );
}

function et_header_settings_page() { ?>

<div class="wrap">
	<h1>Header Settings</h1>

	<form method="post" action="options.php">
	    <?php settings_fields( 'et_header_settings_page_options_group' ); ?>
	    <?php do_settings_sections( 'et_header_settings_page_options_group' ); ?>
	    <table class="form-table">
	        <tr valign="top">
		        <th scope="row">Reply To Email:</th>
		        <td>
		        	<input type="email" name="et_hs_from_to_email" value="<?php echo sanitize_email( get_option('et_hs_from_to_email') ); ?>">
		        </td>
	        </tr>
	        <tr valign="top">
		        <th scope="row">Reply To Name:</th>
		        <td>
		        	<input type="text" name="et_hs_from_to_name" value="<?php echo sanitize_text_field( get_option('et_hs_from_to_name') ); ?>">
		        </td>
	        </tr>
	    </table>
	    
	    <?php submit_button(); ?>

	</form>
</div>

<?php

}

/*
* Register Marking Guides CPT
*/
if ( ! function_exists('rm_marking_guide_cpt') ) {

	// Register Custom Post Type
	function rm_marking_guide_cpt() {
	
		$labels = array(
			'name'                  => _x( 'Marking Guides', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Marking Guide', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Marking Guides', 'text_domain' ),
			'name_admin_bar'        => __( 'Marking Guides', 'text_domain' ),
			'archives'              => __( 'Marking Guide Archives', 'text_domain' ),
			'attributes'            => __( 'Marking Guide Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Marking Guide:', 'text_domain' ),
			'all_items'             => __( 'Marking Guides', 'text_domain' ),
			'add_new_item'          => __( 'Add New Marking Guide', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Marking Guide', 'text_domain' ),
			'edit_item'             => __( 'Edit Marking Guide', 'text_domain' ),
			'update_item'           => __( 'Update Marking Guide', 'text_domain' ),
			'view_item'             => __( 'View Marking Guide', 'text_domain' ),
			'view_items'            => __( 'View Marking Guides', 'text_domain' ),
			'search_items'          => __( 'Search Marking Guide', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Marking Guides list', 'text_domain' ),
			'items_list_navigation' => __( 'Marking Guides list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Marking Guides', 'text_domain' ),
			'description'           => __( 'Courses Classes', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 2,
			'menu_icon'           => 'dashicons-editor-justify',
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
			'show_in_rest'          => false,
		);
		register_post_type( 'marking-guide', $args );
	
	}
	add_action( 'init', 'rm_marking_guide_cpt', 0 );

}

if ( !function_exists('rm_marking_guide_quiz_picker') ) {

	function rm_marking_guide_quiz_picker() {
		$page_type = 'edit';
		$screen = get_current_screen();

		if ($screen->post_type != 'marking-guide') {
			return;
		}
		
		if ( $screen->action == 'add' ) {
			$page_type = 'create';
		}

		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-marking-guide-quiz-picker.php';
	}
	add_action('edit_form_after_title', 'rm_marking_guide_quiz_picker');

}

if ( !function_exists('rm_marking_guide_edit_content') ) {

	function rm_marking_guide_edit_content() {
		$screen = get_current_screen();

		if ($screen->post_type != 'marking-guide') {
			return;
		}
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-marking-guide-edit.php';
	}
	add_action('edit_form_after_editor', 'rm_marking_guide_edit_content');

}

if ( !function_exists('rm_marking_guide_save_post') ) {
	
	function rm_marking_guide_post_updated($post_id, $post_after, $post_before) {
		$post_type = get_post_type($post_id);

		if ($post_type != 'marking-guide') {
			return;
		}

		if (isset($_POST['course']))
			update_post_meta($post_id, '_course', $_POST['course']);
		
		if (isset($_POST['quiz']))
			update_post_meta($post_id, '_quiz', $_POST['quiz']);

		if (isset($_POST['_question_guides']) && is_array($_POST['_question_guides'])) {
			foreach ($_POST['_question_guides'] as $question_id => $content) {
				update_post_meta($post_id, $question_id, $content);
			}
		}
	}
	add_action('post_updated', 'rm_marking_guide_post_updated', 10, 3);

}

/*
* Register Assessment Grading custom post type
*/
if ( ! function_exists('rm_assessment_grading_cpt') ) {

	// Register Custom Post Type
	function rm_assessment_grading_cpt() {
	
		$labels = array(
			'name'                  => _x( 'Assessment Gradings', 'Post Type General Name', 'text_domain' ),
			'singular_name'         => _x( 'Assessment Grading', 'Post Type Singular Name', 'text_domain' ),
			'menu_name'             => __( 'Assessment Gradings', 'text_domain' ),
			'name_admin_bar'        => __( 'Assessment Gradings', 'text_domain' ),
			'archives'              => __( 'Assessment Grading Archives', 'text_domain' ),
			'attributes'            => __( 'Assessment Grading Attributes', 'text_domain' ),
			'parent_item_colon'     => __( 'Parent Assessment Grading:', 'text_domain' ),
			'all_items'             => __( 'Assessment Gradings', 'text_domain' ),
			'add_new_item'          => __( 'Add New Assessment Grading', 'text_domain' ),
			'add_new'               => __( 'Add New', 'text_domain' ),
			'new_item'              => __( 'New Assessment Grading', 'text_domain' ),
			'edit_item'             => __( 'Edit Assessment Grading', 'text_domain' ),
			'update_item'           => __( 'Update Assessment Grading', 'text_domain' ),
			'view_item'             => __( 'View Assessment Grading', 'text_domain' ),
			'view_items'            => __( 'View VR Assessment Gradings', 'text_domain' ),
			'search_items'          => __( 'Search Assessment Grading', 'text_domain' ),
			'not_found'             => __( 'Not found', 'text_domain' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
			'featured_image'        => __( 'Featured Image', 'text_domain' ),
			'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
			'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
			'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
			'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
			'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
			'items_list'            => __( 'Assessment Gradings list', 'text_domain' ),
			'items_list_navigation' => __( 'Assessment Gradings list navigation', 'text_domain' ),
			'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
		);
		$args = array(
			'label'                 => __( 'Assessment Gradings', 'text_domain' ),
			'description'           => __( 'Gradings for completed assessments', 'text_domain' ),
			'labels'                => $labels,
			'supports'              => array( 'page-attributes' ),
			'hierarchical'          => true,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => false,
			'menu_position'         => 2,
			'menu_icon'           => plugin_dir_url( dirname( __FILE__ ) ) . 'admin/images/vr-icon.png',
			'show_in_admin_bar'     => false,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type' 		=> 'post',
			'capabilities' => array(
			  'create_posts' => 'do_not_allow',
			),
			'map_meta_cap' 			=> true,
			'show_in_rest'          => false,
		);
		register_post_type( 'assessment_grading', $args );
	}
	add_action( 'init', 'rm_assessment_grading_cpt', 0 );
}

/*
* Adds course and course-classes CPT Custom fields
*/

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'class_fields',
		'title' => 'Class Configuration',
		'fields' => array (
			array (
				'key' => 'class_public_title',
				'label' => 'Public Title',
				'name' => 'class_public_title',
				'type' => 'text',
				'instructions' => 'This name will be shown to users',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'class_trainer',
				'label' => 'Trainer',
				'name' => 'class_trainer',
				'type' => 'user',
				'instructions' => 'Select Trainer for this Class. Only Users of role "Trainer" are shown.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'role' => array (
					0 => 'trainer',
				),
				'allow_null' => 0,
				'multiple' => 0,
			),
			array (
				'key' => 'vr_class_datetime_start',
				'label' => 'Date / Time',
				'name' => 'vr_class_datetime_start',
				'type' => 'date_time_picker',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'display_format' => 'd/m/Y g:i a',
				'return_format' => 'Y-m-d g:i a',
				'first_day' => 1,
			),
			array (
				'key' => 'class_timezone',
				'label' => 'Timezone',
				'name' => 'timezone',
				'type' => 'timezone_picker',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_time_zone' => 'Australia/Sydney',
			),
			array (
				'key' => 'vr_class_repeat',
				'label' => 'Repeat Class?',
				'name' => 'vr_class_repeat',
				'type' => 'true_false',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'message' => '',
				'default_value' => 0,
				'ui' => 0,
				'ui_on_text' => '',
				'ui_off_text' => '',
			),
			array (
				'key' => 'vr_class_repeat_weekdays',
				'label' => 'Weekday Repetition',
				'name' => 'vr_class_repeat_weekdays',
				'type' => 'checkbox',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'vr_class_repeat',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					1 => 'Mon',
					2 => 'Tue',
					3 => 'Wed',
					4 => 'Thu',
					5 => 'Fri',
					6 => 'Sat',
					0 => 'Sun',
				),
				'allow_custom' => 0,
				'save_custom' => 0,
				'default_value' => array (
				),
				'layout' => 'horizontal',
				'toggle' => 0,
				'return_format' => 'value',
			),
			array (
				'key' => 'vr_class_datetime_end',
				'label' => 'Last Date',
				'name' => 'vr_class_datetime_end',
				'type' => 'date_picker',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'vr_class_repeat',
							'operator' => '==',
							'value' => '1',
						),
					),
				),
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'display_format' => 'd/m/Y',
				'return_format' => 'Y-m-d',
				'first_day' => 1,
			),
			array (
				'key' => 'class_course',
				'label' => 'Course',
				'name' => 'course_class',
				'type' => 'relationship',
				'instructions' => 'Select related Course',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'sfwd-courses',
				),
				'taxonomy' => array (
				),
				'filters' => array (
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy',
				),
				'elements' => '',
				'min' => 1,
				'max' => 1,
				'return_format' => 'object',
			),	
			array (
				'key' => 'class_students',
				'label' => 'Students',
				'name' => 'class_students',
				'type' => 'user',
				'instructions' => 'Please select Course and save this Class before selecting Students. Only Students enrolled to the Course will be shown here.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'role' => '',
				'allow_null' => 0,
				'multiple' => 1,
			),
			array(
				'key' => 'class_room_id',
				'label' => 'Room ID',
				'name' => 'class_room_id',
				'type' => 'number',
				'required' => 0
			)
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'course-class',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'acf_after_title',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

	if( function_exists('acf_add_local_field_group') ):

		acf_add_local_field_group(array (
			'key' => 'vr_contents',
			'title' => 'VR Content',
			'fields' => array (
				array (
					'key' => 'vr_content',
					'label' => 'VR content',
					'name' => 'vr_content',
					'type' => 'repeater',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'collapsed' => '',
					'min' => 0,
					'max' => 0,
					'layout' => 'block',
					'button_label' => 'Add VR Content',
					'sub_fields' => array (
						array (
							'key' => 'vr_content_description',
							'label' => 'Description',
							'name' => 'vr_content_description',
							'type' => 'text',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'maxlength' => '',
						),
						array(
							'key' => 'vr_content_file_thumbnail',
							'label' => 'Thumbnail',
							'name' => 'vr_content_file_thumbnail',
							'type' => 'image',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'slideshow'
									),
								)
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => 2048,
							'max_height' => '',
							'max_size' => 12,
							'mime_types' => '',
						),
						array(
							'key' => 'vr_content_file_image',
							'label' => 'File',
							'name' => 'vr_content_file_image',
							'type' => 'image',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'image'
									),
									array(
										'field' => 'vr_imagevideo_projection',
										'operator' => '==',
										'value' => 'flat'
									)
								)
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => 2048,
							'max_height' => '',
							'max_size' => 12,
							'mime_types' => '',
						),
						array(
							'key' => 'vr_content_file_image_360',
							'label' => 'File',
							'name' => 'vr_content_file_image_360',
							'type' => 'image',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'image'
									),
									array(
										'field' => 'vr_imagevideo_projection',
										'operator' => '==',
										'value' => '360'
									)
								)
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'preview_size' => 'thumbnail',
							'library' => 'all',
							'min_width' => '',
							'min_height' => '',
							'min_size' => '',
							'max_width' => 4096,
							'max_height' => '',
							'max_size' => 12,
							'mime_types' => '',
							'ratio_width' => 2,
							'ratio_height' => 1,
							'ratio_margin' => 0,
						),
						array(
							'key' => 'vr_content_file_model',
							'label' => 'File',
							'name' => 'vr_content_file_model',
							'type' => 'file',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'hologram'
									)
								)
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'library' => 'uploadedTo',
							'min_size' => '',
							'max_size' => 2,
							'mime_types' => 'obj',
						),
						array(
							'key' => 'vr_content_file_material',
							'label' => 'Material',
							'name' => 'vr_content_file_material',
							'type' => 'file',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'hologram'
									)
								)
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'library' => 'uploadedTo',
							'min_size' => '',
							'max_size' => 2,
							'mime_types' => 'mtl',
						),
						array(
							'key' => 'vr_content_file_video',
							'label' => 'File',
							'name' => 'vr_content_file_video',
							'type' => 'file',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'video'
									),
									array(
										'field' => 'vr_imagevideo_projection',
										'operator' => '==',
										'value' => 'flat'
									)
								)
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'library' => 'all',
							'min_size' => '',
							'max_size' => 400,
							'mime_types' => 'mp4',
						),
						array(
							'key' => 'vr_content_file_video_360',
							'label' => 'File',
							'name' => 'vr_content_file_video_360',
							'type' => 'file',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'video'
									),
									array(
										'field' => 'vr_imagevideo_projection',
										'operator' => '==',
										'value' => '360'
									)
								)
							),
							'wrapper' => array(
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'return_format' => 'array',
							'library' => 'all',
							'min_size' => '',
							'max_size' => 400,
							'mime_types' => 'mp4',
						),
						array (
							'key' => 'type',
							'label' => 'Type',
							'name' => 'type',
							'type' => 'radio',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => 0,
							'wrapper' => array (
								'width' => '',
								'class' => '',
								'id' => '',
							),
							'choices' => array (
								'Hologram' => 'Hologram',
								'Image' => 'Image',
								'Video' => 'Video',
								'Slideshow' => 'Slideshow',
							),
							'allow_null' => 0,
							'other_choice' => 0,
							'save_other_choice' => 0,
							'default_value' => '',
							'layout' => 'vertical',
							'return_format' => 'value',
						),
						array (
							'key' => 'vr_imagevideo_projection',
							'label' => 'Projection',
							'name' => 'projection',
							'type' => 'select',
							'instructions' => '',
							'required' => 1,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'Image'
									)
								),
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'Video'
									)
								)
							),
							'choices' => array(
								'flat' => 'Flat',
								'360' => '360'
							)
						),
						array (
							'key' => 'vr_slideshow_slides',
							'label' => 'Slides',
							'name' => 'slides',
							'type' => 'repeater',
							'instructions' => '',
							'required' => 0,
							'conditional_logic' => array(
								array(
									array(
										'field' => 'type',
										'operator' => '==',
										'value' => 'Slideshow'
									)
								)
							),
							'min' => 1,
							'max' => 0,
							'layout' => 'table',
							'button_label' => '',
							'sub_fields' => array(
								array(
									'key' => 'vr_slideshow_slide_file',
									'label' => 'File',
									'name' => 'file',
									'type' => 'image',
									'instructions' => '',
									'required' => 1,
									'conditional_logic' => 0,
									'return_format' => 'array',
									'library' => 'all',
									'min_size' => '',
									'max_size' => '',
									'mime_types' => ''
								)
							)
						),
						array(
							'key' => 'vr_content_id',
							'type' => 'text',
							'name' => 'vr_content_id',
							'required' => 0
						),
					),
				),
				array(
					'key' => 'vr_content_row_id',
					'type' => 'text',
					'name' => 'vr_content_row_id',
					'required' => 0
				),
			),
			'location' => array (
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sfwd-lessons',
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'sfwd-topic',
					),
				),
				array (
					array (
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'course-class',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
		
	endif;

	acf_add_local_field_group(array (
		'key' => 'course_classes',
		'title' => 'Course Classes',
		'fields' => array (
			array (
				'key' => 'course_class',
				'label' => 'Related Classes',
				'name' => 'course_class',
				'type' => 'relationship',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'course-class',
				),
				'taxonomy' => array (
				),
				'filters' => array (
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy',
				),
				'elements' => '',
				'min' => '',
				'max' => '',
				'return_format' => 'object',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-courses',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

endif;

/*
* Adds VR User Content CPT Fields
*/

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'vr_user_contents',
		'title' => 'VR User Content fields',
		'fields' => array (
			array (
				'key' => 'file',
				'label' => 'File',
				'name' => 'file',
				'type' => 'file',
				'instructions' => '',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'return_format' => 'url',
				'library' => 'all',
				'min_size' => '',
				'max_size' => '',
				'mime_types' => '',
			),
			array (
				'key' => 'vr_content_type',
				'label' => 'Type',
				'name' => 'vr_content_type',
				'type' => 'radio',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'choices' => array (
					'audio' => 'Audio Note',
					'video' => 'Video',
					'image' => 'Screenshot',
				),
				'allow_null' => 0,
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'vertical',
				'return_format' => 'value',
			),
			array (
				'key' => 'description',
				'label' => 'Description',
				'name' => 'description',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array (
				'key' => 'relationship',
				'label' => 'Class ID',
				'name' => 'class_id',
				'type' => 'relationship',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array (
					0 => 'course-class',
				),
				'taxonomy' => array (
				),
				'filters' => array (
					0 => 'search',
				),
				'elements' => '',
				'min' => '',
				'max' => '',
				'return_format' => 'object',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'vr_user_content',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	endif;

/**
 * Register custom course and quiz fields.
 */
if (function_exists('register_field_group')) {
	register_field_group(array(
		'key' => 'immerselms_sfwd_courses',
		'title' => 'ImmerseLMS Course Settings',
		'fields' => array(
			array (
				'key' => 'field_5b516279866fa',
				'label' => 'Assigned Trainers/Markers',
				'name' => 'assigned_users',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => '',
				'sub_fields' => array (
					array (
						'key' => 'field_5b516280866fb',
						'label' => 'User',
						'name' => 'user',
						'type' => 'user',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'role' => array (
							0 => 'trainer',
							1 => 'tutor_admin',
							2 => 'administrator',
							3 => 'marker',
						),
						'allow_null' => 0,
						'multiple' => 0,
					),
				),
			),
			array(
				'key' => 'field_1',
				'label' => 'Max Quiz Attempts',
				'name' => 'max_quiz_attempts',
				'type' => 'number',
				'instructions' => 'The maximum amount of attempts for quizzes within this course. Leave blank for uncapped.'
			),
			array(
				'key' => 'field_2',
				'label' => 'Quiz Deadline',
				'name' => 'max_quiz_attempts',
				'type' => 'number',
				'instructions' => 'The maximum amount of attempts for quizzes within this course. Leave blank for uncapped.'
			),
			array(
				'key' => 'field_3',
				'label' => 'Assigned Tutor',
				'name' => 'assigned_tutor',
				'type' => 'user',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'role' => array (
					1 => 'tutor_admin',
					2 => 'administrator'
				),
				'allow_null' => 0,
				'multiple' => 0,
				'instructions' => 'The tutor assigned for this course.'
			)
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-courses'
				),
			)
		)
	));

	register_field_group(array(
		'key' => 'immerselms_sfwd_courses',
		'title' => 'ImmerseLMS Course Settings',
		'fields' => array(
			array(
				'key' => 'field_2',
				'label' => 'Max Quiz Attempts',
				'name' => 'max_quiz_attempts',
				'type' => 'number',
				'instructions' => 'The maximum amount of attempts for this quiz. If a maximum is set for the course, this value will be used instead. Leave blank for uncapped.'
			)
		),
		'location' => array(
			array(
				array(
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'sfwd-quiz'
				),
			)
		)
	));
	
}

/**
 * Related emails for school and student's parents.
 */
if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_5b6c1f3704711',
		'title' => 'Related Emails',
		'fields' => array (
			array (
				'key' => 'field_5b6c1f714d046',
				'label' => 'Parent Email',
				'name' => 'parent_email',
				'type' => 'email',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
			array (
				'key' => 'field_5b6c1fa34d047',
				'label' => 'School Email',
				'name' => 'school_email',
				'type' => 'email',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'user_form',
					'operator' => '==',
					'value' => 'edit',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
endif;

/**
 * Course variation fields.
 */
if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5bcd3a9c9e0d0',
		'title' => 'Course Variations',
		'fields' => array(
			array(
				'key' => 'field_5bcd9b0e3e97c',
				'label' => 'Students',
				'name' => 'students',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => '',
				'sub_fields' => array(
					array(
						'key' => 'field_5bcd9b353e97d',
						'label' => 'Student',
						'name' => 'student',
						'type' => 'user',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array(
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'role' => array(
							0 => 'subscriber',
						),
						'allow_null' => 0,
						'multiple' => 0,
						'return_format' => 'array',
					),
				),
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-courses',
				),
			)
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
	endif;

/**
 * Custom fields for lessons.
 */
if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5be059f9651ad',
		'title' => 'ImmerseLMS Lesson Settings',
		'fields' => array(
			array(
				'key' => 'field_5be05a064fa71',
				'label' => 'Unit Number',
				'name' => 'unit_number',
				'type' => 'text',
				'instructions' => 'The unit number for this course, used primarily in pushing updates to SugarCRM.',
				'required' => 1,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-lessons',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
endif;

/**
 * Custom deadline fields for courses per student.
 */
if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array (
		'key' => 'group_5b6c22ff727d5',
		'title' => 'Student Deadlines',
		'fields' => array (
			array (
				'key' => 'field_5b6c2315a06aa',
				'label' => 'Student Deadlines',
				'name' => 'student_deadlines',
				'type' => 'repeater',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'collapsed' => '',
				'min' => 0,
				'max' => 0,
				'layout' => 'table',
				'button_label' => '',
				'sub_fields' => array (
					array (
						'key' => 'field_5b6c2322a06ab',
						'label' => 'Student',
						'name' => 'student',
						'type' => 'user',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'role' => array (
							0 => 'subscriber',
						),
						'allow_null' => 0,
						'multiple' => 0,
					),
					array (
						'key' => 'field_5b6c2334a06ac',
						'label' => 'Deadline',
						'name' => 'deadline',
						'type' => 'date_picker',
						'instructions' => '',
						'required' => 1,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'display_format' => 'd/m/Y',
						'return_format' => 'Ymd',
						'first_day' => 1,
					),
				),
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'sfwd-courses',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));
	
endif;

/*
* Adds bidirectional relationship between Courses and Classes
*/
function bidirectional_acf_update_value( $value, $post_id, $field  ) {

	// vars
	$field_name = $field['name'];
	$field_key = $field['key'];
	$global_name = 'is_updating_' . $field_name;
	
	// bail early if this filter was triggered from the update_field() function called within the loop below
	// - this prevents an inifinte loop
	if( !empty($GLOBALS[ $global_name ]) ) return $value;
	
	// set global variable to avoid inifite loop
	// - could also remove_filter() then add_filter() again, but this is simpler
	$GLOBALS[ $global_name ] = 1;
	
	// loop over selected posts and add this $post_id
	if( is_array($value) ) {
		foreach( $value as $post_id2 ) {
			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);

			// allow for selected posts to not contain a value
			if( empty($value2) ) {
				$value2 = array();
			}
			
			// bail early if the current $post_id is already found in selected post's $value2
			if( in_array($post_id, $value2) ) continue;
			
			// append the current $post_id to the selected post's 'related_posts' value
			$value2[] = $post_id;
			
			// update the selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);			
		}
	}
	
	// find posts which have been removed
	$old_value = get_field($field_name, $post_id, false);
	
	if( is_array($old_value) ) {
		
		foreach( $old_value as $post_id2 ) {
			
			// bail early if this value has not been removed
			if( is_array($value) && in_array($post_id2, $value) ) continue;
			
			// load existing related posts
			$value2 = get_field($field_name, $post_id2, false);
			
			// bail early if no value
			if( empty($value2) ) continue;
			
			// find the position of $post_id within $value2 so we can remove it
			$pos = array_search($post_id, $value2);
			
			// remove
			unset( $value2[ $pos] );
			
			// update the un-selected post's value (use field's key for performance)
			update_field($field_key, $value2, $post_id2);
		}
	}
	
	// reset global varibale to allow this filter to function as per normal
	$GLOBALS[ $global_name ] = 0;
	
	// return
	return $value;
	
}
add_filter('acf/update_value/name=course_class', 'bidirectional_acf_update_value', 10, 3);

function th_get_roles_for_post_type( $post_type ) {
    global $wp_roles;

    $roles = array();
    $type  = get_post_type_object( $post_type );

    // Get the post type object caps.
    $caps = array( $type->cap->edit_posts, $type->cap->publish_posts, $type->cap->create_posts );
    $caps = array_unique( $caps );

    // Loop through the available roles.
    foreach ( $wp_roles->roles as $name => $role ) {

        foreach ( $caps as $cap ) {

            // If the role is granted the cap, add it.
            if ( isset( $role['capabilities'][ $cap ] ) && true === $role['capabilities'][ $cap ] ) {
                $roles[] = $name;
                break;
            }
        }
    }

    return $roles;
}

add_action( 'load-post.php',     'th_load_user_dropdown_filter' );
add_action( 'load-post-new.php', 'th_load_user_dropdown_filter' );

function th_load_user_dropdown_filter() {
    $screen = get_current_screen();

    if ( empty( $screen->post_type ) || 'vr_user_content' !== $screen->post_type )
        return;

    add_filter( 'wp_dropdown_users_args', 'th_dropdown_users_args', 10, 2 );
}

function th_dropdown_users_args( $args, $r ) {
    global $wp_roles, $post;

    // Check that this is the correct drop-down.
    if ( 'post_author_override' === $r['name'] && 'vr_user_content' === $post->post_type ) {

        $roles = th_get_roles_for_post_type( $post->post_type );

        // If we have roles, change the args to only get users of those roles.
        if ( $roles ) {
            $args['who']      = '';
            $args['role__in'] = $roles;
        }
    }
	return $args;
}
?>