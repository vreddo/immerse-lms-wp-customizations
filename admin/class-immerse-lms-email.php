<?php
/**
 * Allows easy usage of email templates in the plugin. The templates use a stripped down version of
 * handlebars templating.
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/email
 * @author     JC Gurango <jc@jcgurango.com>
 */
class Immerse_Lms_Email
{
    public static function render_template($string, $data) {
        return preg_replace_callback("/{{#if (.+?)}}([\s\S]+?){{\/if}}|{{(.+?)}}/", function($match) use ($data) {
            if (!empty($match[3])) {
                return $data[$match[3]];
            }
    
            if (!empty($match[1])) {
                $boolean = $data[$match[1]];

                error_log('test');
                error_log($match[1]);
                error_log(print_r($boolean, true));
    
                if ($boolean) {
                    return Immerse_Lms_Email::render_template($match[2], $data);
                } else {
                    return '';
                }
            }
        }, $string);
    }

    public static function send_email($recipient, $template_id, $data) {
        // Render the template.
        $rendered = Immerse_Lms_Email::render_email($template_id, $data);

        if( 
            $template_id != 'assessment_completed' &&
            ( sanitize_text_field( get_option('et_hs_from_to_name') ) && sanitize_email( get_option('et_hs_from_to_email') )
        ) ) {
            $headers = array('Content-Type: text/html; charset=UTF-8', 'Reply-To: ' . sanitize_text_field( get_option('et_hs_from_to_name') ) . ' <' . sanitize_email( get_option('et_hs_from_to_email') ) . '>');
        } else {
            $headers = array('Content-Type: text/html; charset=UTF-8');
        }

        // Send it out.
        wp_mail($recipient, $rendered['subject'], $rendered['body'], $headers);
    }

    public static function render_email($template_id, $data) {
        // Retrieve the template.
        $template = Immerse_Lms_Email::retrieve_email_template($template_id);

        // Render the template.
        $subject = Immerse_Lms_Email::render_template($template['subject'], $data);
        $body = Immerse_Lms_Email::render_template($template['body'], $data);

        return array(
            'subject' => $subject,
            'body' => $body
        );
    }

    public static function retrieve_email_template($template_id) {
        $template = get_posts(array(
            'post_type' => 'rm-email-template',
            'meta_key' => '_template_id',
            'meta_compare' => '=',
            'meta_value' => $template_id
        ));

        if (count($template) > 0) {
            $template = $template[0];

            $title = $template->post_title;
            $body = $template->post_content;
            $subject = get_field('email_subject', $template);

            return array(
                'title' => $title,
                'subject' => $subject,
                'body' => $body
            );
        } else {
            if (isset(Immerse_Lms_Email::$immerse_lms_template_defaults[$template_id])) {
                return Immerse_Lms_Email::$immerse_lms_template_defaults[$template_id];
            }

            return null;
        }
    }

    /**
     * Persists the default templates if they don't already exist.
     */
    public static function persist_default_templates() {
        // Retrieve the email template posts.
        $templates = get_posts(array(
            'post_type' => 'rm-email-template',
            'numberposts' => -1
        ));

        if (count($templates) < count(Immerse_Lms_Email::$immerse_lms_template_defaults)) {
            // Some templates are missing.
            $existing_ids = array_map(function($template) {
                return get_post_meta($template->ID, '_template_id', true);
            }, $templates);
            
            foreach (Immerse_Lms_Email::$immerse_lms_template_defaults as $id => $template) {
                if (!in_array($id, $existing_ids)) {
                    $default = Immerse_Lms_Email::$immerse_lms_template_defaults[$id];

                    $new_id = wp_insert_post(array(
                        'post_type' => 'rm-email-template',
                        'post_title' => $default['title'],
                        'post_content' => $default['body'],
                        'post_status' => 'publish'
                    ));

                    update_field('email_subject', $default['subject'], $new_id);
                    update_post_meta($new_id, '_template_id', $id);
                }
            }
        }
    }

    /**
     * Default values for templates
     */
    private static $immerse_lms_template_defaults = array(
        'assessment_completed' => array(
            'title' => 'Assessment - Notification on Student Completion',
            'subject' => 'For Your Review: {{user_name}} has completed {{quiz_title}}',
            'body' => "Student Name: {{user_name}}<br />
            Phone Number: {{user_phone}}<br />
            Email: {{user_email}}<br />
            Course: <a href='{{course_link}}'>{{course_title}}</a><br />
            Quiz: <a href='{{quiz_link}}'>{{quiz_title}}</a>
            {{#if marking_guide}}
            <br />
            Marking Guide: <a href='{{marking_guide_link}}'>{{marking_guide_title}}</a>
            {{/if}}"
        ),
        'assessment_graded' => array(
            'title' => 'Assessment - Student Notification for Trainer Grading',
            'subject' => 'Your trainer has graded {{quiz_title}}',
            'body' => "Hello, {{user_name}}!<br />
            Your submitted answers for the assessment <b>{{quiz_title}}</b> have been graded. Your grade is: <b>{{assessment_grade}}</b>.
            {{#if trainer_notes}}
            <br /><br />Your trainer had the following notes:<br />
            {{trainer_notes}}
            {{/if}}
            {{#if retake}}
            <br /><br />You may retake this assessment through this link: <a href=" . '"{{retake_link}}"' . ">{{quiz_title}}</a>
            {{/if}}"
        ),
        'course_upcoming_deadline' => array(
            'title' => 'Course - Student Notification for Approaching Deadline (7 Days Before Deadline)',
            'subject' => 'Reminder: Please complete {{course_title}}...',
            'body' => "Hello, {{user_name}}<br />
            This is just a reminder that the due date for your assessment/s is on {{deadline}}.
            Please do your best to meet the deadline. If you need help kindly contact your tutor at {{tutor_phone_number}}."
        ),
        'course_passed_deadline' => array(
            'title' => 'Course - Student Notification for Passed Deadline (2 Days After Deadline)',
            'subject' => 'Reminder: {{course_title}} is passed its deadline...',
            'body' => "Hello, {{user_name}}<br />
            The due date for your assessment/s was on {{deadline}}. On checking the system we have not yet received your submission. If you need help kindly contact your tutor at {{tutor_phone_number}}. Please have this submitted by {{deadline_plus_six}}."
        ),
        'course_passed_deadline_school' => array(
            'title' => 'Course - School Notification for Passed Deadline (7 Days After Deadline)',
            'subject' => '{{user_name}} has not yet completed {{course_title}}...',
            'body' => "To whom it may concern,<br />
            <p>We hope this email finds you well. </p>
            <p>The assessments for this term were due on {{deadline}}. On checking our system, {{user_name}} has yet to submit the assessments. They have been contacted and reminded of the deadline but have not made contact nor attempts at completing the work.</p>
            <p>Kindly have them contact their trainer on how to catch up to their studies.</p>"
        ),
        'course_passed_deadline_parent' => array(
            'title' => 'Course - Parent Notification for Passed Deadline (7 Days After Deadline)',
            'subject' => '{{user_name}} has not yet completed {{course_title}}...',
            'body' => "Dear Parent,<br />
            <p>We hope this email finds you well.</p>
            <p>The assessment for this term was due on {{deadline}}. On checking our system, {{user_name}} has yet to submit this. They have been contacted and reminded of the deadline but have not made contact nor attempts at completing the work.</p>
            <p>Kindly have them contact their trainer on how to catch up to their studies.</p>"
        ),
    );
}

// Persist the default email templates.
add_action('init', 'Immerse_Lms_Email::persist_default_templates');