<?php

/**
 * The vreddo-specific functionality of the plugin.
 *
 * @link       pedrops.com
 * @since      1.0.0
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/vreddo
 */

/**
 * The vreddo-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the vreddo-specific stylesheet and JavaScript.
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/vreddo
 * @author     Pedro Germani <pedro.germani@toptal.com>
 */
class Immerse_Lms_VReddo {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->options = get_option($this->plugin_name.'_options');

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Immerse_Lms_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Immerse_Lms_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/immerse-lms-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Immerse_Lms_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Immerse_Lms_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		//wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/immerse-lms-admin.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name."_vreddo_content", plugin_dir_url( __FILE__ ) . 'js/immerse-lms-vreddo-content.js', array( 'jquery' ), $this->version, false );
	}

	/**
	* REST API Init
	*
	*/
	public function immerse_lms_vreddo_library_rest_api_init() {
		register_rest_route( 'vreddo-library/v1', '/add/', array(
			'methods' => 'GET',
			'callback' => array( $this ,'immerse_lms_add_external_vreddo_content'),
		) );
	}

	/**
	* REST API to retrieve download urls by license key and email
	*
	*/
	public function immerse_lms_add_external_vreddo_content() {
		//$s = mime_content_type($download['file']['file']);
		if ( ! isset ( $_GET['email'] ) ) {
			return array(
				'error' => 'No email provided',
			);
		}
		$base_url = $this->options['immerse_lms_field_vreddo_store_url'] . "/wp-json/vreddo-store/v1/license";

		$args = array(
			'email'         => $_GET['email'],
			'product_id'    => $_GET['product_id'],
			'license_key'   => $_GET['license_key'],
			'license_url'	=> $_GET['license_url']
		);
		
		$result = $this->add_vreddo_content($base_url, $args);

		if ($result['error']) {
			wp_send_json_error($result['error']);
		}
		else {
			wp_send_json_success($result);
		}
	}

	public function immerse_lms_ajax_add_vreddo_content (){
		$error = false;
		if (!wp_verify_nonce( $_POST['nonce'], 'immerse_lms_ajax_vreddo_content' )) {
			$error = array (
				'error' => 'ERROR: unverified nonce '
			);

			wp_send_json_error($error);
		}
		
		if ($_POST['email'] == '' || $_POST['product_id'] == '' || $_POST['license_key'] == '') {
			$error = array (
				'error' => 'ERROR: All fields are required '
			);
			wp_send_json_error($error);
		}

		$base_url = $this->options['immerse_lms_field_vreddo_store_url'] . "/wp-json/vreddo-store/v1/license";
		$target_url = $base_url .'?' . http_build_query( $args );

		$args = array(
			'email'         => $_POST['email'],
			'product_id'    => $_POST['product_id'],
			'license_key'   => $_POST['license_key'],
			'license_url'	=> get_site_url()
		);
		$result = $this->add_vreddo_content($base_url, $args);

		if ($result['error']) {
			wp_send_json_error($result['error']);
		}
		else {
			wp_send_json_success($result);
		}
	}

	public function add_vreddo_content ($base_url, $args){
		$target_url = $base_url .'?' . http_build_query( $args );

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_URL, $target_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 400);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			$error = array(
				'error' => curl_error($ch),
				'result' => curl_error($result)
			);
			curl_close ($ch);
			return array('error' => $error);
		}
		curl_close ($ch);

		$downloads = json_decode($result, true);
		
		if ($downloads['error']) {
			$error = array(
				'error' => $downloads['error'] . (($downloads['additional info']) ? ' -- ' . $downloads['additional info'] : ''),
			);
			return array('error' => $error);
		}
		// Get the path to the upload directory.
		$wp_upload_dir = wp_upload_dir();

		foreach ($downloads['downloads'] as $key => $value) {
			$url = $value['url'];
			$img = $wp_upload_dir['path'] . '/'. str_replace(' ', '_', $value['name']) .'_'. substr($value['download_id'], -6) . '.' . $value['extension'] ;

			//$copied = copy($url, $img);

			$ch = curl_init($url);
			$fp = fopen($img, "wb");
		
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_HEADER, 0);

			curl_exec($ch);
			if (curl_errno($ch)) {
				$error = array(
					'error' => curl_error($ch),
				);
				curl_close ($ch);
				fclose($fp);
				return array('error' => $error);
			}
			curl_close($ch);
			fclose($fp);

			
			// if ( ! $copied) {
			// 	$error = array(
			// 		'error' => "File could not be copied to local uploads folder.",
			// 		'url' => $url,
			// 		'img' => $img
			// 	);
			// 	wp_send_json_error($error);
			// }
			if ('obj' == $value['extension']){
				$mime = 'application/octet-stream';
			}
			$attachment = array(
				'guid'=> $wp_upload_dir['url'] . '/' . basename( str_replace(' ', '_', $value['name']) .'_'. substr($value['download_id'], -6) . '.' . $value['extension'] ), 
				'post_mime_type' => $mime ? $mime : mime_content_type ( $img ),
				'post_title' => $value['name'],
				'post_content' => 'Vreddo Content downloaded dinamically',
				'post_status' => 'inherit'
				);

			$image_id = wp_insert_attachment($attachment, $img, null);
			$ids[] = $image_id;
			// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			// Generate the metadata for the attachment, and update the database record.
			$attach_data = wp_generate_attachment_metadata( $image_id, $img );
			$attach_data['_is_vr_content'] = true;

			if ($value['extension'] == "obj") {
				if ($value['thumb']){
					$thumb_url = $value['thumb'];
					$thumb_basename = basename($value['thumb']);
					$thumb_extension = pathinfo($value['thumb'], PATHINFO_EXTENSION);
					$thumb_local = $wp_upload_dir['path'] . '/'. str_replace(' ', '_', $value['name']) .'_'. substr($value['download_id'], -6) . '_thumb.' . $thumb_extension ;
					$thumb_copied = false;
					$ch = curl_init($thumb_url);
					$fp = fopen($thumb_local, "wb");
				
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
					curl_setopt($ch, CURLOPT_FILE, $fp);
					curl_setopt($ch, CURLOPT_HEADER, 0);

					curl_exec($ch);
					if (curl_errno($ch)) {
						$error = array(
							'error' => curl_error($ch),
						);
						curl_close ($ch);
						fclose($fp);
						return array('error' => $error);
					}
					else {
						$thumb_copied = true;
					}
					curl_close($ch);

					fclose($fp);

					//$thumb_copied = copy($thumb_url, $thumb_local);
					if ($thumb_copied) {
						$attach_data['_alternative_thumb'] = $wp_upload_dir['url']. '/'. str_replace(' ', '_', $value['name']) .'_'. substr($value['download_id'], -6) . '_thumb.' . $thumb_extension;
					}
				}
			}


			//$attach_datas[] = $attach_data;
			$update = update_post_meta($image_id, '_is_vr_content', 1);
			$update_metadata[] = wp_update_attachment_metadata( $image_id, $attach_data );
		}

		$return_data = array(
			'success' => true,
			//'email' => $_POST['email'],
			//'license_key' => $_POST['license_key'],
			//'product_id' => $_POST['product_id'],
			'downloads' => $downloads,
			//'attachdata' => $attach_datas,
			'count' => $update_metadata,
			'ids' => $ids, 
			'thumb' => array(
				'thumb_url' => $thumb_url,
				'basename' => $basename,
				'extension' => $extension,
				'thumb_local' => $thumb_local,
				'copied' => $thumb_copied,
				)
		);


		if ($error) $return_data =  array('error' => $error);
		
		return $return_data;
	}

	public function immerse_lms_ajax_get_vreddo_content_table_item (){
		$newitems = array_reverse($_POST['items']);
		$html ='';
		foreach($newitems as $id) {
			$item = get_post((int) $id);
			ob_start();
			require plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-vreddo-content-item.php';
			$html .= ob_get_clean();
		}
		
		$return_data = ['ids'=>$newitems, 'html'=>$html];
		
		wp_send_json_success($return_data);
		wp_die();
	}
	
	/**
	 * Add Class - VReddo Store Content meta box
	 *
	 * @param post $post The post object
	 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/add_meta_boxes
	 */
	public function class_add_meta_boxes( $post ){
		add_meta_box( 'vr_store_meta_box', 'VReddo Store Items', array($this, 'vr_store_build_meta_box'), 'course-class' );
	}

	/**
	 * Build custom field meta box
	 *
	 * @param post $post The post object
	 */
	public function vr_store_build_meta_box( $post ){
		wp_enqueue_style( 'jquery-ui-css-vreddo', 'http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', array(), $this->version, 'all' );
		
		$vr_store_args = array(
			'post_type' => 'attachment',
			'post_status' => 'inherit', 
			'meta_query'=> array(
				array(
					'key' => '_is_vr_content',
					'value' => '1',
					'compare' => '='
				),
			)
		);
		$vr_content = new WP_Query($vr_store_args);

		$vr_store_items = $vr_content->posts;
		$vr_store_added_items_ids = get_post_meta( $post->ID, '_vr_store_list', true );

		foreach ($vr_store_items as $key => $item) {
			if (in_array($item->ID, $vr_store_added_items_ids)) {
				$vr_store_added_items[$key] = (array) $item;
			}
			else {
				$vr_store_all_items[$key] = (array) $item; 
			}
		}
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/partials/immerse-lms-admin-class-vr-store-metabox.php';
	}

	/**
	 * Build custom field meta box
	 *
	 * @param post $post The post object
	 */
	public function class_vr_meta_boxe_save( $post_id ){
		if (array_key_exists('vr_store_item', $_POST)) {
			update_post_meta(
				$post_id,
				'_vr_store_list',
				$_POST['vr_store_item']
			);
		}
		else {
			delete_post_meta(
				$post_id,
				'_vr_store_list'
			);
		}
	}

	/**
	 * Change default file icon on media gallery
	 *
	 */
	function immerse_lms_change_mime_icon($icon, $mime = null, $post_id = null){
		$extension = pathinfo(basename(get_attached_file($post_id)), PATHINFO_EXTENSION);
		if ($extension == 'obj') {
			// $meta = wp_get_attachment_metadata($post_id);
			// if ($meta['_alternative_thumb']) {
			// 	$icon = str_replace(get_bloginfo('wpurl').'/wp-includes/images/media/default.png',  $meta['_alternative_thumb'], $icon);
			// }
			// else {
				$icon = str_replace(get_bloginfo('wpurl').'/wp-includes/images/media/default.png',  plugin_dir_url( dirname( __FILE__ ) ) . 'admin/images/default.png', $icon);
			// }
		}
		
		return $icon;
	}

}

