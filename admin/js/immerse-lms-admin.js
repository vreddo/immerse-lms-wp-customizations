(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	$(function() {

		$(".immerse_lms_marker_students").each(function() {
			// Initialize the control.
			var $this = $(this);

			var selected = $this.data("selected") ? $this.data("selected").toString().split(",") : [];
			var leftSearch = $this.find(".learndash-binary-selector-search-left");
			var rightSearch = $this.find(".learndash-binary-selector-search-right");
			var leftItems = $this.find(".learndash-binary-selector-items-left");
			var rightItems = $this.find(".learndash-binary-selector-items-right");
			var addButton = $this.find(".learndash-binary-selector-button-add");
			var removeButton = $this.find(".learndash-binary-selector-button-remove");
			var selectedField = $this.find(".selectedinput");
			
			// Read the possible items.
			var items = [];

			leftItems.children().each(function() {
				items.push({
					label: $(this).text(),
					value: $(this).attr("value")
				});
			});

			leftSearch.keyup(function() {
				var query = $(this).val();
				
				leftItems.children().hide();

				leftItems.children().filter(function(i, e) {
					if (!query) {
						return true;
					}

					return $(e).text().toLowerCase().indexOf(query.toLowerCase()) > -1;
				}).show();
			});

			rightSearch.keyup(function() {
				var query = $(this).val();
				
				rightItems.children().hide();

				rightItems.children().filter(function(i, e) {
					if (!query) {
						return true;
					}

					return $(e).text().toLowerCase().indexOf(query.toLowerCase()) > -1;
				}).show();
			});

			function render() {
				var leftItemsRendered = [];
				var rightItemsRendered = [];

				items.forEach(function(item) {
					leftItemsRendered.push(
						$("<option />")
						.attr("value", item.value)
						.text(item.label)
						.addClass("learndash-binary-selector-item")
						.addClass(selected.indexOf(item.value) > -1 ? "learndash-binary-selector-item-disabled" : "")
						.prop("disabled", selected.indexOf(item.value) > -1)
					);

					if (selected.indexOf(item.value) > -1) {
						rightItemsRendered.push(
							$("<option />")
							.attr("value", item.value)
							.text(item.label)
							.addClass("learndash-binary-selector-item")
						)
					}
				});

				leftItems.empty().append(leftItemsRendered);
				rightItems.empty().append(rightItemsRendered);

				selectedField.val(selected.join(","));
			}

			addButton.click(function(e) {
				selected = selected.concat(leftItems.val());
				render();
				e.preventDefault();
				e.stopPropagation();
			});

			removeButton.click(function(e) {
				rightItems.val().forEach(function(item) {
					selected.splice(selected.indexOf(item), 1);
				});

				render();
				e.preventDefault();
				e.stopPropagation();
			});

			render();
		});

		// Selectively show/hide the course variations
		if ($("#acf-group_5bcd3a9c9e0d0").length) {
			var courseVariationsGroup = $("#acf-group_5bcd3a9c9e0d0");

			if ($("#parent_id").length) {
				$("#parent_id").change(function() {
					if ($(this).val()) {
						courseVariationsGroup.show();
					} else {
						courseVariationsGroup.hide();
					}
				}).change();
			} else {
				courseVariationsGroup.hide();
			}
		}
	});

	var handlers = {
		set_id: function(field) {
			if (field.data.key == 'vr_content_id') {
				var val = field.val();

				if (!val) {
					field.val(get_next_vr_content_id());
				}
			}
		}
	};

	acf.addAction('load_field', handlers.set_id);
	acf.addAction('append_field', handlers.set_id);
})( jQuery );

function get_next_vr_content_id() {
	var last_id = $(".acf-field[data-key=vr_content_row_id] input").val();

	if (!last_id) {
		last_id = 0;
	}

	last_id++;
	$(".acf-field[data-key=vr_content_row_id] input").val(last_id);

	return last_id;
}