(function ($) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	$(function () {

		// Adds Items to Media Credits Table when native Attachment upload happened
		function addVreddoContent(evt) {
			// disable form submit
			evt.preventDefault();

			// Show spinner icon
			$('#vreddo-licensed-downloads .spinner').css("visibility", 'visible');
			$('#vreddo-licensed-downloads .spinner').css("display", 'inline-block');

			// toggle messages
			$('#vreddo-licensed-downloads .vreddo-content-ajax-msg').hide();
			$('.vreddo-content-ajax-warning').text('Validating license and downloading files. Please wait...').show();

			var action = 'immerse_lms_ajax_add_vreddo_content';
			var senddata = {
				action: action,
				email: $('#email').val(),
				license_key: $('#license_key').val(),
				product_id: $('#product_id').val(),
				license_url: $('#license_url').val(),
				nonce: $('#vreddo-content-nonce').val()
			};
			$.post(ajaxurl, senddata, function (response) {
				console.log(response.data);
				$('.vreddo-content-ajax-msg').hide();
				if (!response || !response.success) {

					if (response.data != null) {

						$('.vreddo-content-ajax-error').text(response.data.error).show().fadeOut(15000);
					} else {
						$('.vreddo-content-ajax-error').text('Error: No response from server').show().fadeOut(15000);
					}
				} else {
					if (response.data.count) {
						addVreddoTableItem(response.data.ids);
						var message = "Success! " + response.data.count.length + ' files downloaded. Adding to VReddo Library';
						$('.vreddo-content-ajax-success').text(message).show();
					} else {
						$('.vreddo-content-ajax-msg').hide();
						$('.vreddo-content-ajax-error').text('Error: Count is null').show().fadeOut(15000);
					}

				}
				$('#vreddo-licensed-downloads .spinner').css("visibility", 'hidden');
				$('#vreddo-licensed-downloads .spinner').css("display", 'none');
			});


		}

		function addVreddoTableItem(items) {
			var action = 'immerse_lms_ajax_get_vreddo_content_table_item';
			var newUploadsenddata = {
				action: action,
				items: items,
				nonce: $('#vreddo-content-nonce').val()
			};
			$.post(ajaxurl, newUploadsenddata, function (response) {
				if (response === '0' || response === '-1') {
					//TODO: Add Error handling (Wrong nonce, no permissions, …) here.

				} else {
					// Test if the file is already on  list, otherwise add it
					$('#vreddo-content-list tbody tr:first').before(response.data.html);
					$('.vreddo-content-ajax-success').text('Files added to VReddo Library').fadeOut(8000);
				}
			});
		}

		function setupVRStoreSortables() {
			$("#sortable1").sortable({
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight",
				update: function () {
					$('#sortable1 li input').prop('disabled', true);
				},
				start: function () {
					$('#sortable1 li').css('cursor', 'grabbing');
				},
				stop: function () {
					$('#sortable1 li').css('cursor', 'grab');
				}
			}).disableSelection();

			$("#sortable2").sortable({
				connectWith: ".connectedSortable",
				placeholder: "ui-state-highlight ui-state-remove",
				update: function( ) {
					$('#sortable2 li input').prop('disabled', false);
				},
				start: function () {
					$('#sortable1 li').css('cursor', 'grabbing');
				},
				stop: function () {
					$('#sortable1 li').css('cursor', 'grab');
				}
			}).disableSelection();

			
		}

		$(window).load(function () {
			// set event click on ajax save button
			$('#vreddo-content-submit').click(addVreddoContent);
			if ($("#sortable1").length) {
				setupVRStoreSortables();
			}	
		});
	});
})(jQuery);