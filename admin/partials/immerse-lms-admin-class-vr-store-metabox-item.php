<li id='vr-store-<?= $item['ID']?>' data-id='<?= $item['ID']?>' class="ui-state-default">
	<?php 
	$meta = wp_get_attachment_metadata($item['ID']);
	//print_r($meta);
	if ($meta['_alternative_thumb']) { ?>
		<img src="<?= $meta['_alternative_thumb'] ?>" width=100 height=100/>
	<?php }
	else {
		$thumb = wp_get_attachment_image( $item['ID'], array(100,100), '', array( "title" => basename(get_attached_file($item['ID']))));
		if ($thumb) {
			echo $thumb;
		}
		else { 
			?>
			<img src="/wp-includes/images/media/default.png" title="<?= basename(get_attached_file($id)); ?>" width="75" height="100" style="margin:0px 12px;"/>
			<?php
		}
		
	}
	?>
	<p><?= $item['post_title']?></p>
	
	<input name='vr_store_item[]' class='vr-store-item' type="hidden" value=<?= $item['ID']?> <?= $disabled ? 'disabled=""' : ''?> />
</li>
