	<p class='description'>Drag and Drop the items you want to relate to this class to the list on the right</p>
	<ul id="sortable1" class="connectedSortable vr-store-list">
		<?php
			$disabled = true;
			if ( $vr_store_all_items ) {
				foreach ($vr_store_all_items as $item) {
					include plugin_dir_path( dirname( __FILE__ ) ) . 'partials/immerse-lms-admin-class-vr-store-metabox-item.php';	
				}
			}
		?>
	</ul>
	
	<ul id="sortable2" class="connectedSortable vr-store-list">
		<?php
			$disabled = false;
			if ( $vr_store_added_items ) {
				foreach ($vr_store_added_items as $item) {
					include plugin_dir_path( dirname( __FILE__ ) ) . 'partials/immerse-lms-admin-class-vr-store-metabox-item.php';	
				}
			}
		?>
	</ul>
	
	<div class='clear'></div>