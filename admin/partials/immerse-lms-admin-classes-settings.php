<div class="wrap">
	<h1>VR Options</h1>
</div>
<form method="post" action="options.php">
	<?php settings_fields( 'immerse-lms-class-settings-group' ); ?>
	<?php do_settings_sections( 'immerse-lms-class-settings-group' ); ?>
	<table class="form-table">
		<tr valign="top">
		<th scope="row">New Option Name</th>
		<td></td>
		</tr>
		
		<tr valign="top">
		<th scope="row">Some Other Option</th>
		<td></td>
		</tr>
		
		<tr valign="top">
		<th scope="row">Options, Etc.</th>
		<td></td>
		</tr>
	</table>
	
	<?php submit_button(); ?>

</form>