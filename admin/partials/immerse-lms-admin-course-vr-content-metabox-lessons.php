<h4>Lessons VR Content</h4>
<table class="course-lessons-vr-content-list wp-list-table widefat fixed striped media">
	<thead>
		<th class="manage-column column-title column-primary">Lesson</th>
		<th class="manage-column" width=10%>qty</th>
		<th class="manage-column">VR Content</th>
	</thead>
	<tbody>
		<?php foreach ($lessons as $lesson) : ?>
			<?php 
			$lesson_id = $lesson['post']->ID;
			if ($vr_content_lesson_num[$lesson_id] || $vr_content_topic_number[$lesson_id]) : ?>	
				<tr class="">
					<td>
						<a href="/wp-admin/post.php?post=<?=$lesson_id?>&action=edit"><?= get_the_title($lesson_id)?></a>
						
					</td>
					<td>
						<?= $vr_content_lesson_num[$lesson_id]?>
					</td>
					<td>
						<?php
						if ($vr_content_lesson_num[$lesson_id]){
							for ($i=0; $i < $vr_content_lesson_num[$lesson_id] ; $i++) { 
								$vr_content_id = get_post_meta($lesson_id, 'vr_content_'.$i.'_vr_content_file', ARRAY_A);
								if (strpos(get_post_mime_type( $vr_content_id ), 'video') !== false) : ?>
									<img src="/wp-includes/images/media/video.png" title="<?= basename(get_attached_file($vr_content_id)); ?>" width=30 height=30/>
								<?php elseif (strpos(get_post_mime_type( $vr_content_id ), 'image') !== false) : ?>
									<?= wp_get_attachment_image( $vr_content_id, array(30,30), '', array( "title" => basename(get_attached_file($vr_content_id)))); ?>
								<?php else : ?>
									<img src="/wp-includes/images/media/default.png" title="<?= basename(get_attached_file($vr_content_id)); ?>" width=30 height=30/>
								<?php endif;
								
							}
						}
						else {
							echo '---';
						}	
						?>
					</td>
				</tr>
				<?php foreach ($vr_content_topic_number[$lesson_id] as $topic_id => $topic_vr_content) : ?>
					<tr class="">
						<td>
							&nbsp;&nbsp;&nbsp;topic: <a href="/wp-admin/post.php?post=<?=$topic_id?>&action=edit"><?= get_the_title($topic_id)?></a>
						</td>
						<td>
							<?= $vr_content_topic_number[$lesson_id][$topic_id]?>
						</td>
						<td>
							<?php
							for ($i=0; $i < $vr_content_topic_number[$lesson_id][$topic_id] ; $i++) { 
								$vr_content_id = get_post_meta($topic_id, 'vr_content_'.$i.'_vr_content_file', ARRAY_A);
								if (strpos(get_post_mime_type( $vr_content_id ), 'video') !== false) : ?>
									<img src="/wp-includes/images/media/video.png" title="<?= basename(get_attached_file($vr_content_id)); ?>" width=30 height=30/>
								<?php elseif (strpos(get_post_mime_type( $vr_content_id ), 'image') !== false) : ?>
									<?= wp_get_attachment_image( $vr_content_id, array(30,30), '', array( "title" => basename(get_attached_file($vr_content_id)))); ?>
								<?php else : ?>
									<img src="/wp-includes/images/media/default.png" title="<?= basename(get_attached_file($vr_content_id)); ?>" width=30 height=30/>
								<?php endif;
							}
							?>
						</td>
					</tr>
				<?php endforeach;?>
			<?php endif; ?>
		<?php endforeach;?>		
	</tbody>
</table>
			