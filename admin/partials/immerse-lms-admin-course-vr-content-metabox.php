<h4>Classes VR Content</h4>
<table class="course-classes-vr-content-list wp-list-table widefat fixed striped media">
	<thead>
		<th class="manage-column column-title column-primary">Class</th>
		<th class="manage-column" width=10%>qty</th>
		<th class="manage-column">VR Content</th>
	</thead>
	<tbody>
		<?php foreach ($classes_ids as $class_id) : ?>
			<?php if ($vr_content_class_num[$class_id]) : ?>	
				<tr class="">
					<td>
						<a href="/wp-admin/post.php?post=<?=$class_id?>&action=edit"><?= get_the_title($class_id)?></a> (<?= get_post_meta($class_id, 'class_public_title', ARRAY_A)?>)
					</td>
					<td>
					<?= $vr_content_class_num[$class_id]?>
					</td>	
					<td>
						<?php
						for ($i=0; $i < $vr_content_class_num[$class_id] ; $i++) { 
							$vr_content_id = get_post_meta($class_id, 'vr_content_'.$i.'_vr_content_file', ARRAY_A);
							echo wp_get_attachment_image( $vr_content_id, array(30,30), '', array( "title" => basename(get_attached_file($vr_content_id))));
						}
						?>
					</td>
				</tr>
			<?php endif; ?>
		<?php endforeach;?>		
	</tbody>
</table>