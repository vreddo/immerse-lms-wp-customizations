<?php
$display_questions = get_current_screen()->action != 'add';
?>
<div id="question_marking_guides" <?= $display_questions ? '' : 'style="display: none;"' ?>>
	<?php
	global $post;
	$quiz_pro_id = apply_filters('immerse_lms_get_quiz_pro_id', get_post_meta($post->ID, '_quiz', true));

	$questionMapper = new WpProQuiz_Model_QuestionMapper();
	$questions = $questionMapper->fetchAll($quiz_pro_id);
	?>
	<?php foreach ($questions as $question): ?>
	<div style="margin-top: 16px;">
		<h3 style="margin-bottom: 0px;"><?= __('Guide for') ?> <?= $question->getTitle() ?></h3>
		<p><?= $question->getQuestion() ?></p>
		<?php
		$meta_key = '_guide_question_' . $question->getQuestionId();
		wp_editor(get_post_meta($post->ID, $meta_key, true), $meta_key, array(
			'textarea_name' => '_question_guides[_guide_question_' . $question->getQuestionId() . ']'
		));
		?>
	</div>
	<?php endforeach ?>
</div>
<h3 id="question_marking_guides_save_message" <?= !$display_questions ? '' : 'style="display: none;"' ?>>
	<?php if ($display_questions): ?>
		Save the post to create guides for the new quiz's questions. (The guides for this quiz will still be saved if you change back)
	<?php else: ?>
		Save the post to create guides for specific questions.
	<?php endif ?>
</h3>