<?php
global $post;
$selected_course = get_post_meta($post->ID, '_course', true);
$selected_quiz = get_post_meta($post->ID, '_quiz', true);
?>
<script>
	jQuery(function($) {
		var loadingCourses = true;
		var selectedQuiz = <?= json_encode($selected_quiz) ?>;

		$("#course").change(function() {
			var course = $(this).val();

			$("#quiz option").show();
			$("#quiz option[data-course-id!=" + course + "]").hide();
			$("#quiz option[value='']").show();

			if ($("#quiz option:selected").is("[style='display: none;']")) {
				$("#quiz").val("");
			}
		});
		
		$("#course").change();

		$("#quiz").change(function() {
			var val = $(this).val();

			if (val && val == selectedQuiz) {
				$("#question_marking_guides").show();
				$("#question_marking_guides_save_message").hide();
			} else {
				$("#question_marking_guides").hide();
				$("#question_marking_guides_save_message").show();
			}
		});
	});
</script>
<?php
$courses = get_posts(array(
	'post_type' => 'sfwd-courses',
	'numberposts' => -1
));

$quizzes = get_posts(array(
	'post_type' => 'sfwd-quiz',
	'numberposts' => -1
));
?>
<table style="width: 100%; margin-top: -8px;" cellspacing="0" cellpadding="0">
	<tr>
		<td style="width: 50%;">
			<label>Course</label>
			<select style="width: 100%;" id="course" name="course" required>
				<?php
				foreach ($courses as $course) {
					?>
					<option value="<?= $course->ID ?>" <?= $selected_course == $course->ID ? 'selected' : '' ?>><?= $course->post_title ?></option>
					<?php
				}
				?>
			</select>
		</td>
		<td style="width: 50%;">
			<label>Quiz</label>
			<select style="width: 100%;" id="quiz" name="quiz" required>
				<option value=""></option>
				<?php
				foreach ($quizzes as $quiz) {
					$course = apply_filters('immerse_lms_get_course_for_quiz', $quiz->ID);
					?>
					<option value="<?= $quiz->ID ?>" data-course-id="<?= $course->ID ?>" <?= $selected_quiz == $quiz->ID ? 'selected' : '' ?>><?= $quiz->post_title ?></option>
					<?php
				}
				?>
			</select>
		</td>
	</tr>
</table>
<h3 style="margin-bottom: 0px;"><?= __('Overall Guide') ?></h3>