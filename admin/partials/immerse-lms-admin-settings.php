<div class="wrap">
	<h1>Immerse LMS Settings</h1>
</div>

<div class="wrap">
<form action="options.php" method="POST">
		<?php
		// output security fields for the registered setting "bnpg_options"
		settings_fields($this->plugin_name);
		// output setting sections and their fields
		// (sections are registered for plugin, each field is registered to a specific section)
		do_settings_sections($this->plugin_name);
		// output save settings button
		submit_button('Save Settings');
		?>
	</form>
</div>