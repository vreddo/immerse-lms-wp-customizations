<div class="wrap">
	<h1>Vreddo Library</h1>
</div>
		
<div>
	<form id="vreddo-licensed-downloads">
		<input id="vreddo-content-nonce" type="hidden" name="vreddo_content_ajax_nonce" value="<?php echo wp_create_nonce('immerse_lms_ajax_vreddo_content');?>" />
		<table>
			<tr>
				<td>
					<label for="email">Email</label>	
				</td>
				<td>
					<input type="text" name="email" id="email" value='' style="width:300px" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="license_key">License Key</label>
				</td>
				<td>
					<input type="text" name="license_key" id="license_key" value='' style="width:300px" required/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="product_id">Product ID</label>
				</td>
				<td>
					<input type="text" name="product_id" id="product_id" value='' style="width:300px" required/>
				</td>
			</tr>
		</table>
		<br/>
		<button id="vreddo-content-submit" type="submit" class="button button-primary alignleft" ><?php _e('Add Vreddo Content')?></button>
		<span class='spinner alignleft'></span>
		<div class="vreddo-content-ajax-msg vreddo-content-ajax-success notice-success alignleft ">VReddo content Saved!</div>
		<div class="vreddo-content-ajax-msg vreddo-content-ajax-warning alignleft alert">VReddo content NOT saved!</div>
		<div class="vreddo-content-ajax-msg vreddo-content-ajax-error alignleft">VReddo content ERROR: Ajax Error</div>
		<div class="clear"></div>
		
		<br/>
		<table id="vreddo-content-list" class="vreddo-content-list wp-list-table widefat fixed striped media">
			<thead>
				<th class="manage-column" width="5%">ID</th>
				<th class="manage-column" width="5%">Thumb</th>
				<th class="manage-column column-title column-primary" width="45%">Name</th>
				<th class="manage-column" width="45%">Date/Time</th>
				
			</thead>
			<tbody>
				<?php 
					
					if ( $vr_content ) :
						foreach ($vr_content->posts as $item) : ?>
							<?php 
							include plugin_dir_path( dirname( __FILE__ ) ) . 'partials/immerse-lms-admin-vreddo-content-item.php'; ?>
						<?php endforeach ?>		
				<?php endif; ?>
				<tr>
						<td colspan=4>
							VReddo items are shown here
						</td>
				</tr>
			</tbody>
		</table>
	</form>
</div>