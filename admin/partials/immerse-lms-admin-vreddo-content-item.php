<tr id="vreddo-item-<?= $item->ID ?>" class="">
	<td>
		<?= $item->ID ?>
	</td>
	<td>
		<?php 
			$meta = wp_get_attachment_metadata($item->ID);
			//print_r($meta);
			if ($meta['_alternative_thumb']) { ?>
				<img src="<?= $meta['_alternative_thumb'] ?>" width=30 height=30/>
			<?php }
			else {
				echo wp_get_attachment_image( $item->ID, array(30,30), '', array( "title" => basename(get_attached_file($item->ID))));
			}
		?>
	
	</td>
	<td>
		<a href="/wp-admin/post.php?post=<?=$item->ID?>&action=edit">
			<?= $item->post_title?>
		</a>
	</td>

	<td>
		<?= $item->post_date?>
	</td>
</tr>