<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              cfeduex.com
 * @since             1.0.0
 * @package           Immerse_Lms
 *
 * @wordpress-plugin
 * Plugin Name:       Immerse LMS Customizations
 * Plugin URI:        cfeduex.com
 * Description:       REQUIRED: Enables Immerse LMS Custom BuddyPress/Learndash/WP/VR Code and Customizations 
 * Version:           1.2.2
 * Author:            CFEDUEX
 * Author URI:        cfeduex.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       immerse-lms
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Make sure that this plugin loads after our required plugins by loading it last.
 */
function immerse_lms_loadlast() {
	// ensure path to this file is via main wp plugin path
	$wp_path_to_this_file = preg_replace('/(.*)plugins\/(.*)$/', WP_PLUGIN_DIR."/$2", __FILE__);
	$this_plugin = plugin_basename(trim($wp_path_to_this_file));
	$active_plugins = get_option('active_plugins');
	$this_plugin_key = array_search($this_plugin, $active_plugins);

	if ($this_plugin_key <= count($active_plugins)) {
		array_splice($active_plugins, $this_plugin_key, 1);
		array_push($active_plugins, $this_plugin);
		update_option('active_plugins', $active_plugins);
	}
}
add_action('activated_plugin', 'immerse_lms_loadlast');

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'IMMERSE_LMS_WP_VERSION', '1.2.2' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-immerse-lms-activator.php
 */
function activate_immerse_lms() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-immerse-lms-activator.php';
	Immerse_Lms_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-immerse-lms-deactivator.php
 */
function deactivate_immerse_lms() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-immerse-lms-deactivator.php';
	Immerse_Lms_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_immerse_lms' );
register_deactivation_hook( __FILE__, 'deactivate_immerse_lms' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-immerse-lms.php';

/**
 * Statistic ref mapper class.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-immerse-lms-wpproquiz-model-statisticrefmapper.php';

/**
 * ACF image aspect ratio validation.
 */
require plugin_dir_path( __FILE__ ) . 'includes/acf-image-aspect-ratio-validation.php';

/**
 * Helper functions.
 */
require plugin_dir_path( __FILE__ ) . 'includes/helpers.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_immerse_lms() {

	$plugin = new Immerse_Lms();
	$plugin->run();

}
run_immerse_lms();
