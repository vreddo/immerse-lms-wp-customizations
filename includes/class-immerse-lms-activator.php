<?php

/**
 * Fired during plugin activation
 *
 * @link       pedrops.com
 * @since      1.0.0
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/includes
 * @author     Pedro Germani <pedro.germani@toptal.com>
 */
class Immerse_Lms_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		
		// gets the subscriber role
		$admins = get_role( 'subscriber' );

		$admins->add_cap( 'edit_vr_user_content' ); 
		$admins->add_cap( 'edit_vr_user_contents' ); 
		$admins->remove_cap( 'edit_other_vr_user_contents' ); 
		$admins->remove_cap( 'publish_vr_user_contents' ); 
		$admins->add_cap( 'read_vr_user_content' ); 
		$admins->remove_cap( 'read_private_vr_user_contents' ); 
		$admins->add_cap( 'delete_vr_user_content' ); 
		
		// gets the admin role
		$admins = get_role( 'administrator' );

		$admins->add_cap( 'edit_vr_user_content' ); 
		$admins->add_cap( 'edit_vr_user_contents' ); 
		$admins->add_cap( 'edit_other_vr_user_contents' ); 
		$admins->add_cap( 'publish_vr_user_contents' ); 
		$admins->add_cap( 'read_vr_user_content' ); 
		$admins->add_cap( 'read_private_vr_user_contents' ); 
		$admins->add_cap( 'delete_vr_user_content' ); 
	}

	/*
	* configure capabilities
	ref: https://wordpress.stackexchange.com/questions/108338/capabilities-and-custom-post-types
	*/

	public function add_vr_user_content_caps() {
		
	}

}
