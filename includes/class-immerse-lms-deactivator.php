<?php

/**
 * Fired during plugin deactivation
 *
 * @link       pedrops.com
 * @since      1.0.0
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/includes
 * @author     Pedro Germani <pedro.germani@toptal.com>
 */
class Immerse_Lms_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		// gets the subscriber role
		$admins = get_role( 'subscriber' );

		$admins->remove_cap( 'edit_vr_user_content' ); 
		$admins->remove_cap( 'edit_vr_user_contents' ); 
		$admins->remove_cap( 'edit_other_vr_user_contents' ); 
		$admins->remove_cap( 'publish_vr_user_contents' ); 
		$admins->remove_cap( 'read_vr_user_content' ); 
		$admins->remove_cap( 'read_private_vr_user_contents' ); 
		$admins->remove_cap( 'delete_vr_user_content' ); 
		
		// gets the admin role
		$admins = get_role( 'administrator' );

		$admins->remove_cap( 'edit_vr_user_content' ); 
		$admins->remove_cap( 'edit_vr_user_contents' ); 
		$admins->remove_cap( 'edit_other_vr_user_contents' ); 
		$admins->remove_cap( 'publish_vr_user_contents' ); 
		$admins->remove_cap( 'read_vr_user_content' ); 
		$admins->remove_cap( 'read_private_vr_user_contents' ); 
		$admins->remove_cap( 'delete_vr_user_content' ); 
	}

}
