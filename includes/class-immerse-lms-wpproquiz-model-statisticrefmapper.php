<?php
class ImmerseLms_Model_StatisticRefMapper extends WpProQuiz_Model_StatisticRefMapper
{
    public function getQuizAttemptTime($refId)
    {
        return intval($this->_wpdb->get_col($this->_wpdb->prepare('
      SELECT SUM(question_time)
      FROM
        ' . $this->_tableStatistic . '
      WHERE
        statistic_ref_id = %d
    ', $refId))[0]);
    }

    public function fetchById($id)
    {
        $result = $this->_wpdb->get_results(
            $this->_wpdb->prepare('
      SELECT
        u.`user_login`, u.`display_name`, u.ID AS user_id,
        sf.*,
        SUM(s.correct_count) AS correct_count,
        SUM(s.incorrect_count) AS incorrect_count,
        SUM(s.points) AS points,
        SUM(q.points) AS g_points
      FROM
        ' . $this->_tableStatisticRef . ' AS sf
        INNER JOIN ' . $this->_tableStatistic . ' AS s ON(s.statistic_ref_id = sf.statistic_ref_id)
        LEFT JOIN ' . $this->_wpdb->users . ' AS u ON(u.ID = sf.user_id)
        LEFT JOIN ' . $this->_tableQuestion . ' AS q ON(q.id = s.question_id)
      WHERE
        sf.statistic_ref_id = %d
      GROUP BY
        sf.statistic_ref_id
      ORDER BY
        sf.create_time DESC
      ', $id)
            , ARRAY_A);

        if (count($result)) {
            return new WpProQuiz_Model_StatisticHistory($result[0]);
        } else {
            return null;
        }
    }

    public function fetchAssessments()
    {
        $result = $this->_wpdb->get_results(
            '
      SELECT
        u.`user_login`, u.`display_name`, u.ID AS user_id,
        sf.*,
        SUM(s.correct_count) AS correct_count,
        SUM(s.incorrect_count) AS incorrect_count,
        SUM(s.points) AS points,
        SUM(q.points) AS g_points
      FROM
        ' . $this->_tableStatisticRef . ' AS sf
        INNER JOIN ' . $this->_tableStatistic . ' AS s ON(s.statistic_ref_id = sf.statistic_ref_id)
        LEFT JOIN ' . $this->_wpdb->users . ' AS u ON(u.ID = sf.user_id)
        LEFT JOIN ' . $this->_tableQuestion . ' AS q ON(q.id = s.question_id)
        LEFT JOIN
        (
        SELECT max(meta_id) AS meta_id,
          max(post_id) AS post_id,
          meta_key,
          meta_value
        FROM     ' . $this->_wpdb->postmeta . ' pm
        INNER JOIN ' . $this->_wpdb->posts . ' post ON pm.post_id = post.ID AND post.post_type = \'sfwd-quiz\'
        WHERE    meta_key = \'quiz_pro_id\'
        GROUP BY meta_key, meta_value) AS qpm ON qpm.meta_value = sf.quiz_id
        LEFT JOIN ' . $this->_wpdb->term_relationships . ' qtr ON qtr.object_id = qpm.post_id
        LEFT JOIN ' . $this->_wpdb->term_taxonomy . ' qtx ON qtx.term_taxonomy_id = qtr.term_taxonomy_id
        LEFT JOIN ' . $this->_wpdb->terms . ' qt ON qt.term_id = qtx.term_id
      WHERE
          qtx.taxonomy = \'category\' AND
          qt.slug = \'assessments\'
      GROUP BY
        sf.statistic_ref_id
      ORDER BY
        sf.create_time DESC
      '
            , ARRAY_A);

        $r = array();

        foreach ($result as $row) {
            if (!empty($row['user_login'])) {
                $row['user_name'] = $row['user_login'] . ' (' . $row['display_name'] . ')';
            }

            $r[] = new WpProQuiz_Model_StatisticHistory($row);
        }

        return $r;
    }

    public function fetchByUser($userId)
    {
        $result = $this->_wpdb->get_results(
            $this->_wpdb->prepare('
      SELECT
        u.`user_login`, u.`display_name`, u.ID AS user_id,
        sf.*,
        SUM(s.correct_count) AS correct_count,
        SUM(s.incorrect_count) AS incorrect_count,
        SUM(s.points) AS points,
        SUM(q.points) AS g_points
      FROM
        ' . $this->_tableStatisticRef . ' AS sf
        INNER JOIN ' . $this->_tableStatistic . ' AS s ON(s.statistic_ref_id = sf.statistic_ref_id)
        LEFT JOIN ' . $this->_wpdb->users . ' AS u ON(u.ID = sf.user_id)
        LEFT JOIN ' . $this->_tableQuestion . ' AS q ON(q.id = s.question_id)
      WHERE
        sf.user_id = %d
      GROUP BY
        sf.statistic_ref_id
      ORDER BY
        sf.create_time DESC
      ', $userId)
            , ARRAY_A);

        $r = array();

        foreach ($result as $row) {
            if (!empty($row['user_login'])) {
                $row['user_name'] = $row['user_login'] . ' (' . $row['display_name'] . ')';
            }

            $r[] = new WpProQuiz_Model_StatisticHistory($row);
        }

        $meta_quizzes = array_filter($this->getUserMetaQuizData($userId), function ($quiz) use ($r) {
            foreach ($r as $q) {
                if ($q->getQuizId() == $quiz->getQuizId()) {
                    return false;
                }
            }

            return true;
        });

        $r = array_merge($r, $meta_quizzes);

        return $r;
    }

    public function getUserQuizAttempt($userId, $quizId)
    {
        $attempts = $this->getUserQuizAttempts($userId, $quizId);

        if (count($attempts) > 0) {
            return $attempts[0];
        }

        return null;
    }

    public function getUserMetaQuizData($userId)
    {
        $usermeta = get_user_meta($userId, '_sfwd-quizzes', true);
        $quizzes = empty($usermeta) ? false : $usermeta;
        $returned = array();

        if ($quizzes) {
            $user = get_user_by('id', $userId);

            foreach ($quizzes as $quiz) {
                $row = array(
                    'user_login' => $user->user_login,
                    'display_name' => $user->display_name,
                    'statistic_ref_id' => $quiz['statistic_ref_id'],
                    'quiz_id' => get_post_meta($quiz['quiz'], 'quiz_pro_id', true),
                    'user_id' => $user->ID,
                    'create_time' => $quiz['time'],
                    'is_old' => 0,
                    'form_data' => null,
                    'correct_count' => $quiz['count'],
                    'incorrect_count' => 0,
                    'points' => $quiz['total_points'] == 0 ? $quiz['percentage'] : $quiz['points'],
                    'g_points' => $quiz['total_points'] == 0 ? 100 : $quiz['total_points'],
                );

                $returned[] = new WpProQuiz_Model_StatisticHistory($row);
            }
        }

        return $returned;
    }

    public function getUserQuizAttempts($userId, $quizId = null)
    {
        $result = $this->_wpdb->get_results(
            $this->_wpdb->prepare('
      SELECT
        u.`user_login`, u.`display_name`, u.ID AS user_id,
        sf.*,
        SUM(s.correct_count) AS correct_count,
        SUM(s.incorrect_count) AS incorrect_count,
        SUM(s.points) AS points,
        SUM(q.points) AS g_points
      FROM
        ' . $this->_tableStatisticRef . ' AS sf
        LEFT JOIN ' . $this->_tableStatistic . ' AS s ON(s.statistic_ref_id = sf.statistic_ref_id)
        LEFT JOIN ' . $this->_wpdb->users . ' AS u ON(u.ID = sf.user_id)
        LEFT JOIN ' . $this->_tableQuestion . ' AS q ON(q.id = s.question_id)
        INNER JOIN
          (SELECT user_id AS uid, quiz_id AS qid, MAX(statistic_ref_id) AS statistic_ref_id FROM ' . $this->_tableStatisticRef . ' GROUP BY quiz_id, user_id) sfm
          ON (sf.statistic_ref_id = sfm.statistic_ref_id)
      WHERE
        sf.user_id = %d '
                . ($quizId ? ' AND sf.quiz_id = %d ' : '') .
                '
      GROUP BY
        sf.statistic_ref_id
      ORDER BY
        sf.create_time DESC
      ', $userId, $quizId)
            , ARRAY_A);

        $r = array();

        foreach ($result as $row) {
            if (!empty($row['user_login'])) {
                $row['user_name'] = $row['user_login'] . ' (' . $row['display_name'] . ')';
            }

            $r[] = new WpProQuiz_Model_StatisticHistory($row);
        }

        $meta_quizzes = $this->getUserMetaQuizData($userId);

        $meta_quizzes = array_unique_callback($meta_quizzes, function ($quiz) {
            return $quiz->getQuizId();
        });

        $meta_quizzes = array_filter($meta_quizzes, function ($quiz) use ($r, $quizId) {
            if ($quiz->getQuizId() != $quizId) {
                return false;
            }

            foreach ($r as $q) {
                if ($q->getQuizId() == $quiz->getQuizId()) {
                    return false;
                }
            }

            return true;
        });

        $r = array_merge($r, $meta_quizzes);

        return $r;
    }

    public function getPreviousUserQuizAttempts($userId, $quizId, $latestRef)
    {
        $result = $this->_wpdb->get_results(
            $this->_wpdb->prepare('
      SELECT
        u.`user_login`, u.`display_name`, u.ID AS user_id,
        sf.*,
        SUM(s.correct_count) AS correct_count,
        SUM(s.incorrect_count) AS incorrect_count,
        SUM(s.points) AS points,
        SUM(q.points) AS g_points
      FROM
        ' . $this->_tableStatisticRef . ' AS sf
        INNER JOIN ' . $this->_tableStatistic . ' AS s ON(s.statistic_ref_id = sf.statistic_ref_id)
        LEFT JOIN ' . $this->_wpdb->users . ' AS u ON(u.ID = sf.user_id)
        INNER JOIN ' . $this->_tableQuestion . ' AS q ON(q.id = s.question_id)
      WHERE
        sf.user_id = %d AND sf.quiz_id = %d AND sf.statistic_ref_id < %d
      GROUP BY
        sf.statistic_ref_id
      ORDER BY
        sf.create_time DESC
      ', $userId, $quizId, $latestRef)
            , ARRAY_A);

        $r = array();

        foreach ($result as $row) {
            if (!empty($row['user_login'])) {
                $row['user_name'] = $row['user_login'] . ' (' . $row['display_name'] . ')';
            }

            $r[] = new WpProQuiz_Model_StatisticHistory($row);
        }

        return $r;
    }

    public function overrideStatScore($statisticRefId, $questionId, $points)
    {
        $this->_wpdb->query(
            $this->_wpdb->prepare('
    UPDATE
      ' . $this->_tableStatistic . '
    SET
      points=%d
    WHERE
      statistic_ref_id = %d AND
      question_id = %d
    ', $points, $statisticRefId, $questionId)
        );
    }
}
