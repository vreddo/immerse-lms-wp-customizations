<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       pedrops.com
 * @since      1.0.0
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/includes
 * @author     Pedro Germani <pedro.germani@toptal.com>
 */
class Immerse_Lms {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Immerse_Lms_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'IMMERSE_LMS_WP_VERSION' ) ) {
			$this->version = IMMERSE_LMS_WP_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'immerse-lms';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_recurring_schedules();

		// Make sure we at least have a readable session to begin with.
		session_start();
		session_write_close();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Immerse_Lms_Loader. Orchestrates the hooks of the plugin.
	 * - Immerse_Lms_i18n. Defines internationalization functionality.
	 * - Immerse_Lms_Admin. Defines all hooks for the admin area.
	 * - Immerse_Lms_Public. Defines all hooks for the public side of the site.
	 * - immerse-lms-cpt. Defines all custom post types.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-immerse-lms-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-immerse-lms-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-immerse-lms-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the admin VReddo area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-immerse-lms-vreddo.php';

		/**
		 * The class responsible for handling email sending & templating.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-immerse-lms-email.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-immerse-lms-public.php';

		$this->loader = new Immerse_Lms_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Immerse_Lms_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Immerse_Lms_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
		$plugin_admin = new Immerse_Lms_Admin( $this->get_plugin_name(), $this->get_version() );
		$plugin_vreddo = new Immerse_Lms_VReddo( $this->get_plugin_name(), $this->get_version() );

		// Add default plugin admin scripts and styles
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_vreddo, 'enqueue_scripts' );
		
		// Add extra data on user profile
		$this->loader->add_action( 'show_user_profile', $plugin_admin, 'extra_user_profile_fields' );
		$this->loader->add_action( 'edit_user_profile', $plugin_admin, 'extra_user_profile_fields' );
		
		// Adds custom menus
		$this->loader->add_action( 'admin_menu',  $plugin_admin, 'immerse_lms_class_options_create_menu');
		$this->loader->add_action( 'admin_init', $plugin_admin, 'register_immerse_lms_settings' );
		//$this->loader->add_filter('learndash_submenu', $plugin_admin, 'immerse_lms_add_submenu_ld', 10, 1);

		// Add activity to newly registered users, so they can be found at search.
		$this->loader->add_action( 'user_register', $plugin_admin, 'immerse_lms_user_registration_save', 10, 1 );

		// Redirect away from submitted essays screen.
		$this->loader->add_action( 'current_screen', $plugin_admin, 'immerse_lms_redirect_from_essays_screen', 10, 1 );

		// Allow other mime types to be uploaded to the media library.
		$this->loader->add_filter( 'upload_mimes', $plugin_admin, 'immerse_lms_upload_mimes', 1, 1 );

		// Validate max width for 2D videos.
		$this->loader->add_filter( 'acf/validate_attachment/name=vr_content_file_video', $plugin_admin, 'immerse_lms_validate_vr_video', 10, 4 );
		$this->loader->add_filter( 'acf/validate_attachment/name=vr_content_file_video_360', $plugin_admin, 'immerse_lms_validate_vr_video', 10, 4 );
		
		// Only allow one level of sub-courses.
		$this->loader->add_filter('page_attributes_dropdown_pages_args', $plugin_admin, 'immerse_lms_filter_parent_course_dropdown', 10, 1);

		// add VR actions and filters only if VR is Enabled.
		if (!$plugin_admin->options['immerse_lms_field_enable_vr']) return;
		else {
			// Theme template filter
			$this->loader->add_filter( 'immerse_lms_get_user_classes', $plugin_admin, 'immerse_lms_get_user_classes', 10, 2 );
			
			// filter students field on Course classes only for students for the selected course
			$this->loader->add_filter('acf/fields/user/query/name=class_students', $plugin_admin, 'immerse_lms_class_students_filter', 10, 3);

			// Adds custom metaboxes
			$this->loader->add_action( 'add_meta_boxes_sfwd-courses', $plugin_admin, 'course_add_meta_boxes' );
			$this->loader->add_action( 'add_meta_boxes_course-class', $plugin_vreddo, 'class_add_meta_boxes' );
			$this->loader->add_action( 'save_post', $plugin_vreddo, 'class_vr_meta_boxe_save' );
			$this->loader->add_filter( 'wp_mime_type_icon', $plugin_vreddo, 'immerse_lms_change_mime_icon', 10, 3);

			// add custom data to timezone profile field.
			$this->loader->add_action( 'bp_init', $plugin_admin, 'bp_add_custom_xprofile_timezone_list' );

			// add custom data to timezone profile field.
			$this->loader->add_action( 'bp_init', $plugin_admin, 'bp_add_custom_xprofile_headset_id' );

			// add Vreddo content Ajax
			$this->loader->add_action( 'wp_ajax_immerse_lms_ajax_add_vreddo_content', $plugin_vreddo, 'immerse_lms_ajax_add_vreddo_content' );

			// add Vreddo content Ajax to update content table
			$this->loader->add_action( 'wp_ajax_immerse_lms_ajax_get_vreddo_content_table_item', $plugin_vreddo, 'immerse_lms_ajax_get_vreddo_content_table_item' );

			$this->loader->add_action( 'rest_api_init', $plugin_vreddo, 'immerse_lms_vreddo_library_rest_api_init');
		}

		// Add Markers and assessment actions and filters
		$this->loader->add_action( 'wp_ajax_immerse_lms_script', $plugin_admin, 'immerse_lms_run_script' );
		$this->loader->add_action( 'show_user_profile', $plugin_admin, 'immerse_lms_marker_students_field' );
		$this->loader->add_action( 'edit_user_profile', $plugin_admin, 'immerse_lms_marker_students_field' );
		$this->loader->add_action( 'personal_options_update', $plugin_admin, 'immerse_lms_save_marker_students_field' );
		$this->loader->add_action( 'edit_user_profile_update', $plugin_admin, 'immerse_lms_save_marker_students_field' );

		$this->loader->add_action($this->get_plugin_name() . '_every_hour', $plugin_admin, 'immerse_lms_send_late_course_notifications');
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		$plugin_public = new Immerse_Lms_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action('wp_footer', $plugin_public, 'immerse_lms_check_enable_online_notifications', 10);
		$this->loader->add_action( 'wp_login', $plugin_public, 'immerse_lms_last_login', 10, 2 );
		$this->loader->add_action( 'bp_after_profile_loop_content', $plugin_public, 'immerse_lms_bp_profile_content', 10, 0 );
		
		$this->loader->add_filter('immerse_lms_get_last_statistic_ref_id', $plugin_public, 'immerse_lms_get_last_statistic_ref_id', 10, 2);
		$this->loader->add_filter('immerse_lms_count_statistic_refs', $plugin_public, 'immerse_lms_count_statistic_refs', 10, 2);
		$this->loader->add_filter('immerse_lms_get_quiz_pro_id', $plugin_public, 'immerse_lms_get_quiz_pro_id', 10, 1);
		$this->loader->add_filter('immerse_lms_get_user_course_progress', $plugin_public, 'immerse_lms_get_user_course_progress', 10, 2);
		$this->loader->add_filter('immerse_lms_get_course_steps_hierarchy', $plugin_public, 'immerse_lms_get_course_steps_hierarchy', 10, 2);
		$this->loader->add_filter('immerse_lms_get_course_for_quiz', $plugin_public, 'immerse_lms_get_course_for_quiz', 10, 2);
		$this->loader->add_filter('immerse_lms_get_lesson_for_quiz', $plugin_public, 'immerse_lms_get_lesson_for_quiz', 10, 2);
		$this->loader->add_filter('immerse_lms_learndash_lesson_progress', $plugin_public, 'immerse_lms_learndash_lesson_progress', 10, 3);
		$this->loader->add_filter('immerse_lms_quiz_lesson_complete', $plugin_public, 'immerse_lms_quiz_lesson_complete', 10, 2);
		$this->loader->add_filter('immerse_lms_learndash_course_progress', $plugin_public, 'immerse_lms_learndash_course_progress', 10, 1);
		$this->loader->add_filter('immerse_lms_displayed_user_name', $plugin_public, 'immerse_lms_displayed_user_name', 10, 1);
		$this->loader->add_filter('immerse_lms_get_max_quiz_attempts', $plugin_public, 'immerse_lms_get_max_quiz_attempts', 10, 2);
		$this->loader->add_filter('immerse_lms_format_duration', $plugin_public, 'immerse_lms_format_duration', 10, 1);
		$this->loader->add_filter('immerse_lms_get_quiz_data', $plugin_public, 'immerse_lms_get_quiz_data', 10, 1);
		$this->loader->add_filter('the_content', $plugin_public, 'immerse_lms_prefill_quiz_data', 10, 1);
		$this->loader->add_filter('learndash_quiz_content', $plugin_public, 'immerse_lms_lock_quiz_content', 10000, 1);
		$this->loader->add_filter('immerse_lms_quiz_locked', $plugin_public, 'immerse_lms_quiz_locked', 10, 2);
		$this->loader->add_filter('bp_notifications_get_registered_components', $plugin_public, 'immerse_lms_notifications_get_registered_components', 10, 1);

		// We need to pre-read the "action" because this filter normally will return null, this stores the action before the buddypress action has a chance to change it to null.
		$this->loader->add_filter('bp_notifications_get_notifications_for_user', $plugin_public, 'immerse_lms_pre_read_format_buddypress_notifications', 0, 5);
		$this->loader->add_filter('bp_notifications_get_notifications_for_user', $plugin_public, 'immerse_lms_format_buddypress_notifications', 20, 5);
		$this->loader->add_filter('immerse_lms_add_notification', $plugin_public, 'immerse_lms_add_notification', 0, 4);
		$this->loader->add_filter('learndash_post_args', $plugin_public, 'immerse_lms_learndash_post_args', 10, 1 );
		// Load AJAX methods.
		foreach (get_class_methods('Immerse_Lms_Public') as $method) {
			if ($method == 'immerse_lms_ajax_success' || $method == 'immerse_lms_ajax_failure') {
				continue;
			}

			if (strpos($method, 'immerse_lms_ajax_') === 0) {
				$this->loader->add_action('wp_ajax_' . $method, $plugin_public, $method);
			}

			if (strpos($method, 'immerse_lms_ajax_public_') === 0) {
				$this->loader->add_action('wp_ajax_nopriv_' . $method, $plugin_public, $method);
			}
		}

		// Intercepts completed quizzes
		$this->loader->add_action( 'wp_ajax_wp_pro_quiz_completed_quiz', $plugin_public, 'immerse_lms_wp_pro_quiz_completed_quiz', 1 );
		
		// filter course listing to only list root courses.
		$this->loader->add_action('pre_get_posts', $plugin_public, 'immerse_lms_root_courses_only', 10, 1);

		// Add hierarchy to courses
		$this->loader->add_action('registered_post_type', $plugin_public, 'immerse_lms_add_hierarchy', 10, 1);
		$this->loader->add_action('wp_footer', $plugin_public, 'immerse_lms_show_masqueraded', 99);
	}

	/**
	 * Register all of the scheduled events for the plugin.
	 * 
	 * @since 1.0.0
	 * @access private
	 */
	private function define_recurring_schedules() {
		$prefix = $this->get_plugin_name();

		if (!wp_next_scheduled($prefix . '_every_hour')) {
			wp_schedule_event(time(), 'hourly', $prefix . '_every_hour');
		}

		if (!wp_next_scheduled($prefix . '_twice_daily')) {
			wp_schedule_event(time(), 'twicedaily', $prefix . '_twice_daily');
		}

		if (!wp_next_scheduled($prefix . '_every_day')) {
			wp_schedule_event(time(), 'daily', $prefix . '_every_day');
		}
	}
	
	
	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Immerse_Lms_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
