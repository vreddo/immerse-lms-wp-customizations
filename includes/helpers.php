<?php
/**
 * A helper function that returns unique items in an array based on a callback.
 */
if (!function_exists('array_unique_callback')) {
	function array_unique_callback(array $arr, callable $callback, $strict = false) {
		return array_filter(
			$arr,
			function ($item) use ($strict, $callback) {
				static $haystack = array();
				$needle = $callback($item);
				if (in_array($needle, $haystack, $strict)) {
					return false;
				} else {
					$haystack[] = $needle;
					return true;
				}
			}
		);
	}
}

/**
 * Helper functions for dealing with assessment grades.
 */
function assessment_render_answer($stat) {
    switch ($stat['question']->getAnswerType()) {
        case 'multiple':
        case 'single':
            ?>
            <?php
            foreach ($stat['question']->getAnswerData() as $i => $answer) {

                ?>
                <div class="options" <?php if($answer->isCorrect()): ?>style="background-color: #abffab;"<?php endif ?>>
                    <div class="option-user-answer <?php if($stat['answer'][$i]): ?>option-user-answered<?php endif ?>"></div>
                    <div class="option-choices"><?= $answer->getAnswer() ?></div>
                </div>
                <?php
            }
            ?>
            <?php
            break;
        case 'matrix_sort_answer':
            ?>
            <div class="assessment-grading-sheet-student-answer">Correct Answer:</div>
            <table class="matrix_sort_answer">
                <tbody>
                    <?php
                    foreach ($stat['question']->getAnswerData() as $answer) {
                        ?>
                        <tr>
                            <td style="border-right: 1px solid rgba(0, 0, 0, 0.11);"><?= $answer->getAnswer() ?></td>
                            <td style="width: 70%;"><?= $answer->getSortString() ?></td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
			break;
        case 'essay':
            $answer = $stat['answer'];

            if ($answer['type'] == 'text') {
                echo $answer['answer'];
            } else {
                ?>
                <a href="<?= $answer['answer'] ?>">Download answer</a>
                <?php
            }
			break;
    }
}

function create_assessment_grading($statistic_ref_id) {
    $assessment_grading_id = wp_insert_post(array(
        'post_status' => 'unread',
        'post_title' => '',
        'post_content' => '',
        'post_type' => 'assessment_grading'
    ), true);

    if (is_wp_error($assessment_grading_id)) {
        return $assessment_grading_id;
    }

    update_post_meta($assessment_grading_id, '_statistic-ref-id', $statistic_ref_id);
    return get_assessment_grading($assessment_grading_id);
}

function update_assessment_grading($assessment_grading_id, $update) {
    require_once 'wp-admin/includes/file.php';

    if (!get_assessment_grading($assessment_grading_id)['is_latest']) {
        wp_die('This is not the latest attempt for this quiz by this student.');
    }

    $stat_mapper = new ImmerseLms_Model_StatisticRefMapper();
    $stats_mapper = new WpProQuiz_Model_StatisticMapper();
    $question_mapper = new WpProQuiz_Model_QuestionMapper();

    $statistic_ref_id = get_post_meta($assessment_grading_id, '_statistic-ref-id', true);
    $statistic_ref = $stat_mapper->fetchById($statistic_ref_id);
    $stats = $stats_mapper->fetchAllByRef($statistic_ref_id);

    $post_update = array();

    if (isset($update['trainer_name'])) {
        $post_update['post_title'] = $update['trainer_name'];
    }

    if (isset($update['trainer_notes'])) {
        $post_update['post_content'] = $update['trainer_notes'];
    }
    
    if (isset($update['trainer_signature'])) {
        $file = wp_handle_upload($update['trainer_signature'], array('test_form' => FALSE));
        
        if (!isset($file['error'])) {
            update_post_meta($assessment_grading_id, '_trainer-signature', $file['url']);
        }
    }

    $assessment_grading = get_post($assessment_grading_id);

    if ($assessment_grading->post_status == 'unread') {
        $post_update['post_status'] = 'read';
    }

    if (isset($update['answers'])) {
        foreach ($update['answers'] as $stat_id => $stat) {
            $mark = $stat['mark'];
            $points = 0;
            
            $results = array_filter($stats, function($item) use ($stat_id) { return $item->getQuestionId() == $stat_id; });

            foreach ($results as $s) {
                $answer_data = $s->getAnswerData();
                $answer_data = $answer_data ? json_decode($answer_data) : null;

                if ($mark == 'correct') {
                    $question = $question_mapper->fetchById($s->getQuestionId(), false);
                    $points = $question->getPoints();
                }

                // Update the essay data.
                $post_id = $answer_data->graded_id;

                $post = get_post($post_id);
                $q_id		= get_post_meta( $post_id, 'quiz_id', true );
                $qs_id  	= get_post_meta( $post_id, 'question_id', true );
                
                $_POST['quiz_id'] = $q_id;
                $_POST['question_id'] = $qs_id;
                $_POST['post_status'] = 'graded';
                $_POST['original_points_awarded'] = 0;
                $_POST['points_awarded'] = $points;

                wp_update_post(array(
                    'ID' => $post_id,
                    'post_status' => 'graded'
                ));
            }
        }
    }

    if (count($post_update)) {
        $post_update['ID'] = $assessment_grading_id;
        wp_update_post($post_update);
    }

    $grading = get_assessment_grading($assessment_grading_id);

    if ($grading['grade'] != 'ungraded' && $grading['status'] != 'graded') {
        wp_update_post(array(
            'ID' => $assessment_grading_id,
            'post_status' => 'graded'
        ));

        $grading['status'] = 'graded';
    }

    if ($grading['status'] == 'graded' && $grading['trainer_name'] && $grading['trainer_signature']) {
        assessment_send_graded_email($grading);
    }

    return $grading;
}

function assessment_send_graded_email($grading) {
    $assessment_grade = 'Not Yet Competent';

    if ($grading['grade'] == 'competent') {
        $assessment_grade = 'Competent';
    }

    $quiz_title = $grading['quiz']->post_title;
    $user = $grading['student'];
    $quiz_link = get_the_permalink($grading['quiz']);

    // Check if there's a course attached.
    if (isset($grading['course']) && $grading['course']) {
        // The quiz link is different for this.
        $quiz_slug = $grading['quiz']->post_name;
        $course_link = get_the_permalink($grading['course']);
        $quiz_link = $course_link . 'quizzes/' . $quiz_slug . '/';
    }

    Immerse_Lms_Email::send_email($user->user_email, 'assessment_graded', array(
        'user_name' => $user->display_name,
        'quiz_title' => $grading['quiz']->post_title,
        'assessment_grade' => $assessment_grade,
        'trainer_notes' => $grading['trainer_notes'],
        'retake' => $grading['grade'] != 'competent',
        'retake_link' => $quiz_link . '?gid=' . $grading['grading']->ID
    ));
}

function get_assessment_grading($assessment_grading_id) {
    global $wpdb;

    $assessment_grading = get_post($assessment_grading_id);
    $statistic_ref_id = get_post_meta($assessment_grading_id, '_statistic-ref-id', true);
    $signature = get_post_meta($assessment_grading_id, '_trainer-signature', true);
    $grade = 'competent';

    $stat_mapper = new ImmerseLms_Model_StatisticRefMapper();
    $stats_mapper = new WpProQuiz_Model_StatisticMapper();
    $quiz_mapper = new WpProQuiz_Model_QuizMapper();
    $question_mapper = new WpProQuiz_Model_QuestionMapper();

    $statistic_ref = $stat_mapper->fetchById($statistic_ref_id);

    if (!$statistic_ref) {
        return;
    }

    $student_id = $statistic_ref->getUserId();
    $student = get_user_by('id', $student_id);
    $quiz_pro_id = $statistic_ref->getQuizId();
    $quiz_id = learndash_get_quiz_id_by_pro_quiz_id($quiz_pro_id);
    $quiz = get_post($quiz_id);
    $course = apply_filters('immerse_lms_get_course_for_quiz', $quiz_id, $student_id);

    $wpdb->statistic_ref = $wpdb->prefix . 'wp_pro_quiz_statistic_ref';

    // Get the latest statistic ref for the same user and quiz.
    $last_ref_id = $wpdb->get_col("
    SELECT
        statistic_ref_id
    FROM
        $wpdb->statistic_ref
    WHERE
        quiz_id = $quiz_pro_id AND
        user_id = $student_id
    ORDER BY
        statistic_ref_id DESC
    LIMIT 1
    ")[0];

    $stats = array();

    foreach ($stats_mapper->fetchAllByRef($statistic_ref_id) as $stat) {
        $question = $question_mapper->fetchById($stat->getQuestionId(), 0);

        if ($question) {
            $s = array();

            $s['question'] = $question;

            $answer_data = $stat->getAnswerData();
            $s['mark'] = 'none';
            $s['answer'] = $answer_data ? json_decode($answer_data) : null;

            if ($question->getAnswerType() == 'essay') {
                $answer_id = $s['answer']->graded_id;
                $answer_post = get_post($answer_id);
                $upload = get_post_meta($answer_id, 'upload', true);

                if ($upload) {
                    $s['answer'] = array(
                        'type' => 'upload',
                        'answer' => $upload
                    );
                } else {
                    $s['answer'] = array(
                        'type' => 'text',
                        'answer' => $answer_post ? $answer_post->post_content : null
                    );
                }

                $points_available = $question->getPoints();
                $q_id		= get_post_meta( $answer_id, 'quiz_id', true );
                $qs_id  	= get_post_meta( $answer_id, 'question_id', true );
                $essay_data = learndash_get_submitted_essay_data( $q_id, $qs_id, $answer_post );
                
                if ($essay_data['status'] == 'graded') {
                    if ($essay_data['points_awarded'] >= $points_available) {
                        $s['mark'] = 'correct';
                    } else {
                        $s['mark'] = 'incorrect';
                    }

                    $s['last_graded'] = $answer_post->post_modified_gmt;
                }
            } else {
                if ($stat->getCorrectCount() > 0) {
                    $s['mark'] = 'correct';
                }
        
                if ($stat->getIncorrectCount() > 0) {
                    $s['mark'] = 'incorrect';
                }

                $s['last_graded'] = date('Y-m-d H:m:s', $statistic_ref->getCreateTime());
            }

            $stats[$stat->getQuestionId()] = $s;

            if ($s['mark'] == 'none') {
                $grade = 'ungraded';
            }

            if ($grade == 'competent' && $s['mark'] == 'incorrect') {
                $grade = 'notcompetent';
            }
        }
    }

    $largest_date = '';

    foreach ($stats as $stat) {
        if (isset($stat['last_graded'])) {
            if ($stat['last_graded'] > $largest_date) {
                $largest_date = $stat['last_graded'];
            }
        }
    }

    if ($assessment_grading->post_status != 'graded' && $grade != 'ungraded') {
        $assessment_grading->post_status = 'graded';

        wp_update_post(array(
            'ID' => $assessment_grading->ID,
            'post_status' => 'graded'
        ));
    }

    return array(
        'student' => $student,
        'quiz' => $quiz,
        'course' => $course,
        'grading' => $assessment_grading,
        'status' => $assessment_grading->post_status,
        'grade' => $grade,
        'date' => $assessment_grading->post_modified_gmt,
        'last_graded_date' => $largest_date,
        'trainer_name' => $assessment_grading->post_title,
        'trainer_signature' => $signature,
        'trainer_notes' => $assessment_grading->post_content,
        'answers' => $stats,
        'is_latest' => $last_ref_id == $statistic_ref_id,
        'statistic_ref_id' => $statistic_ref_id
    );
}