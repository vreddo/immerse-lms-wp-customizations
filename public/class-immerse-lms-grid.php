<?php
/**
 * Reusable public grid system which includes rendering, filtering, and sorting
 * as implemented in sub classes.
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/grid
 * @author     JC Gurango <jc@jcgurango.com>
 */
class Immerse_Lms_Grid
{
    public function get_filters() {
        return array();
    }

    public function get_columns() {
        return array();
    }

    public function get_rows($filters, $sort, $page = 1) {
        return array();
    }

    public function count_pages($filters, $sort) {
        return 1;
    }

    public function render_filter($filter) {
        $filters = isset($_GET['filters']) ? $_GET['filters'] : array();
        $value = null;

        if (isset($filters[$filter['name']])) {
            $value = $filters[$filter['name']];
        }

        switch ($filter['type']) {
            case 'text':
                ?>
                <input type="text" name="filters[<?= esc_attr($filter['name']) ?>]" value="<?= esc_attr($value) ?>" style="width: 100%; height: 40px;" />
                <?php
                break;
            case 'date_fromto':
                ?>
                <input type="text" name="filters[<?= esc_attr($filter['name']) ?>][from]" value="<?= $value ? esc_attr($value['from']) : '' ?>" style="width: 50%; height: 40px; float: left;" />
                <input type="text" name="filters[<?= esc_attr($filter['name']) ?>][to]" value="<?= $value ? esc_attr($value['to']) : '' ?>" style="width: 50%; height: 40px; float: left;" />
                <script>
                    jQuery(function($) {
                        $("input[name='filters[<?= esc_attr($filter['name']) ?>][from]']").datepicker({ autoHide: true, format: 'yyyy-mm-dd' });
                        $("input[name='filters[<?= esc_attr($filter['name']) ?>][to]']").datepicker({ autoHide: true, format: 'yyyy-mm-dd' });
                    });
                </script>
                <?php
                break;
            case 'select':
                ?>
                <select name="filters[<?= esc_attr($filter['name']) ?>]">
                    <option value=""></option>
                    <?php foreach ($filter['options'] as $val => $label) : ?>
                    <option value="<?= esc_attr($val) ?>" <?=  $val === $value ? 'selected' : '' ?>><?= esc_html($label) ?></option>
                    <?php endforeach ?>
                </select>
                <?php
                break;
        }
    }

    public function get_filter_values() {
        $values = array();

        foreach ((isset($_GET['filters']) ? $_GET['filters'] : array()) as $key => $filter_value) {
            $current_filter = null;

            foreach ($this->get_filters() as $filter) {
                if ($filter['name'] == $key) {
                    $current_filter = $filter;
                }
            }

            if ($current_filter) {
                if ($current_filter['type'] == 'date_fromto') {
                    if (isset($filter_value['from']) && $filter_value['from']) {
                        $values[$key . '_from'] = $filter_value['from'];
                    }

                    if (isset($filter_value['to']) && $filter_value['to']) {
                        $values[$key . '_to'] = $filter_value['to'];
                    }
                } else {
                    if ($filter_value) {
                        $values[$key] = $filter_value;
                    }
                }
            }
        }

        return $values;
    }

    public function render_cell($column, $row) {
        $column['func']($row);
    }

    public function render() {
        $filters = $this->get_filters();
        $columns = $this->get_columns();
        $filter = $this->get_filter_values();
        $sort = null;
        $pages = $this->count_pages($filter, $sort);
        $page = 1;

        if (isset($_GET['filter_page'])) {
            $page = max(min($_GET['filter_page'], $pages), 1);
        }

        $rows = $this->get_rows($filter, $sort, $page);

        $grid = $this;

        require_once plugin_dir_path( dirname( __FILE__ ) ) . '/public/partials/immerse-lms-public-grid.php';
    }

    public function render_html() {
        ob_start();
        $this->render();
        return ob_get_clean();
    }
}