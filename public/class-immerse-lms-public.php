<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       pedrops.com
 * @since      1.0.0
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Immerse_Lms
 * @subpackage Immerse_Lms/public
 * @author     Pedro Germani <pedro.germani@toptal.com>
 */
class Immerse_Lms_Public
{

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

	/**
	 * Registered callbacks for formatting custom BuddyPress notifications.
	 * 
	 * @since	1.0.0
	 * @access	protected
	 * @var		array		$notifications	Registered callbacks for formatting custom BuddyPress notifications.
	 */
	protected $notifications;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($plugin_name, $version) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

        // Register notification callbacks.
        $this->notifications = array();
        $this->notifications['test_notification'] = 'immerse_lms_format_test_notification';
        $this->notifications['user_online'] = 'immerse_lms_format_notification_user_online';
        $this->notifications['user_online_diploma'] = 'immerse_lms_format_notification_user_online';
		
		add_shortcode( 'vr_user_classes', array($this, 'vr_user_classes_shortcode') );
        add_shortcode( 'vr_user_content', array($this, 'vr_user_content_shortcode') );
        add_shortcode( 'course_flow', array($this, 'immerse_lms_course_flow_shortcode') );
        add_shortcode( 'completed_unit_report', array($this, 'immerse_lms_completed_unit_report_shortcode') );
        add_shortcode( 'course_notes', array($this, 'immerse_lms_course_notes_shortcode') );
        add_shortcode( 'quiz_history', array($this, 'immerse_lms_quiz_history_shortcode') );
        add_shortcode( 'assessment_grading', array($this, 'immerse_lms_assessment_grading_shortcode') );
		add_shortcode( 'assessment_grading_list', array($this, 'immerse_lms_assessment_grading_list_shortcode') );
        add_shortcode( 'course_products', array($this, 'immerse_lms_course_products_shortcode') );
        add_shortcode( 'submitted_quizzes', array($this, 'immerse_lms_submitted_quizzes_shortcode') );
        add_shortcode( 'tutor_dashboard', array($this, 'immerse_lms_tutor_dashboard_shortcode') );

        add_action('wp_footer', array($this, 'custom_masquerade'));

    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Immerse_Lms_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Immerse_Lms_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/immerse-lms-public.css', array(), $this->version, 'all');
		wp_enqueue_style($this->plugin_name . 'alertify', plugin_dir_url(__FILE__) . 'css/alertify.css', array(), $this->version, 'all');
		wp_enqueue_style($this->plugin_name . 'alertify-theme', plugin_dir_url(__FILE__) . 'css/alertify-default.css', array(), $this->version, 'all');
		
		wp_enqueue_style($this->plugin_name . 'fullcalendar', '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css', array(), $this->version, 'all');
		wp_enqueue_style($this->plugin_name. 'fullcalendar-print', plugin_dir_url(__FILE__) . 'css/immerse-lms-public.css', array(), $this->version, 'all');
		//https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.css

        wp_enqueue_style($this->plugin_name. 'beefup-accordion-css', plugin_dir_url(__FILE__) . 'css/jquery.beefup.css', array(), $this->version, 'all');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Immerse_Lms_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Immerse_Lms_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */


        wp_enqueue_script('moment-js', plugin_dir_url(__FILE__) . 'js/moment.js', array('jquery'), $this->version, false);
        wp_enqueue_script('alertify-js', plugin_dir_url(__FILE__) . 'js/alertify.js', array(), $this->version, false);
		wp_enqueue_script('fullcalendar-js', '//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js', array('jquery','moment-js'), $this->version, false);
		wp_enqueue_script($this->plugin_name.'-immerse-lms-public', plugin_dir_url(__FILE__) . 'js/immerse-lms-public.js', array('jquery', 'fullcalendar-js'), $this->version, false);

		if (is_user_logged_in()) {
			//$current_user = wp_get_current_user();
			//$user_id = $current_user->ID;
			$user_id = wp_get_current_user()->ID;
			$user_info = get_userdata($user_id);
			
			if (in_array('administrator', $user_info->roles) || in_array('tutor_admin', $user_info->roles)) {
				$user_role = 'administrator';
			}
			else if (in_array('trainer', $user_info->roles)) {
				$user_role = 'trainer';
			}
			else if (in_array('bbp_participant', $user_info->roles)) {
				$user_role = 'student';
			}
			
			$user_timezone =  xprofile_get_field_data('Timezone',$user_id);
			if ($user_timezone) {
				$user_timezone = explode(' ', $user_timezone);
				$user_timezone = $user_timezone[0];
			}
			else $user_timezone = 'UTC';


			$datatoBePassed = array(
				'user_timezone' => $user_timezone,
				'user_role' 	=> $user_role,
			);
			wp_localize_script( $this->plugin_name.'-immerse-lms-public', 'php_vars', $datatoBePassed );
		}

        wp_enqueue_script('beefup-accordion-js', plugin_dir_url(__FILE__) . 'js/jquery.beefup.min.js', array('jquery'), $this->version, false);

        if ( is_page( 'assessment-grading-list' ) ) {
            wp_enqueue_style($this->plugin_name. 'flexdatalist-css', plugin_dir_url(__FILE__) . 'css/jquery.flexdatalist.min.css', array(), $this->version, 'all');
            wp_enqueue_script('flexdatalist-js', plugin_dir_url(__FILE__) . 'js/jquery.flexdatalist.min.js', array('jquery'), $this->version, false);
        }

    }

    /**
     * Get the latest statistic ref ID for a user and, optionally, a quiz ID.
     */
    public function immerse_lms_get_last_statistic_ref_id($user_id, $quiz_id = null) {
        global $wpdb;

        $statistic_ref_id = $wpdb->get_col("
            SELECT statistic_ref_id FROM
            {$wpdb->prefix}wp_pro_quiz_statistic_ref
            WHERE
                user_id = '{$user_id}' " . ($quiz_id ? " AND quiz_id = '$quiz_id'" : "") . "
            ORDER BY
                statistic_ref_id DESC
            LIMIT 1
        ");

        return count($statistic_ref_id) ? $statistic_ref_id[0] : null;
    }

    /**
     * Counts the amount of quiz attempts for a user.
     */
    public function immerse_lms_count_statistic_refs($user_id, $quiz_id = null) {
        global $wpdb;

        $statistic_ref_id = $wpdb->get_col("
            SELECT COUNT(*) FROM
            {$wpdb->prefix}wp_pro_quiz_statistic_ref
            WHERE
                user_id = '{$user_id}' " . ($quiz_id ? " AND quiz_id = '$quiz_id'" : "") . "
            ORDER BY
                statistic_ref_id DESC
            LIMIT 1
        ");

        return count($statistic_ref_id) ? $statistic_ref_id[0] : 0;
    }

    /**
     * Get the WP Quiz Pro ID, given a post ID.
     */
    public function immerse_lms_get_quiz_pro_id($quiz_post_id) {
        $id = get_post_meta($quiz_post_id, 'quiz_pro_id', true);
        return $id;
    }

    /**
     * Get the course progress of a user, given the user's ID and the course ID. If no course ID is provided, the progress of all courses is returned.
     */
    public function immerse_lms_get_user_course_progress($user_id, $course_id = null) {
        $course_progress = get_user_meta($user_id, '_sfwd-course_progress', true);

        if (empty($course_id)) {
            return $course_progress;
        }

        $course_progress = isset($course_progress[$course_id]) ? $course_progress[$course_id] : array();
        return $course_progress;
    }

    /**
     * Get the full hierarchy for a course, including its lessons, topics, and quizzes. If a user ID is provided, returns their custom hierarchy if one is present.
     */
    public function immerse_lms_get_course_steps_hierarchy($course_id = null, $user_id = null) {
        if (empty($course_id)) {
            $course_id = learndash_get_course_id();
        }

        $steps = get_post_meta($course_id, 'ld_course_steps', true);

        $custom_steps = null;

        if ($user_id) {
            $custom_steps = get_post_meta($course_id, '_custom_course_flow_' . $user_id, true);
        }

        if (is_array($custom_steps)) {
            $lessons = array();

            foreach ($custom_steps as $step) {
                $lesson = array(
                    'lesson' => get_post($step['id']),
                    'children' => array(),
                );

                foreach ($step['children'] as $child) {
                    $lesson['children'][] = get_post($child);
                }

                $lessons[] = $lesson;
            }

            return $lessons;
        } else {
            // Check for sub-courses of this course that the user may be enrolled in.
            global $wpdb;

            $sub_course = $wpdb->get_var(
                $wpdb->prepare(
                    "
                    SELECT
                        post.ID
                    FROM 
                        $wpdb->postmeta meta
                    INNER JOIN
                        $wpdb->posts post
                        ON post.ID = meta.post_id
                    WHERE
                        meta.meta_key LIKE 'students_%_student' AND
                        post.post_parent = %d AND
                        post.post_type = 'sfwd-courses' AND
                        meta.meta_value = %s
                    ",
                    $course_id,
                    $user_id
                )
            );
            
            if ($sub_course) {
                return $this->immerse_lms_get_course_steps_hierarchy($sub_course, $user_id);
            }
        }

        if (!isset($steps['h'])) {
            return array();
        }

        $steps = $steps['h'];
        $lessons = array();

        foreach ($steps['sfwd-lessons'] as $id => $lesson) {
            $children = array();

            foreach ($lesson['sfwd-topic'] as $cid => $child) {
                $children[] = get_post($cid);
            }

            foreach ($lesson['sfwd-quiz'] as $cid => $child) {
                $children[] = get_post($cid);
            }

            $lessons[] = array(
                'lesson' => get_post($id),
                'children' => $children,
            );
        }

        return $lessons;
    }

    /**
     * Gets the first course a given quiz is found in.
     */
    public function immerse_lms_get_course_for_quiz($quiz_id, $user_id = null) {
        $courses = learndash_get_courses_for_step($quiz_id);
        $primaries = isset($courses['primary']) ? array_keys($courses['primary']) : [];
        $secondaries = isset($courses['secondary']) ? array_keys($courses['secondary']) : [];
        
        $id = null;

        if ($user_id) {
            $user_courses = learndash_user_get_enrolled_courses($user_id);
            
            foreach ($user_courses as $course_id) {
                if (in_array($course_id, $primaries)) {
                    $id = $course_id;
                    break;
                }

                if (in_array($course_id, $secondaries)) {
                    $id = $course_id;
                    break;
                }
            }
        } else {
            if (count($primaries) > 0) {
                $id = $primaries[0];
            } else if (count($secondaries) > 0) {
                $id = $secondaries[0];
            }
        }

        if ($id) {
            return get_post($id);
        }

        return null;
    }

    /**
     * Gets the first lesson a given quiz is found in.
     */
    public function immerse_lms_get_lesson_for_quiz($quiz_id, $user_id = null) {
        $course = apply_filters('immerse_lms_get_course_for_quiz', $quiz_id, $user_id);
        $steps = apply_filters('immerse_lms_get_course_steps_hierarchy', $course->ID, $user_id);

        foreach ($steps as $lesson) {
            foreach ($lesson['children'] as $child) {
                if ($child->ID == $quiz_id) {
                    return $lesson;
                }
            }
        }

        return null;
    }

    /**
     * Gets the lesson progress for a particular user. If no user ID is provided, returns the lesson progress for the currently logged in user.
     */
    public function immerse_lms_learndash_lesson_progress($course_id, $lesson, $user_id = null) {
        $stat_mapper = new ImmerseLms_Model_StatisticRefMapper();

        if (empty($user_id)) {
            $current_user = wp_get_current_user();
            $user_id = $current_user->ID;
        }

        $course_progress = apply_filters('immerse_lms_get_user_course_progress', $user_id, $course_id);

        // Will set this to false if we find a quiz that isn't 100%.
        $has_quiz = false;
        $lesson_complete = false;

        $progress = array();

        foreach ($lesson['children'] as $child) {
            if ($child->post_type == 'sfwd-quiz') {
                if (!$has_quiz) {
                    $has_quiz = true;
                    $lesson_complete = true;
                }

                $passed = false;
                $attempt = $stat_mapper->getUserQuizAttempt($user_id, apply_filters('immerse_lms_get_quiz_pro_id', $child->ID));

                if ($attempt) {
                    $passed = $attempt->getPoints() == $attempt->getGPoints();

                    // It's also possible that there's an assessment here.
                    if (in_category('assessments', $child)) {
                        $assessment = $this->immerse_lms_find_assessment_grading_from_statistic($attempt->getStatisticRefId());
                        
                        if ($assessment) {
                            $passed = $passed || $assessment['grade'] == 'competent';
                        }
                    }
                }

                if ($passed) {
                    $progress[$child->ID] = true;
                } else {
                    $lesson_complete = false;
                    $progress[$child->ID] = false;
                }
            }
        }

        foreach ($lesson['children'] as $child) {
            if ($child->post_type == 'sfwd-topic') {
                if ($lesson_complete) {
                    $progress[$child->ID] = true;
                } else {
                    $progress[$child->ID] = isset($course_progress['topics'][$lesson['lesson']->ID][$child->ID]) && $course_progress['topics'][$lesson['lesson']->ID][$child->ID];
                }
            }
        }

        return $progress;
    }

    /**
     * Returns whether or not a user has completed a particular quiz.
     */
    public function immerse_lms_quiz_lesson_complete($quiz_id, $user_id) {
        $lesson = apply_filters('immerse_lms_get_lesson_for_quiz', $quiz_id, $user_id);
        $progress = apply_filters('immerse_lms_learndash_lesson_progress', apply_filters('immerse_lms_get_course_for_quiz', $quiz_id, $user_id)->ID, $lesson, $user_id);

        foreach ($progress as $item) {
            if (!$item) {
                return false;
            }
        }

        return true;
    }

    /**
     * Gets a summary of the course progress for a given user. The options array can include a course ID, user ID, both, or
     * neither. If a user ID is not provided, the currently logged in user is used. If a course ID is not provided, the
     * currently viewed course ID is used.
     */
    public function immerse_lms_learndash_course_progress($options = array()) {
        extract(shortcode_atts(array('course_id' => 0, 'user_id' => 0, 'array' => false), $options));

        if (empty($user_id)) {
            $current_user = wp_get_current_user();
            $user_id = $current_user->ID;
        }

        if (empty($course_id)) {
            $course_id = learndash_get_course_id();
        }

        $course_flow = apply_filters('immerse_lms_get_course_steps_hierarchy', $course_id, $user_id);
        $course_progress = apply_filters('immerse_lms_get_user_course_progress', $user_id, $course_id);
        $total = 0;
        $completed = 0;

        // Count the total amount of lessons, topics, and quizzes, and check their completion.
        foreach ($course_flow as $lesson) {
            $progress = apply_filters('immerse_lms_learndash_lesson_progress', $course_id, $lesson, $user_id);
            $total += count($progress) + 1;
            $children_completed = 0;
            $children_incomplete = 0;

            foreach ($progress as $child => $passed) {
                if ($passed) {
                    $children_completed++;
                } else {
                    $children_incomplete++;
                }
            }

            $completed += $children_completed;

            if ($children_incomplete == 0) {
                $completed++;
            }
        }

        if ($total == 0) {
            return array(
                'percentage' => 0,
                'completed' => 0,
                'total' => 0,
            );
        }

        return array(
            'percentage' => floor($completed / $total * 100),
            'completed' => $completed,
            'total' => $total,
        );

        return learndash_course_progress($options);
    }

    /**
     * Returns the username for display purposes. If no user ID is provided, the currently logged in user is used.
     */
    public function immerse_lms_displayed_user_name($user_id = null) {
        if ($user_id == null) {
            $user_id = bp_displayed_user_id();
        }

        return get_userdata($user_id)->user_login;
    }

    /**
     * Returns the max amount of attempts for a quiz or a course.
     */
    public function immerse_lms_get_max_quiz_attempts($course_id, $quiz_id = null) {
        if ($quiz_id) {
            $quiz_attempts = get_field('max_quiz_attempts', $quiz_id);

            if ($quiz_attempts) {
                return $quiz_attempts;
            }
        }

        $quiz_attempts = get_field('max_quiz_attempts', $course_id);

        if ($quiz_attempts) {
            return $quiz_attempts;
        }

        return null;
    }

    /**
     * Formats an integer representing seconds into a readable format including hours, minutes, and
     * seconds separated from each other. (e.x. 10h3m2s, 6m1s, or 2h)
     */
    public function immerse_lms_format_duration($duration) {
        $d = $duration;
        $f = [];

        // Hours
        if ($d > 3600) {
            $v = floor($d / 3600);
            $f[] = $v . 'h';
            $d -= $v * 3600;
        }

        // Mins
        if ($d > 60) {
            $v = floor($d / 60);
            $f[] = $v . 'm';
            $d -= $v * 60;
        }

        // Seconds
        if ($d > 0) {
            $v = floor($d);
            $f[] = $v . 's';
            $d -= $v;
        }

        if (count($f) == 0) {
            return '0s';
        }

        return implode(' ', $f);
    }

    public function immerse_lms_get_quiz_data($user_id) {
        global $wpdb;
        $wpdb->statistic_ref = $wpdb->prefix . 'wp_pro_quiz_statistic_ref';
        $wpdb->statistic = $wpdb->prefix . 'wp_pro_quiz_statistic';
        $user = get_user_by('id', $user_id);

        // Get the user meta.
        $usermeta = get_user_meta($user_id, '_sfwd-quizzes', true);
        $usermeta = array_reverse($usermeta);

        $quiz_mapper = new WpProQuiz_Model_QuizMapper();
        $stat_ref_mapper = new ImmerseLms_Model_StatisticRefMapper();
        $stat_mapper = new WpProQuiz_Model_StatisticMapper();
        
        foreach ($usermeta as $q) {
            $quizPost = get_post($q['quiz']);
            $course = get_post($q['course']);

            if (isset($q['statistic_ref_id']) && $q['statistic_ref_id']) {
                $stat = $stat_ref_mapper->fetchById($q['statistic_ref_id']);

                if ($stat) {
                    $q['points'] = $stat->getPoints();
                    $q['total_points'] = $stat->getGPoints();
                }
            }

            // Recalculate the grade.
            if (isset($q['graded']) && isset($q['statistic_ref_id'])) {
                $stats = $wpdb->get_row($wpdb->prepare("
                SELECT
                    COALESCE(SUM(correct_count), 0) AS total_correct,
                    COALESCE(SUM(incorrect_count), 0) AS total_incorrect,
                    COALESCE(SUM(points), 0) AS total_points
                FROM
                    $wpdb->statistic
                WHERE
                    answer_data NOT LIKE '{\"graded_id\":%' AND
                    statistic_ref_id = %d
                ", $q['statistic_ref_id']));

                $score = $stats->total_correct;
                $count = $stats->total_correct + $stats->total_incorrect;
                $points = $stats->total_points;

                foreach ($q['graded'] as $val) {
                    $points += $val['points_awarded'];

                    if ($val['points_awarded'] > 0) {
                        $score++;
                    }

                    $count++;
                }

                $pass = $points == $q['total_points'];
                $q['score'] = $score;
                $q['count'] = $count;
                $q['points'] = $points;
            }

            $stat = [
                'statistic_ref_id' => isset($q['statistic_ref_id']) ? $q['statistic_ref_id'] : null,
                'user_id' => $user->ID,
                'name' => $user->user_login,
                'email' => $user->user_email,
                'quiz_post_id' => $q['quiz'],
                'quiz_id' => $q['pro_quizid'],
                'quiz_title' => $quizPost->post_title,
                'score' => $q['score'],
                'total' => $q['question_show_count'],
                'date' => date(get_option('date_format'), $q['time']),
                'percentage' => $q['total_points'] == 0 ? $q['percentage'] : (round(($q['points'] / $q['total_points']) * 10000) / 100),
                'time_spent' => apply_filters('immerse_lms_format_duration', $q['timespent']),
                'passed' => $q['points'] == $q['total_points'] ? 'YES' : 'NO',
                'course_id' => $course ? $course->ID : null,
                'course_title' => $course ? $course->post_title : null,
                'graded' => isset($q['graded']) ? $q['graded'] : null
            ];

            // Don't return 0/0 quiz attempts.
            if ($stat['percentage'] != 0 || $stat['score'] != 0 || $stat['total'] || $stat['total'] != 0) {
                $returned[] = $stat;
            }
        }

        return $returned;
    }

    function immerse_lms_tutors_only() {
        if (!is_user_logged_in() || (!current_user_can('administrator') && !current_user_can('tutor_admin'))) {
            return false;
        }

        return true;
    }

    function immerse_lms_trainers_only() {
        if (!is_user_logged_in() || (!current_user_can('administrator') && !current_user_can('tutor_admin') && !current_user_can('trainer'))) {
            return false;
        }

        return true;
    }

    function immerse_lms_logged_in_users_only() {
        if (!is_user_logged_in()) {
            return false;
        }

        return true;
    }

    function immerse_lms_get_user_from_query() {
        return (isset($_GET['user']) && (current_user_can('tutor_admin') || current_user_can('administrator'))) ? get_user_by('login', $_GET['user']) : wp_get_current_user();
    }

    function render_partial($location, $variables = array()) {
        extract($variables);

        ob_start();
        require_once plugin_dir_path( dirname( __FILE__ ) ) . $location;
        return ob_get_clean();
    }

    function immerse_lms_quiz_history_shortcode( $atts ) {
        if ($this->immerse_lms_logged_in_users_only()) {
            $user = $this->immerse_lms_get_user_from_query();
            
            return $this->render_partial('public/partials/immerse-lms-public-quiz-history.php', compact('user'));
        }

        return "Please log in to view this content.";
    }

    function immerse_lms_assessment_grading_shortcode( $atts ) {
        $allowed = $this->immerse_lms_trainers_only();
        $is_marker = false;

        if (!$allowed) {
            if (current_user_can('marker')) {
                $allowed = true;
                $is_marker = true;
            }
        }

        if ($allowed) {
            $assessment_grading_id = $_GET['gid'];
            
            return $this->render_partial('public/partials/immerse-lms-public-assessment-grading.php', compact('assessment_grading_id', 'is_marker'));
        }

        return "You are not authorized to view this content.";
    }

    function immerse_lms_assessment_grading_list_shortcode( $atts ) {
        $allowed = $this->immerse_lms_trainers_only();
        $is_marker = false;

        if (!$allowed) {
            if (current_user_can('marker')) {
                $allowed = true;
                $is_marker = true;
            }
        }

        if ($allowed) {
            return $this->render_partial('public/partials/immerse-lms-public-assessment-grading-list.php', compact('is_marker'));
        }

        return "You are not authorized to view this content.";
    }

    function immerse_lms_submitted_quizzes_shortcode( $atts ) {
        if ($this->immerse_lms_trainers_only()) {
            require_once(__DIR__ . '/grids/class-submitted-quizzes-grid.php');
            $grid = new Submitted_Quizzes_Grid();
            return $grid->render_html() . '<small>Columns with * are related to an assessment.</small>';
        }

        return "You are not authorized to view this content.";
    }

    /**
     * Store the last login for use later.
     */
    function immerse_lms_last_login( $user_login, $user ) {
        update_user_meta( $user->ID, 'immerse_lms_newly_logged_in', true );
    }

    function immerse_lms_course_flow_shortcode( $atts ) {
        if ($this->immerse_lms_tutors_only()) {
            $user = $this->immerse_lms_get_user_from_query();

            $atts = apply_filters( 'bp_learndash_user_courses_atts', array());
            $userCourses = apply_filters( 'bp_learndash_user_courses', ld_get_mycourses($user->ID,  $atts));

            return $this->render_partial('public/partials/immerse-lms-public-course-flow.php', compact('user', 'userCourses'));
        }

        return "You are not authorized to view this content.";
    }

    function immerse_lms_check_enable_online_notifications() {
        $options = get_option($this->plugin_name . '_options');
        $user = $this->immerse_lms_get_user_from_query();
        $atts = apply_filters( 'bp_learndash_user_courses_atts', array());
        $userCourses = apply_filters( 'bp_learndash_user_courses', ld_get_mycourses($user->ID,  $atts));

        echo '<script>var ENABLE_STUDENT_ONLINE_NOTIFICATIONS = ' . json_encode(isset($options['immerse_lms_field_enable_online_notification']) && $options['immerse_lms_field_enable_online_notification'] && (current_user_can('administrator') || current_user_can('tutor_admin') || current_user_can('trainer'))) . ';</script>';
        
        if (get_user_meta($user->ID, 'immerse_lms_newly_logged_in', true)) {
            // Check for trainers online in the last 5 minutes.
            $five_minutes_ago = time() - 5 * 60 * 1000;
            
            $users = get_users(array(
                'role' => 'tutor_admin',
                'meta_key' => 'immerse_lms_heartbeat',
                'meta_value' => $five_minutes_ago,
                'meta_compare' => '>'
            ));

            echo '<script>var ONLINE_TUTORS_MESSAGE = ' . json_encode($options['immerse_lms_field_tutor_online_notification']) . ';</script>';

            echo '<script>var ONLINE_TUTORS = ' . json_encode(array_map(function($user) {
                return $user->display_name;
            }, $users)) . ';</script>';

            update_user_meta($user->ID, 'immerse_lms_newly_logged_in', false);
        } else {
            echo '<script>var ONLINE_TUTORS = [];</script>';
        }

        echo '<script>var USER_LOGGED_IN = ' . json_encode(is_user_logged_in()) . ';</script>';
    }

    function immerse_lms_prefill_quiz_data($content) {
        global $post;
        $quizData = array();

        // Prefill data on the last quiz attempt, if one is specified.
        if (isset($_GET['gid'])) {
            $assessment = get_assessment_grading($_GET['gid']);

            if ($assessment && $assessment['student']->ID == get_current_user_id()) {
                foreach ($assessment['answers'] as $answer) {
                    $qid = $answer['question']->getId();
                    $quizData[$qid] = array(
                        'type' => $answer['question']->getAnswerType(),
                        'mark' => $answer['mark'],
                        'answer' => $answer['answer']
                    );
                }

                $content = $content . '<script>var LAST_QUIZ_ATTEMPT_DATA = ' . json_encode($quizData) . ';</script>';
            }
        }

        return $content;
    }

    function immerse_lms_quiz_locked($quiz_post, $user) {
        global $wpdb;
        $quiz_id = apply_filters('immerse_lms_get_quiz_pro_id', $quiz_post->ID);

        // Check if they've already aced the quiz.
        $quiz_stat = null;

        foreach (apply_filters('immerse_lms_get_quiz_data', $user->ID) as $stat) {
            if ($stat['quiz_post_id'] == $quiz_post->ID) {
                if ($stat['passed'] == 'YES') {
                    return "This quiz has already been completed & passed.";
                }

                $quiz_stat = $stat;
                break;
            }
        }

        // Check if this quiz is an assessment.
        if (in_category('assessments', $quiz_post)) {
            // Check if this is a currently pending assessment.
            $wpdb->statistic_ref = $wpdb->prefix . "wp_pro_quiz_statistic_ref";

            $grading = $wpdb->get_results("
            SELECT
                assessment_grading.id
            FROM
                $wpdb->statistic_ref stat
            INNER JOIN
                $wpdb->postmeta meta ON
                meta.meta_key = '_statistic-ref-id' AND
                meta.meta_value = stat.statistic_ref_id
            INNER JOIN
                $wpdb->posts assessment_grading ON
                assessment_grading.ID = meta.post_id AND
                assessment_grading.post_type = 'assessment_grading'
            WHERE
                stat.user_id = '$user->ID' AND
                stat.quiz_id = $quiz_id
            ORDER BY
                stat.statistic_ref_id DESC
            LIMIT 1
            ");

            if (count($grading) > 0) {
                $assessment_grading = get_assessment_grading($grading[0]->id);

                if ($assessment_grading['status'] != 'graded') {
                    return "This assessment has not yet been graded.";
                } else if ($assessment_grading['grade'] == 'competent') {
                    return "This quiz has already been completed & passed.";
                }
            }
        }

        // Check if the quiz is locked manually.
        $quiz_locked = get_user_meta($user->ID, '_quiz-locked-' . $quiz_id, true);

        if ($quiz_locked) {
            $locking_user = get_user_by('id', $quiz_locked);

            if (!$locking_user) {
                return "This quiz has been temporarily locked.";
            }

            return "This quiz has been temporarily locked by " . $locking_user->display_name . ".";
        }

        return false;
    }

    function immerse_lms_lock_quiz_content($content) {
        global $post;
        $user = wp_get_current_user();
        $msg = $this->immerse_lms_quiz_locked($post, $user);

        if ($msg) {
            return $msg;
        }

        // Output the URL for the next quiz after this one.
        $quiz_id = $post->ID;
        $user_id = wp_get_current_user()->ID;
        $course = apply_filters('immerse_lms_get_course_for_quiz', $quiz_id, $user_id);
        $steps = apply_filters('immerse_lms_get_course_steps_hierarchy', $course->ID, $user_id);
        $nextStep = null;
        $next = false;

        foreach ($steps as $lesson) {
            if ($next) {
                $nextStep = array();
                $nextStep['type'] = 'lesson';
                $nextStep['url'] = get_the_permalink($lesson['lesson']);
            }

            $next = false;

            foreach ($lesson['children'] as $child) {
                if ($child->post_type == 'sfwd-quiz') {
                    if ($next) {
                        $nextStep = array();
                        $nextStep['type'] = 'quiz';
                        $nextStep['url'] = get_the_permalink($child);
                        $next = false;
                    }

                    if ($child->ID == $quiz_id) {
                        $next = true;
                    }
                }
            }
        }

        if ($nextStep) {
            // Inject our button after the location of this string.
            $searchString = 'value="View questions">';
            $location = strpos($content, $searchString) + strlen($searchString);
            $before = substr($content, 0, $location);
            $after = substr($content, $location);
            $linkText = "";

            if ($nextStep['type'] == "quiz") {
                $linkText = "Next Quiz";
            }

            if ($nextStep['type'] == "lesson") {
                $linkText = "Next Lesson";
            }

            $link = "\n" . '<a href="' . $nextStep['url'] . '" class="button">' . $linkText . '</a>';
            $content = $before . $link . $after;
        }
        
        return $content;
    }

    function immerse_lms_completed_unit_report_shortcode( $atts ) {
        if ($this->immerse_lms_tutors_only()) {
            $user = $this->immerse_lms_get_user_from_query();
            
            $statMapper = new ImmerseLms_Model_StatisticRefMapper();
            $quizMapper = new WpProQuiz_Model_QuizMapper();
            $quizAttempts = $statMapper->getUserQuizAttempts($user->ID);
            
            $courses = [];
            $lessons = [];
            $units = [];
            $structure = [];
            
            foreach ($quizAttempts as $attempt) {
                if ($attempt->getIncorrectCount() == 0) {
                    $quizId = learndash_get_quiz_id_by_pro_quiz_id($attempt->getQuizId());
                    $course = apply_filters('immerse_lms_get_course_for_quiz', $quizId, $user->ID);
                    
                    if ($course) {
                        if (!isset($courses[$course->ID])) {
                            $courses[$course->ID] = $course;
                            $structure[$course->ID] = [];
                        }
                        
                        $lesson = learndash_get_lesson_id($quizId, $course->ID);
            
                        if ($lesson) {
                            $lesson = get_post($lesson);
            
                            if (!isset($lessons[$lesson->ID])) {
                                $lessons[$lesson->ID] = $lesson;
                                $structure[$course->ID][$lesson->ID] = [];
                            }
            
                            $structure[$course->ID][$lesson->ID][] = $attempt;
                        }
                    }
                }
            }

            return $this->render_partial('public/partials/immerse-lms-public-completed-unit-report.php', compact('user', 'courses', 'lessons', 'units', 'structure', 'quizAttempts'));
        }

        return "You are not authorized to view this content.";
    }

    function immerse_lms_course_notes_shortcode( $atts ) {
        if ($this->immerse_lms_logged_in_users_only()) {
            $user = $this->immerse_lms_get_user_from_query();

            if ((current_user_can('tutor_admin') || current_user_can('administrator')) && $user->ID == wp_get_current_user()->ID) {
                return '<script>window.location.href = "/my-notes/";</script>';
            }

            $notes = get_posts(array(
                'posts_per_page' => -1,
                'post_type' => 'coursenote',
                'author' => $user->ID,
                'orderby' => 'post_date',
                'order' => 'DESC'
            ));
            
            $tutorNotes = get_posts(array(
                'posts_per_page' => -1,
                'post_type' => 'coursenote',
                'meta_key' => '_rm-for-user',
                'meta_value' => $user->ID,
                'orderby' => 'post_date',
                'order' => 'DESC'
            ));
            
            $noteList = array();
            
            $userCourses = apply_filters( 'bp_learndash_user_courses', ld_get_mycourses($user->ID,  $atts));
            
            foreach ($userCourses as $courseId) {
                $hierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $courseId, $user->ID);
                
                foreach ($hierarchy as $lesson) {
                    $topicHash = '^^' . $lesson['lesson']->ID;
                    $noteList[$topicHash]['tutor'] = array();
                    $noteList[$topicHash]['students'] = '^^';
                }
            }
            
            foreach ($notes as $note) {
                $topic = get_post_meta($note->ID, '_nt-course-array', true);
                $topicHash = $topic[0] . '^' . $topic[1] . '^' . $topic[2];
            
                if (!isset($noteList[$topicHash])) {
                    $noteList[$topicHash] = [];
                }
                
                if (!isset($noteList[$topicHash]['student'])) {
                    $noteList[$topicHash]['student'] = [];
                }
            
                $noteList[$topicHash]['student'][] = $note;
            }
            
            foreach ($tutorNotes as $note) {
                if ($note->post_author != $user->ID) {
                    $topic = get_post_meta($note->ID, '_nt-course-array', true);
                    $topicHash = $topic[0] . '^' . $topic[1] . '^' . $topic[2];
            
                    if (!isset($noteList[$topicHash])) {
                        $noteList[$topicHash] = [];
                    }
                    
                    if (!isset($noteList[$topicHash]['tutor'])) {
                        $noteList[$topicHash]['tutor'] = [];
                    }
            
                    $noteList[$topicHash]['tutor'][] = $note;
                }
            }
            
            return $this->render_partial('public/partials/immerse-lms-public-course-notes.php', compact('user', 'noteList'));
        }

        return "Please log in to view this content.";
    }

    function immerse_lms_tutor_dashboard_shortcode( $atts ) {
           
        return $this->render_partial('public/partials/immerse-lms-public-tutor-dashboard.php');

    }
	
	/*
	* Adds VR User Content list shortcode
	*/
	function vr_user_content_shortcode( $atts ) {
		if (is_user_logged_in()) {
            require_once(__DIR__ . '/grids/class-vr-user-uploads-grid.php');
            $grid = new Vr_User_Uploads_Grid();
            return $grid->render_html();
		} else {
			echo "You are not allowed to see this page, please <a href='/wp-login.php'> log in here</a>.";
		}
	}

	/*
	* Adds VR User Class list/calendar shortcode
	*/
	function vr_user_classes_shortcode( $atts ) {
		

		if (is_user_logged_in()) {
			//$current_user = wp_get_current_user();
			//$user_id = $current_user->ID;
			$user_id = wp_get_current_user()->ID;
			$user_info = get_userdata($user_id);
			$user_role = 'student';
			if (in_array('administrator', $user_info->roles) || in_array('tutor_admin', $user_info->roles)) {
				$user_role = 'administrator';
			}
			else if (in_array('trainer', $user_info->roles) ) {
				$user_role = 'trainer';
			}
		

			$posts_per_page = -1;
		
			$vr_user_classes_upcoming = $this->get_vr_user_classes($user_id, $user_role, $posts_per_page, null, false);
			$vr_user_classes_past = $this->get_vr_user_classes($user_id, $user_role, $posts_per_page, null, true);
			
			$found_posts_upcoming = $vr_user_classes_upcoming[1];
			$vr_user_classes_upcoming = $vr_user_classes_upcoming[0];

			$found_posts_past = $vr_user_classes_past[1];
			$vr_user_classes_past = $vr_user_classes_past[0];
			
			//wp_localize_script($this->plugin_name, 'vr_user_classes', $vr_user_classes);
			
			require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/partials/immerse-lms-public-vr-user-classes.php';	
		}
		else {
			echo "You are not allowed to see this page, please <a href='/wp-login.php'> log in here</a>.";
		}
		
	}

    public function immerse_lms_is_enrolled_in_diploma_course($user_id) {
        $atts = apply_filters( 'bp_learndash_user_courses_atts', array());
        $userCourses = apply_filters( 'bp_learndash_user_courses', ld_get_mycourses($user_id,  $atts));

        foreach ($userCourses as $course_id) {
            $course_name = get_the_title($course_id);

            if (in_category('diploma-course', $course_id)) {
                return true;
            }
        }

        return false;
    }

    public function immerse_lms_ajax_success($return_data = null) {
        wp_send_json_success($return_data);
        wp_die();
    }

    public function immerse_lms_ajax_failure($return_data = null) {
        wp_send_json_error($return_data);
        wp_die();
    }

    /**
     * AJAX heartbeat for determining if the user is online.
     */
    public function immerse_lms_ajax_heartbeat() {
        update_user_meta( wp_get_current_user()->ID, 'immerse_lms_heartbeat', time() );
        return $this->immerse_lms_ajax_success(true);
    }

    public function immerse_lms_find_assessment_grading_from_statistic($statistic_ref_id) {
        global $wpdb;

        if ($statistic_ref_id) {
            $assessment_grading_id = $wpdb->get_col("
            SELECT post.ID
            FROM $wpdb->posts post
            INNER JOIN $wpdb->postmeta postmeta
                ON
                    postmeta.post_id = post.ID AND
                    postmeta.meta_key = '_statistic-ref-id' AND
                    postmeta.meta_value = '" . $statistic_ref_id . "'
            ");

            if (count($assessment_grading_id)) {
                return get_assessment_grading($assessment_grading_id[0]);
            }
        }

        return null;
    }

    public function immerse_lms_ajax_get_user_notification_message() {
        $username = $_REQUEST['username'];
        $user = get_user_by('login', $username);
        $enrolled_in_diploma_course = $this->immerse_lms_is_enrolled_in_diploma_course($user->ID);
        $options = get_option($this->plugin_name . '_options');

        // Add notifications to BuddyPress.
        if ($enrolled_in_diploma_course) {
            $this->immerse_lms_add_notification('user_online_diploma', wp_get_current_user()->ID, $user->ID);
        } else {
            $this->immerse_lms_add_notification('user_online', wp_get_current_user()->ID, $user->ID);
        }

        return $this->immerse_lms_ajax_success(str_replace('{name}', $user->display_name, $enrolled_in_diploma_course ? $options['immerse_lms_field_online_diploma_notification'] : $options['immerse_lms_field_online_notification']));
    }

	/*
	* Returns collection of VR User Classes list shortcode
	*/
	public function get_vr_user_classes($user_id, $user_role = null, $posts_per_page = 10, $page = 1, $past_classes ){
		$args = array(
			'post_type' => 'course-class',
			'posts_per_page' => $posts_per_page,
			
		);
		$args['meta_query'] = array('relation' => 'OR');
		
		if ($past_classes) {
			$compare = '<';
			$args['order'] = 'DESC';
		}
		else {
			$compare = '>=';
			$args['order'] = 'ASC';
		}
		
		array_push($args['meta_query'], 
			array(
				'key' => 'vr_class_datetime_start',
				'value' => date('Y-m-d'),
				'compare' => $compare,
				'type'=> 'date'
			)
		);
		array_push($args['meta_query'], 
			array(
				'key' => 'vr_class_datetime_end',
				'value' => date('Y-m-d'),
				'compare' => $compare,
				'type'=> 'date'
			)
		);

		if ($user_role == 'student' ){
			array_push($args['meta_query'], 
				array(
					'key' => 'class_students',
					'value' => sprintf(':"%s";', $user_id),
					'compare' => 'LIKE',					
				)
			);
		} 
		else if ($user_role == 'trainer' ){
			array_push($args['meta_query'],
				array(
					'key' => 'class_trainer',
					'value' => $user_id,
					'compare' => '=',					
				)
			);
		}
		
		if ($page > 1) {
			$args['paged'] = $page;
		}

		$vr_classes = new WP_Query($args);
		
		if ( $vr_classes->have_posts() ) {
			foreach ($vr_classes->posts as $key => $vr_user_class) {
				$user_timezone =  xprofile_get_field_data('Timezone',$user_id);
					if ($user_timezone) {
						$user_timezone = explode(' ', $user_timezone);
						$user_timezone = $user_timezone[0];
					}
					else $user_timezone = 'UTC';
				
				$user_timezone = new DateTimeZone($user_timezone);
				$class_timezone = new DateTimeZone(get_field('class_timezone', $vr_user_class->ID));
				
				$startDate =  new DateTime(get_field('vr_class_datetime_start', $vr_user_class->ID), $class_timezone);				
				$endDate =  new DateTime(get_field('vr_class_datetime_end', $vr_user_class->ID), $class_timezone);				
				
				$repeat = get_field('vr_class_repeat', $vr_user_class->ID);
				$weekdays = get_field('vr_class_repeat_weekdays', $vr_user_class->ID);
				
				$vr_user_classes[$vr_user_class->ID]['ID'] = $vr_user_class->ID;
				$vr_user_classes[$vr_user_class->ID]['class_public_title'] = get_field('class_public_title', $vr_user_class->ID);
				$vr_user_classes[$vr_user_class->ID]['class_trainer'] = get_field('class_trainer', $vr_user_class->ID);
				$vr_user_classes[$vr_user_class->ID]['vr_class_datetime_start'] = $startDate->format('d/m/Y H:i (\G\M\TP)');
				$vr_user_classes[$vr_user_class->ID]['class_timezone'] = get_field('class_timezone', $vr_user_class->ID);		
				$vr_user_classes[$vr_user_class->ID]['class_course'] = get_field('class_course', $vr_user_class->ID);		
				$vr_user_classes[$vr_user_class->ID]['class_students'] = get_field('class_students', $vr_user_class->ID);		
				$vr_user_classes[$vr_user_class->ID]['class_vr_content_count'] = (int) get_post_meta($vr_user_class->ID, 'vr_content', ARRAY_A);	
				
				if ($repeat) {
					$vr_user_classes[$vr_user_class->ID]['class_vr_repeat'] = $repeat;			
					$vr_user_classes[$vr_user_class->ID]['vr_class_repeat_weekdays'] = $weekdays;	

					$days = array();
					if ($weekdays) {
						foreach ($weekdays as $weekday) {
							$days = array_merge($days, $this->get_days($startDate->format('Y-m-d'), $endDate->format('Y-m-d'), $weekday));	
						}
						sort($days);
					}
					else {
						$days = array_merge($days, $this->get_days($startDate->format('Y-m-d'), $endDate->format('Y-m-d')));	
					}
					
					

					$now = new DateTime();
					$now = $now->format('Y-m-d');
					$nextDay = '';
					foreach ($days as $day) {
						if ($now <= $day){
							$nextDay = $day;
							break;
						}
					}

					$nextDay = new DateTime($nextDay, $class_timezone);
					$nextDay->setTimeZone($user_timezone);
					$vr_user_classes[$vr_user_class->ID]['class_vr_repeat_next_date'] = $nextDay->format('d/m/Y') . ' ' . $startDate->format('H:i (\G\M\TP)');		
				}
				
			}
		}
		else $vr_user_classes = array();
		return array($vr_user_classes, $vr_classes->found_posts);
	}

	public function immerse_lms_ajax_delete_upload() {
		$data = $_POST['id'];
		$update_post = array(
			'ID'           => $_POST['id'],
			'post_status'   => 'trash',
		);
	  
	  // Update the post into the database
		$post_id = 0;
	 	$post_id = wp_update_post( $update_post );
		if (is_wp_error($post_id)) {
			$errors = $post_id->get_error_messages();
            return $this->immerse_lms_ajax_failure(-1);
        }
		else 
			return $this->immerse_lms_ajax_failure('Media ID: ' . $post_id. ' removed');
    }

	public function immerse_lms_ajax_load_vr_classes() {
		$current_user = wp_get_current_user();
		$user_id = $current_user->ID;
		$posts_per_page = -1;
		
		$user_role = $_POST['user_role'];
		$past = $_POST['past_classes'] == 'true' ? true : false;
		$calendar_timezone = new DateTimeZone($_POST['timezone']);

		$vr_user_classes = $this->get_vr_user_classes($user_id, $user_role, $posts_per_page, null, $past);
		$found_posts = $vr_user_classes[1];
		$vr_user_classes = $vr_user_classes[0];
		
		foreach ($vr_user_classes as $vr_user_class) {
			//$date = date("Y-m-d\TH:i:s\-03:00", strtotime(get_field('vr_class_datetime_start', $vr_user_class['ID'])));
			
			$class_timezone =  $vr_user_class['class_timezone'];
			$tz = new DateTimeZone("GMT");
			if ($class_timezone) $tz = new DateTimeZone($class_timezone);

			$startDate =  new DateTime(get_field('vr_class_datetime_start', $vr_user_class['ID']), $tz);
			$startDate->setTimeZone($calendar_timezone);
			
			$startTime = $startDate->format('H:i:sP');

			$endDate =  new DateTime(get_field('vr_class_datetime_end', $vr_user_class['ID']), $tz);

			$now = new DateTime();
			$now->setTimeZone($calendar_timezone);
			$weekdays = get_field('vr_class_repeat_weekdays', $vr_user_class['ID']);
			$days = array();
			if ($weekdays) {
				foreach ($weekdays as $weekday) {
					$dates = $this->get_days($startDate->format('Y-m-d'), $endDate->format('Y-m-d'), $weekday);	
					$days = array_merge($dates, $days);	
				}
			}
			else {
				$days = $this->get_days($startDate->format('Y-m-d'), $endDate->format('Y-m-d'));	
			}

			foreach ($days as $day) {
				// only returns past or future events based on $past
				if ( $past && $day < $now->format('Y-m-d') || ! $past && $day >= $now->format('Y-m-d')) {
					$events[] = array(
						'id' => $vr_user_class['ID'], 
						'title' => $vr_user_class['class_public_title'],
						'start' => $day.'T'.$startTime,
						
					);	
				}
			}
		}
		
		echo json_encode($events);
		wp_die();
	}

	private function get_days ( $startDate, $endDate, $weekday = false )
	{
		$r = array ();
		$startDate = strtotime ( $startDate . ' GMT' );
		$endDate = strtotime ( $endDate . ' GMT' );

		$day = gmdate ( 'l', $startDate );

		do
		{	
			
			if ($weekday) {
				if (gmdate('N', $startDate) == $weekday) {
					$r[] = gmdate ( 'Y-m-d', $startDate );
				}
				$startDate += 86400 * 1;
			}
			else {
				$r[] = gmdate ( 'Y-m-d', $startDate );
				$startDate += 86400 * 7;
			}
			

		} while ( $startDate <= $endDate );

		return $r;
	} 
	
    /**
     * AJAX call to get the last statistic ref ID for a user and a quiz.
     */
    public function immerse_lms_ajax_count_statistic_refs() {
        $statistic_ref_id = apply_filters('immerse_lms_count_statistic_refs', wp_get_current_user()->ID, $_POST['quiz_id']);
        return $this->immerse_lms_ajax_success($statistic_ref_id);
    }

    /**
     * AJAX call to get the last quiz attempt for a user and a quiz.
     */
    public function immerse_lms_ajax_get_quiz_attempt() {
        $stats = apply_filters('immerse_lms_get_quiz_data', wp_get_current_user()->ID);
        
        foreach ($stats as $stat) {
            if ($stat['quiz_id'] == $_POST['quiz_id']) {
                return $this->immerse_lms_ajax_success($stat);
            }
        }
    }

    /**
     * Override the score of the last recorded attempt by a provided user on a
     * provided quiz.
     */
    public function immerse_lms_ajax_override_quiz_score() {
        $quizId = $_POST['quiz_id'];
        $userId = $_POST['user_id'];

        if (!$quizId || !$userId) {
            return $this->immerse_lms_ajax_failure(['message' => "Quiz ID and user ID are required."]);
        }

        // Check if there's an existing record.
        $stat_mapper = new WpProQuiz_Model_StatisticRefMapper();
        $quiz = $stat_mapper->fetchAll($quizId, $userId);

        if (!$quiz || !count($quiz)) {
            return $this->immerse_lms_ajax_failure(['message' => "User hasn't taken that quiz yet."]);
        }

        $stat_mapper = new ImmerseLms_Model_StatisticRefMapper();
        $quiz = $stat_mapper->fetchAll($quizId, $userId);
        $statRefId = $quiz[count($quiz) - 1]->getStatisticRefId();

        $stats_mapper = new WpProQuiz_Model_StatisticMapper();
        $stats = $stats_mapper->fetchAllByRef($statRefId);

        $question_mapper = new WpProQuiz_Model_QuestionMapper();
        $points = 0;

        foreach ($stats as $stat) {
            $question = $question_mapper->fetch($stat->getQuestionId());

            if ($question) {
                $stat_mapper->overrideStatScore($stat->getStatisticRefId(), $stat->getQuestionId(), $question->getPoints());
            }
        }

        if (apply_filters('immerse_lms_quiz_lesson_complete', learndash_get_quiz_id_by_pro_quiz_id($quizId), $userId)) {
            $lesson = apply_filters('immerse_lms_get_lesson_for_quiz', learndash_get_quiz_id_by_pro_quiz_id($quizId), $userId);
            $lesson = explode(' ', $lesson['lesson']->post_title)[0];
            rm_sugarcrm_push_unit_completion($userId, $lesson);
        }

        return $this->immerse_lms_ajax_success();
    }

    /**
     * Create a new course note at a given location. If the "for_user" isn't
     * the currently logged in user, it will save it as a tutor note. Otherwise,
     * it will save it as a student note.
     */
    public function immerse_lms_ajax_post_note() {
        $location = $_POST['location'];
        $content = $_POST['content'];
        $for_user = $_POST['for_user'];

        if (!$content || !$location) {
            return $this->immerse_lms_ajax_failure(['message' => 'Content and location are required.']);
        }

        $location = explode('^', $location);

        $noteid = wp_insert_post(array(
            'post_type' => 'coursenote',
            'post_content' => $content,
            'post_status' => 'publish',
        ));

        update_post_meta($noteid, '_nt-course-array', $location);

        if (isset($for_user) && $for_user != wp_get_current_user()->ID && (current_user_can('administrator') || current_user_can('tutor_admin'))) {
            update_post_meta($noteid, '_rm-for-user', $for_user);
        }

        return $noteid;
    }

    /**
     * Edits a previously created course note.
     */
    public function immerse_lms_ajax_save_note() {
        $id = $_POST['id'];
        $content = $_POST['content'];

        if (!$content || $id) {
            return $this->immerse_lms_ajax_failure(['message' => 'Content and ID are required.']);
        }

        $note = get_post($id);

        if (!$note) {
            return $this->immerse_lms_ajax_failure(['message' => 'Note doesn\'t exist.']);
        }

        if ($note->post_author == wp_get_current_user()->ID) {
            wp_update_post(array(
                'ID' => $id,
                'post_content' => $content,
            ));
        } else {
            return $this->immerse_lms_ajax_failure(['message' => 'You can only edit your own notes.']);
        }

        return $this->immerse_lms_ajax_success();
    }

    public function immerse_lms_ajax_get_student_answer() {

        global $wpdb;
        session_start();

        $quiz_id = $_POST['quiz_id'];
        $user_id = $_POST['user_id'];

        $quiz_meta = get_user_meta($user_id, '_sfwd-quizzes');
        $recent_quiz = end($quiz_meta[0]);
        $stat_ref_id = $recent_quiz['statistic_ref_id'];

        $questions = $wpdb->get_results( "SELECT question_id, answer_data FROM " . $wpdb->prefix . "wp_pro_quiz_statistic WHERE statistic_ref_id = " . $stat_ref_id . "", OBJECT );

        $user_sort_answers = array();

        if( $questions ) {

            foreach( $questions as $question ) {

                $question_id = $question->question_id;
                
                $check_answer_type = $wpdb->get_results( "SELECT answer_type, answer_data FROM " . $wpdb->prefix . "wp_pro_quiz_question WHERE id = " . $question_id . "", OBJECT );

                if( $check_answer_type[0]->answer_type == 'sort_answer' ) {
                    
                    $correct_arr = json_decode($question->answer_data, true);
                    $user_arr = array();

                    $answer_data = unserialize($check_answer_type[0]->answer_data);

                    foreach( $correct_arr as $item ) {

                        for( $i = 0; $i <= (count($correct_arr) - 1); $i++ ) {

                            $gen = md5(  get_current_user_id() . $question_id . $i );

                            if($item == $gen) {

                                $reflection = new ReflectionClass($answer_data[$i]);
                                $property = $reflection->getProperty('_answer');
                                $property->setAccessible(true);
                                $main_answer = $property->getValue($answer_data[$i]);
                                $user_arr[] = array( 'text' => $main_answer, 'is_correct' => 1 );

                            }

                        }

                    }

                    $user_sort_answers[$question_id] = $user_arr;
                    
                }

            }

        }

        return $this->immerse_lms_ajax_success($user_sort_answers);

    }

    public function immerse_lms_ajax_populate_empty_quiz_statistics() {

        session_start();

        if( $_GET['run'] == 'yes' ) {

            global $wpdb;

            $correct_answers = array();
            
            $quiz_post_id_array_query = $wpdb->get_results( "SELECT ID FROM " . $wpdb->base_prefix . "posts WHERE post_type = 'sfwd-quiz'", OBJECT );
            
            foreach($quiz_post_id_array_query as $quiz_post_id) {

                $quiz_post_id = $quiz_post_id->ID;

                $quiz_pro_id = get_post_meta( $quiz_post_id, 'quiz_pro_id', true );

                $quiz_questions = $wpdb->get_results( "SELECT id, answer_type, answer_data FROM " . $wpdb->base_prefix . "wp_pro_quiz_question WHERE quiz_id = $quiz_pro_id AND (answer_type = 'single' || answer_type = 'multiple' || answer_type = 'sort_answer' || answer_type = 'matrix_sort_answer')" );

                $per_question = array();

                foreach($quiz_questions as $quiz_question_value) {
                
                    $answer_data = '';

                    if( $quiz_question_value->answer_type == 'single' || $quiz_question_value->answer_type == 'multiple' ) {
                        
                        $tag_values = array();

                        $unserialize_data = unserialize($quiz_question_value->answer_data);

                        foreach($unserialize_data as $unserialize_data_value) {
                            
                            $reflection = new ReflectionClass($unserialize_data_value);
                            $property = $reflection->getProperty('_correct');
                            $property->setAccessible(true);
                            if( $property->getValue($unserialize_data_value) ) {
                                $tag_values[] = 1;
                            } else {
                                $tag_values[] = 0;
                            }

                        }

                        $answer_data = json_encode($tag_values);

                        $per_question[] = array(
                            'statistic_ref_id' => '', // data will be from the main script generation
                            'question_id' => $quiz_question_value->id,
                            'correct_count' => 1,
                            'incorrect_count' => 0,
                            'hint_count' => 0,
                            'points' => 1,
                            'question_time' => 20,
                            'answer_data' => $answer_data
                        );

                    }

                    if( $quiz_question_value->answer_type == 'sort_answer' || $quiz_question_value->answer_type == 'matrix_sort_answer' ) {
                        
                        $tag_values = array();

                        $unserialize_data = unserialize($quiz_question_value->answer_data);

                        $per_question[] = array(
                            'statistic_ref_id' => '', // data will be from the main script generation
                            'question_id'      => $quiz_question_value->id,
                            'correct_count'    => 1,
                            'incorrect_count'  => 0,
                            'hint_count'       => 0,
                            'points'           => 1,
                            'question_time'    => 20,
                            'sort_data'        => count($unserialize_data)
                        );

                    }

                }

                $correct_answers[$quiz_pro_id] = $per_question;

            }

            $db_items = array();

            $users = get_users( array( 'fields' => array( 'ID' ) ) );

            foreach($users as $user) {

                $user_id = $user->ID;

                // Retrieve lesson progress from user meta
                $usermeta_sfwd_quizzes = get_user_meta($user_id, '_sfwd-quizzes', true);

                if( $usermeta_sfwd_quizzes ) {
                    
                    foreach( $usermeta_sfwd_quizzes as $usermeta_sfwd_quiz_key => $usermeta_sfwd_quiz_value ) {
                        
                        // Filter only those without statistics
                        if( !$usermeta_sfwd_quiz_value['statistic_ref_id'] ) {

                            if(
                                ( $usermeta_sfwd_quiz_value['percentage'] == 100 && $usermeta_sfwd_quiz_value['total_points'] > 0 && !$usermeta_sfwd_quiz_value['graded'] ) ||
                                ( $usermeta_sfwd_quiz_value['percentage'] <= 0 && $usermeta_sfwd_quiz_value['graded'] )
                            ) {

                                $inserted_quiz_statistic_ref_id = '';
                                
                                // PAUSED
                                $wpdb->insert( 
                                    $wpdb->base_prefix . 'wp_pro_quiz_statistic_ref', 
                                    array( 
                                        'quiz_id'     => $usermeta_sfwd_quiz_value['pro_quizid'], 
                                        'user_id'     => $user_id,
                                        'create_time' => $usermeta_sfwd_quiz_value['started']
                                    )
                                );

                                // PAUSED
                                $inserted_quiz_statistic_ref_id = $wpdb->insert_id;
                                $usermeta_sfwd_quizzes[$usermeta_sfwd_quiz_key]['statistic_ref_id'] = $inserted_quiz_statistic_ref_id;

                            }

                            $merged_correct_answers = array();

                            ///////// merge non essay answers

                            if( $usermeta_sfwd_quiz_value['percentage'] == 100 && $usermeta_sfwd_quiz_value['total_points'] > 0 && !$usermeta_sfwd_quiz_value['graded'] ) {

                                $correct_non_essay_answers = $correct_answers[$usermeta_sfwd_quiz_value['pro_quizid']];

                                if( $correct_non_essay_answers ) {

                                    foreach($correct_non_essay_answers as $key => $value) {

                                        $correct_non_essay_answers[$key]['statistic_ref_id'] = $inserted_quiz_statistic_ref_id;

                                        if( $correct_non_essay_answers[$key]['sort_data'] ) {

                                            $count = $correct_non_essay_answers[$key]['sort_data'];
                                            $question_id = $correct_non_essay_answers[$key]['question_id'];

                                            $datapos_array = array();

                                            for ( $i = 0; $i < $count; $i++ ) {
                                                $datapos_array[ $i] = md5( $user_id . $question_id . $i );
                                            }

                                            $correct_non_essay_answers[$key]['answer_data'] = json_encode($datapos_array);

                                        }

                                    }

                                    // merge to main
                                    $merged_correct_answers = array_merge($merged_correct_answers, $correct_non_essay_answers);

                                }

                            }

                            ///////// merge essay answers

                            if( $usermeta_sfwd_quiz_value['percentage'] <= 0 && $usermeta_sfwd_quiz_value['graded'] ) {

                                $user_essay_answers_data = $usermeta_sfwd_quiz_value['graded'];

                                $quiz_pro_id = $usermeta_sfwd_quiz_value['pro_quizid'];
                            
                                $quiz_essay_questions = $wpdb->get_results( "SELECT id FROM " . $wpdb->base_prefix . "wp_pro_quiz_question WHERE quiz_id = $quiz_pro_id AND answer_type = 'essay'" );

                                $user_essay_answers = array();

                                foreach($quiz_essay_questions as $quiz_essay_question_value) {
                                    
                                    if( $user_essay_answers_data[$quiz_essay_question_value->id]['post_id'] ) {

                                        $answer_data = '{"graded_id":' . $user_essay_answers_data[$quiz_essay_question_value->id]['post_id'] . '}';

                                        $points_awarded = $usermeta_sfwd_quizzes[$usermeta_sfwd_quiz_key]['graded'][$quiz_essay_question_value->id]['points_awarded'];

                                        if( $points_awarded == 0 ) {
                                            $correct_count = 0;
                                            $incorrect_count = 1;
                                            $points = 0;
                                        } else {
                                            $correct_count = 1;
                                            $incorrect_count = 0;
                                            $points = 1;
                                        }

                                        $user_essay_answers[] = array(
                                            'statistic_ref_id' => $inserted_quiz_statistic_ref_id, // data will be from the main script generation
                                            'question_id'      => $quiz_essay_question_value->id,
                                            'correct_count'    => $correct_count,
                                            'incorrect_count'  => $incorrect_count,
                                            'hint_count'       => 0,
                                            'points'           => $points,
                                            'question_time'    => 20,
                                            'answer_data'      => $answer_data
                                        );

                                    }

                                }

                                $merged_correct_answers = array_merge($merged_correct_answers, $user_essay_answers);

                            }

                            foreach($merged_correct_answers as $generated_answers) {

                                // PAUSED
                                $wpdb->insert( 
                                    $wpdb->base_prefix . 'wp_pro_quiz_statistic', 
                                    array(
                                        'statistic_ref_id' => $generated_answers['statistic_ref_id'],
                                        'question_id'      => $generated_answers['question_id'],
                                        'correct_count'    => $generated_answers['correct_count'],
                                        'incorrect_count'  => $generated_answers['incorrect_count'],
                                        'hint_count'       => $generated_answers['hint_count'],
                                        'points'           => $generated_answers['points'],
                                        'question_time'    => $generated_answers['question_time'],
                                        'answer_data'      => $generated_answers['answer_data']
                                    )
                                );

                            }

                            if( $merged_correct_answers ) {
                                $db_items[$user_id][$usermeta_sfwd_quiz_value['pro_quizid']] = $merged_correct_answers;
                            }

                            // PAUSED
                            update_user_meta($user_id, '_sfwd-quizzes', $usermeta_sfwd_quizzes);

                        }

                    }

                }

            }

            return $this->immerse_lms_ajax_success($db_items);

        }

    }

    /**
     * Exports the quiz attempt data for a given user. This function is based on the code which
     * LearnDash uses in the "Users" view in the WP dashboard.
     */
    public function immerse_lms_ajax_export_quiz_data() {
        $user_id = $_POST['user_id'];

        if (!$user_id) {
            return $this->immerse_lms_ajax_failure(['message' => 'User ID is required.']);
        }

        if (!current_user_can('administrator') && !current_user_can('tutor_admin')) {
            return $this->immerse_lms_ajax_failure(['message' => 'You are unauthorized.']);
        }

        $userId = $user_id;
        return $this->immerse_lms_ajax_success(apply_filters('immerse_lms_get_quiz_data', $userId));
    }

    public function immerse_lms_ajax_save_course_flow() {
        $user_id = $_POST['user_id'];
        $course_id = $_POST['course_id'];
        $course_flow = isset($_POST['course_flow']) ? $_POST['course_flow'] : array();
        
        if (!$user_id || !$course_id) {
            return $this->immerse_lms_ajax_failure(['message' => 'User ID is required.']);
        }

        // Validate course flow.
        try {
            if (!is_array($course_flow)) {
                throw new \Exception();
            }

            foreach ($course_flow as $lesson) {
                if (!isset($lesson['id']) || !$lesson['id'] || !isset($lesson['children']) || !is_array($lesson['children'])) {
                    throw new \Exception();
                }

                // Make sure the lesson ID is actually a lesson.
                $lesson_post = get_post($lesson['id']);

                if (!$lesson_post || $lesson_post->post_type != 'sfwd-lessons') {
                    throw new \Exception();
                }

                foreach ($lesson['children'] as $child) {
                    $child_post = get_post($child);

                    if (!$child_post || ($child_post->post_type != 'sfwd-topic' && $child_post->post_type != 'sfwd-quiz')) {
                        throw new \Exception();
                    }
                }
            }
        } catch (\Exception $e) {
            return $this->immerse_lms_ajax_failure(['message' => 'Invalid course flow schema.']);
        }

        update_post_meta($course_id, '_custom_course_flow_' . $user_id, $course_flow);
        return $this->immerse_lms_ajax_success();
    }

    function immerse_lms_die() {
        throw new \Exception();
    }

	/**
	 * Creates a grading sheet and emails the tutor. Also checks if the quiz is locked and exits beforehand if so.
	 */
	function immerse_lms_wp_pro_quiz_completed_quiz() {
        $quiz = $_POST['quiz'];
        $quiz = get_post($quiz);
        $user = wp_get_current_user();

        if($this->immerse_lms_quiz_locked($quiz, $user)) {
            throw new \Exception();
        }

        register_shutdown_function(function() {
            $quiz = $_POST['quiz'];

            $categories = wp_get_post_categories($quiz, array(
                'fields' => 'slugs'
            ));
    
            if (in_array('assessments', $categories)) {
                $quiz = get_post($quiz);
                $user = wp_get_current_user();
                $course = apply_filters('immerse_lms_get_course_for_quiz', $quiz->ID, $user->ID);
                $phone = get_user_meta($user->ID, 'phone_number', true);
                $quizName = $quiz->post_title;
                $quizProId = apply_filters('immerse_lms_get_quiz_pro_id', $quiz->ID);

                // Get the latest statistic ref ID for the quiz by the user.
                $statistic_ref_id = apply_filters('immerse_lms_get_last_statistic_ref_id', $user->ID, $quizProId);

                $assessment_grading = create_assessment_grading($statistic_ref_id);
                
                $marking_guide = get_posts(array(
                    'numberposts' => 1,
                    'post_type' => 'marking-guide',
                    'meta_query' => array(
                        array(
                            'key' => '_quiz',
                            'value' => $quiz->ID
                        )
                    )
                ));

                if (count($marking_guide) > 0) {
                    $marking_guide = $marking_guide[0];
                } else {
                    $marking_guide = null;
                }

                $users = get_field('assigned_users', $course);
                $emails = array();

                foreach ($users as $u) {
                    $email = $u['user']['user_email'];

                    if ($email) {
                        $emails[] = $email;
                    }
                }

                if (count($emails)) {
                    foreach ($emails as $email) {
                        Immerse_Lms_Email::send_email($email, 'assessment_completed', array(
                            'user_name' => $user->display_name,
                            'user_phone' => $phone,
                            'user_email' => $user->user_email,
                            'course_link' => get_the_permalink($course),
                            'course_title' => $course->post_title,
                            'quiz_link' => site_url("/assessment-grading-sheet/?gid=" . $assessment_grading['grading']->ID),
                            'quiz_title' => $quizName,
                            'marking_guide' => $marking_guide,
                            'marking_guide_link' => get_the_permalink($marking_guide),
                            'marking_guide_title' => $marking_guide->post_title
                        ));
                    }
                }
            }
        });
    }

    public function immerse_lms_notifications_get_registered_components( $component_names = array() ) {
        if (!is_array($component_names)) {
            $component_names = array();
        }
        
        $component_names[] = $this->plugin_name;

        return $component_names;
    }

    protected $pre_read_action;

    function immerse_lms_pre_read_format_buddypress_notifications( $action, $item_id, $secondary_item_id, $total_items, $format = 'string' ) {
        $this->pre_read_action = $action;
        return $action;
    }

    function immerse_lms_format_buddypress_notifications( $action, $item_id, $secondary_item_id, $total_items, $format = 'string' ) {
        // The action gets pre-read because the buddypress filter returns null.
        $action = $this->pre_read_action;

        if (strpos($action, $this->plugin_name . '_') === 0) {
            $action_name = substr($action, strlen($this->plugin_name) + 1);

            if (isset($this->notifications[$action_name])) {
                $callback = $this->notifications[$action_name];
                $notification = $this->$callback($action_name, $item_id, $secondary_item_id, $total_items);
                $notification_converted = $notification;

                if ($format == 'string') {
                    $notification_converted = '<a href="' . esc_url( $notification['link'] ) . '" title="' . esc_attr( $notification['title'] ) . '">' . esc_html( $notification['text'] ) . '</a>';
                }

                return apply_filters( 'immerse_lms_format_notification', $notification_converted, $notification['link'], (int) $total_items, $notification['text'], $notification['title'] );
            }
        }
    }

    public function immerse_lms_add_notification($action, $user_id, $item_id = 0, $secondary_item_id = null) {
        bp_notifications_add_notification( array(
            'user_id'           => $user_id,
            'item_id'           => $item_id,
            'secondary_item_id' => $secondary_item_id,
            'component_name'    => $this->plugin_name,
            'component_action'  => $this->plugin_name . '_' . $action,
            'date_notified'     => bp_core_current_time(),
            'is_new'            => 1
        ) );
    }

    public function immerse_lms_format_test_notification($action, $item_id, $secondary_item_id, $total_items) {
        return array(
            'text' => 'Test notification',
            'link' => 'https://www.google.com/',
            'title' => 'Test'
        );
    }

    public function immerse_lms_format_notification_user_online($action, $item_id, $secondary_item_id, $total_items) {
        $options = get_option($this->plugin_name . '_options');
        $user = get_user_by('ID', $item_id);
    
        if ($action == 'user_online') {
            return array(
                'text' => str_replace('{name}', $user->display_name, $options['immerse_lms_field_online_notification']),
                'title' => str_replace('{name}', $user->display_name, $options['immerse_lms_field_online_notification']),
                'link' => ''
            );
        }

        if ($action == 'user_online_diploma') {
            return array(
                'text' => str_replace('{name}', $user->display_name, $options['immerse_lms_field_online_diploma_notification']),
                'title' => str_replace('{name}', $user->display_name, $options['immerse_lms_field_online_diploma_notification']),
                'link' => ''
            );
        }
	}
	
	/* 
	* Add a View Payment Button related to custom field at course edit page
	*/
	function immerse_lms_course_products_shortcode( $atts ) {
		global $post;
		$course_meta = get_post_meta( $post->ID, '_sfwd-courses', false);

		//echo $product->get_price_html();
		if ($course_meta[0]['sfwd-courses_immerse_lms_payment_plan_url']) {
			?>
			<a class="btn-join" href="<?= $course_meta[0]['sfwd-courses_immerse_lms_payment_plan_url']?>" id="btn-join">View Payment Plans</a>
			<?php
		}
		
	}
	
	/**
     * Add payment plan field in course setting metabox
     * @param $post_args
     * @return mixed
     */
    public function immerse_lms_learndash_post_args( $post_args ) {

        foreach ( $post_args as  $key => $arg ) {

            //Add Hide Price Tag in course edit metabox
            if ( is_array( $arg )
				&& isset( $arg['post_type'] )
				&& 'sfwd-courses' === $arg['post_type'] ) {

                $post_args[$key]['fields']['immerse_lms_payment_plan_url'] =  array(                    
					'name' => esc_html__( 'Payment Plan Product URL', 'learndash' ),
					'type' => 'text',
					'placeholder'	=> esc_html__( 'Optional', 'learndash' ),
					'help_text' => sprintf('Entering a URL in this field will enable the "View Payment Plans" button. The button will not display if this field is left empty.')
					
                );
            }
        }

        return $post_args;
    }

    public function immerse_lms_ajax_test_hourly() {
        do_action('immerse-lms_every_hour');
        die();
    }

    public function immerse_lms_bp_profile_content() {
        $user_id = get_current_user_id();
        $pin = get_user_meta($user_id, '_vreddo-pin', true);

        if (!$pin) {
            $pin = rand(100000, 999999);
            update_user_meta($user_id, '_vreddo-pin', $pin);
        }
        
        // Expire in 10 years.
        $expiry = time() + (10 * 365 * 24 * 60 * 60); /*+ (10 * 60);*/
        update_user_meta($user_id, '_vreddo-pin-expiry', $expiry);

        ?>
        <h2>PIN</h2>
        <p>The following PIN has been generated for you. <!-- It will expire in 10 minutes. !--></p>
        <h3><?= $pin ?></h3>
        <?php
    }

    public function get_update_objects_for_sugar_trainee($sugarcrm_client, $data) {
        $meta_updates = array();
        $user_update = array();

        $user_update['display_name'] = $data->name;
        $meta_updates['nickname'] = $data->name;
        $meta_updates['first_name'] = $data->first_name;
        $meta_updates['last_name'] = $data->last_name;
        $meta_updates['parent_email'] = $data->guardian_email;
        $user_update['user_email'] = $data->email1;

        error_log('Checking for school...');

        if (isset($data->rm3_schools_rm1_manage_trainee_1rm3_schools_ida) && $data->rm3_schools_rm1_manage_trainee_1rm3_schools_ida) {
            error_log('School found with ID: ' . $data->rm3_schools_rm1_manage_trainee_1rm3_schools_ida);

            // Retrieve the school.
            try {
                $school = $sugarcrm_client->get_school($data->rm3_schools_rm1_manage_trainee_1rm3_schools_ida);
                
                if (isset($school->email1) && $school->email1) {
                    $meta_updates['school_email'] = $school->email1;
                } else {
                    $meta_updates['school_email'] = $school->email1;
                }
            } catch (\Exception $e) {
                error_log('Couldn\'t retrieve school: ' . $e->getMessage());
            }
        } else {
            $meta_updates['school_email'] = null;
        }

        return array(
            'user' => $user_update,
            'meta' => $meta_updates
        );
    }

    public function immerse_lms_root_courses_only($query) {
        // get_current_screen() doesn't work at this point in the code. We'l lhave to verify the URL ourselves
        // with this $_SERVER check.
        if ($_SERVER['REQUEST_URI'] != '/wp-admin/edit.php?post_type=sfwd-courses') {
            if (isset($query->query['post_type']) && $query->query['post_type'] == 'sfwd-courses') {
                if (!isset($query->query_vars['post_parent']) || !$query->query_vars['post_parent']) {
                    $query->query_vars['post_parent'] = 0;
                }
            }
        }
    }

    public function immerse_lms_add_hierarchy($post_type) {
        if ($post_type == 'sfwd-courses') {
            global $wp_post_types;
            $wp_post_types['sfwd-courses']->hierarchical = 1;
        }
    }

    public function immerse_lms_ajax_public_sugarcrm_callback() {
        error_log('SugarCRM callback...');

        $posted = file_get_contents('php://input');
        //error_log($posted);
        $posted = json_decode($posted);
        $sugarcrm_id = $posted->data->id;

        // Retrieve the SugarCRM client (defined in rm-boss-child-theme)
        $sugarcrm_client = rm_sugarcrm_client();

        if ($posted->isUpdate) {
            // Find a user with that sugarCRM ID.
            $users = get_users(array(
                'meta_key' => 'sugarcrm_id',
                'meta_value' => $sugarcrm_id,
                'meta_compare' => 'LIKE'
            ));

            error_log('Found ' . count($users) . ' users with ID ' . $sugarcrm_id);

            if (count($users) > 0) {
                error_log('Making updates...');
                $user = $users[0];

                // Check data changes.
                $updates = $this->get_update_objects_for_sugar_trainee($sugarcrm_client, $posted->data);

                $user_update = $updates['user'];
                $user_update['ID'] = $sugarcrm_id;
                $meta_updates = $updates['meta'];
                error_log('Updating ' . count($meta_updates) . ' meta values and ' . (count($user_update) - 1) . ' user values for ' . $user->ID . '...');

                foreach ($meta_updates as $key => $value) {
                    update_user_meta($user->ID, $key, $value);
                }

                if (count($user_update) > 0) {
                    wp_update_user($user_update);
                }

                error_log('Updated.');
            }
        } else {
            error_log('Creating user...');

            // Get the update data.
            $updates = $this->get_update_objects_for_sugar_trainee($sugarcrm_client, $posted->data);
            $user_update = $updates['user'];
            $meta_updates = $updates['meta'];

            // Save the ID.
            $meta_updates['sugarcrm_id'] = $sugarcrm_id;

            // Work out the username.
            $first_name = str_replace(' ', '', $posted->data->first_name);
            $last_name = str_replace(' ', '', $posted->data->last_name);
            error_log($first_name . ' ' . $last_name);
            $user_first_part = '';

            for ($i = 0; $i < strlen($first_name); $i++) {
                $letter = $first_name[$i];
                $user_first_part .= $letter;
                $username = $user_first_part . $last_name;
                $existing_user = get_user_by('login', $username);

                if (!$existing_user) {
                    break;
                }
            }

            error_log('Got username: ' . $username);

            // Work out the password.
            $password = $first_name . $last_name;

            // Add username and password to update.
            $user_update['user_login'] = $username;
            $user_update['user_pass'] = $password;

            // Create the user.
            $user_id = wp_insert_user($user_update);

            if (is_wp_error($user_id)) {
                error_log('Error: ' . json_encode($user_id));
                wp_send_json_error($user_id);
                return;
            } else {
                foreach ($meta_updates as $key => $value) {
                    update_user_meta($user_id, $key, $value);
                }

                error_log('Created.');
            }

        }

        wp_send_json_success();
    }

    public function immerse_lms_ajax_public_masquerade() {
        $uid = filter_input(INPUT_POST, 'uid', FILTER_SANITIZE_NUMBER_INT);
		$reset = filter_input(INPUT_POST, 'reset', FILTER_VALIDATE_BOOLEAN);

        if ($reset || current_user_can('administrator') || current_user_can('marker') || current_user_can('trainer') || current_user_can('tutor_admin')) {
            if(!$reset && !$uid)
                wp_die('Security Check');

            session_start();

            if($reset && !isset($_SESSION['immerse_lms_masquerade_active']))
                wp_logout();

            $user_id = $reset ? $_SESSION['immerse_lms_masquerade_active']->ID : $uid;
            $user_name = $reset ? $_SESSION['immerse_lms_masquerade_active']->user_login : get_userdata($uid)->user_login;

            // Flush the session if user requests reset OR user attempts to masquerade as the original user
            if($reset || (isset($_SESSION['immerse_lms_masquerade_active']) && $_SESSION['immerse_lms_masquerade_active']->ID == $uid)) {
                unset($_SESSION['immerse_lms_masquerade_active']);
            } elseif(!$reset && !isset($_SESSION['immerse_lms_masquerade_active'])) {
                $_SESSION['immerse_lms_masquerade_active'] = wp_get_current_user();
            }

            session_write_close();

            wp_set_current_user($user_id, $user_name);
            wp_set_auth_cookie($user_id);
            do_action('wp_login', $user_name, wp_get_current_user());

            echo wp_get_current_user()->ID == $user_id ? 1 : 0;
            exit();
        }
    }

    public function immerse_lms_show_masqueraded() {
		if(isset($_SESSION['immerse_lms_masquerade_active'])){
			$prev_user = $_SESSION['immerse_lms_masquerade_active'];
            echo $this->render_partial('public/partials/immerse-lms-public-masqueraded.php');
		}
    }

    /**
     * Custom masquerade
     */
    public function custom_masquerade() {

        if( is_user_logged_in() ) {

            $user = wp_get_current_user();
            $role = ( array ) $user->roles;

            // filter for marker, administrator, trainer and tutor admin only
            if( in_array('marker', $role) ||
                in_array('administrator', $role) ||
                in_array('trainer', $role) ||
                in_array('tutor_admin', $role) ) {

                // remove original plugin script and add the custom one
                wp_dequeue_script( 'buddypress-global-search' );
                wp_enqueue_script('custom-buddypress-global-search', plugin_dir_url(__FILE__) . 'js/buddypress-global-search.js', array('jquery'), $this->version, false);

                // will be used for localize script to pass variables from server side to client side
                $data = array(
                    'nonce'     => wp_create_nonce( 'bboss_global_search_ajax' ),
                    'action'    => 'bboss_global_search_ajax',
                    'debug'     => true,//set it to false on production
                    'ajaxurl'   => admin_url( 'admin-ajax.php', is_ssl() ? 'admin' : 'http' ),
                    //'search_url'    => home_url( '/' ), Now we are using form[role='search'] selector
                    'loading_msg'    => __("Loading Suggestions","buddypress-global-search"),
                    'enable_ajax_search'    => 'yes',
                    'per_page' => 5
                );
                            
                if(isset($_GET["s"])) {
                    $data["search_term"] = $_GET["s"];
                }

                wp_localize_script( 'custom-buddypress-global-search', 'BBOSS_GLOBAL_SEARCH', $data );
            }

        } 

    }

}
