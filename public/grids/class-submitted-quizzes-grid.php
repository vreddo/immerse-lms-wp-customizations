<?php
require_once(__DIR__ . '/../class-immerse-lms-grid.php');

class Submitted_Quizzes_Grid extends Immerse_Lms_Grid
{
    public function get_filters() {
        return array(
            array(
                'name' => 'course',
                'label' => 'Course',
                'type' => 'text'
            ),
            array(
                'name' => 'quiz',
                'label' => 'Quiz',
                'type' => 'text'
            ),
            array(
                'name' => 'trainee',
                'label' => 'Trainee',
                'type' => 'text'
            ),
            array(
                'name' => 'submitted',
                'label' => 'Submitted',
                'type' => 'date_fromto'
            ),
            array(
                'name' => 'grade',
                'label' => 'Assessment Grade',
                'type' => 'select',
                'options' => array(
                    'ungraded' => 'Ungraded',
                    'notcompetent' => 'Not Yet Competent',
                    'competent' => 'Competent'
                )
            ),
            array(
                'name' => 'marker',
                'label' => 'Marker',
                'type' => 'text'
            ),
            array(
                'name' => 'status',
                'label' => 'Status',
                'type' => 'select',
                'options' => array(
                    'unread' => 'Unread',
                    'read' => 'Read',
                    'graded' => 'Graded'
                )
            )
        );
    }

    function build_filter_query($filters) {
        $clauses = array();
        $vars = array();
        
        if (isset($filters['course'])) {
            $clauses[] = 'course_title LIKE %s';
            $vars[] = '%' . $filters['course'] . '%';
        }

        if (isset($filters['quiz'])) {
            $clauses[] = 'quiz_title LIKE %s';
            $vars[] = '%' . $filters['quiz'] . '%';
        }

        if (isset($filters['trainee'])) {
            $clauses[] = 'student_name LIKE %s';
            $vars[] = '%' . $filters['trainee'] . '%';
        }

        if (isset($filters['submitted_from'])) {
            $clauses[] = 'create_time >= ' . strtotime($filters['submitted_from']);
        }

        if (isset($filters['submitted_to'])) {
            $clauses[] = 'create_time <= ' . strtotime($filters['submitted_to']);
        }

        if (isset($filters['marker'])) {
            $clauses[] = 'assessment_marker LIKE %s';
            $vars[] = '%' . $filters['marker'] . '%';
        }

        if (isset($filters['grade'])) {
            switch ($filters['grade']) {
                case 'competent':
                    $clauses[] = 'assessment_status = %s AND ' . $this->create_points_sub_query('stat.points') . ' = max_points';
                    $vars[] = 'graded';
                    break;
                case 'notcompetent':
                    $clauses[] = 'assessment_status = %s AND ' . $this->create_points_sub_query('stat.points') . ' < max_points';
                    $vars[] = 'graded';
                    break;
                default:
                    $clauses[] = 'assessment_status <> %s';
                    $vars[] = 'graded';
                    break;
            }
        }

        if (isset($filters['status'])) {
            $clauses[] = 'assessment_status = %s';
            $vars[] = $filters['status'];
        }

        return array(implode(' AND ', $clauses), $vars);
    }

    public function get_columns() {
        return array(
            array(
                'label' => 'Course',
                'func' => function($row) {
                    ?>
                    <a href="<?= esc_attr(get_the_permalink($row->course_id)) ?>"><?= esc_html($row->course_title) ?></a>
                    <?php
                }
            ),
            array(
                'label' => 'Quiz',
                'func' => function($row) {
                    ?>
                    <a href="<?= esc_attr(get_the_permalink($row->quiz_post_id)) ?>"><?= esc_html($row->quiz_title) ?></a>
                    <?php
                }
            ),
            array(
                'label' => 'Trainee',
                'func' => function($row) {
                    ?>
                    <a href="<?= esc_attr(get_edit_user_link($row->student_id)) ?>"><?= esc_html($row->student_name) ?></a>
                    <?php
                }
            ),
            array(
                'label' => 'Submitted',
                'func' => function($row) {
                    echo date(get_option('date_format'), $row->create_time);
                }
            ),
            array(
                'label' => 'Grade',
                'func' => function($row) {
                    ?>
                    <?= $row->grade ?>/<?= $row->max_grade ?> (<?= floor($row->points / $row->max_points * 100) ?>%)
                    <?php
                }
            ),
            array(
                'label' => 'Grade *',
                'func' => function($row) {
                    if ($row->assessment_status != 'graded') {
                        ?>Ungraded<?php
                    } else {
                        if ($row->points == $row->max_points) {
                            ?>Competent<?php
                        } else {
                            ?>Not Yet Competent<?php
                        }
                    }
                }
            ),
            array(
                'label' => 'Marker *',
                'func' => function($row) {
                    echo $row->assessment_marker;
                }
            ),
            array(
                'label' => 'Status *',
                'func' => function($row) {
                    $status = $row->assessment_status;

                    switch ($status) {
                        case 'unread':
                            $status = 'Unread';
                            break;
                        case 'read':
                            $status = 'Read';
                            break;
                        case 'graded':
                            $status = 'Graded';
                            break;
                    }

                    echo $status;
                }
            ),
            array(
                'label' => '',
                'func' => function($row) {
                    ?>
                    <a href="<?= esc_attr(site_url('/assessment-grading-sheet/?gid=' . $row->assessment_grading_id)) ?>">View</a>
                    <?php
                }
            )
        );
    }

    function create_points_sub_query($points_column) {
        global $wpdb;

        return "(
            SELECT
                SUM(
                    CASE WHEN stat.answer_data LIKE '{\"graded_id\":%}' THEN
                    COALESCE(LEFT(
                        SUBSTRING(
                            graded_points.meta_value,
                            LOCATE(CONCAT('{s:7:\"post_id\";i:', REPLACE(REPLACE(stat.answer_data, '{\"graded_id\":', ''), '}', ''), ';s:6:\"status\";s:6:\"graded\";s:14:\"points_awarded\";i:') COLLATE utf8_general_ci, graded_points.meta_value) +
                            LENGTH(CONCAT('{s:7:\"post_id\";i:', REPLACE(REPLACE(stat.answer_data, '{\"graded_id\":', ''), '}', ''), ';s:6:\"status\";s:6:\"graded\";s:14:\"points_awarded\";i:'))
                        )
                    , 1), $points_column)
                    ELSE
                    $points_column
                    END
                )
            FROM
                $wpdb->statistic stat
            INNER JOIN
                $wpdb->statistic_ref ref
                ON
                    ref.statistic_ref_id = stat.statistic_ref_id
            LEFT JOIN
                $wpdb->usermeta graded_points
                ON
                    graded_points.meta_key = '_sfwd-quizzes' AND
                    graded_points.user_id = ref.user_id
            WHERE
                stat.statistic_ref_id = a.statistic_ref_id
        )";
    }

    function build_sql($filters = array(), $sort, $page = 1) {
        global $wpdb;
        $wpdb->statistic_ref = $wpdb->prefix . 'wp_pro_quiz_statistic_ref';
        $wpdb->statistic = $wpdb->prefix . 'wp_pro_quiz_statistic';
        $wpdb->question = $wpdb->prefix . 'wp_pro_quiz_question';
        $vars = array();

        $sql = "
        SELECT
        ";

        if ($page == -1) {
            $sql .= 'COUNT(*) AS count';
        } else {
            $sql .= '*';
            $sql .= ', ';
            $sql .= $this->create_points_sub_query('stat.points') . ' AS \'points\', ';
            $sql .= $this->create_points_sub_query('stat.correct_count') . ' AS \'grade\'';
        }

        $sql .= "
        FROM
            (
                SELECT
                    sref.statistic_ref_id AS 'statistic_ref_id',
                    sref.quiz_id,
                    sref.create_time,
                    course_post.ID AS 'course_id',
                    course_post.post_title AS 'course_title',
                    quiz_post.ID AS 'quiz_post_id',
                    quiz_post.post_title AS 'quiz_title',
                    student.ID AS 'student_id',
                    student.display_name AS 'student_name',
                    assessment_post.ID AS 'assessment_grading_id',
                    assessment_post.post_status AS 'assessment_status',
                    assessment_post.post_title AS 'assessment_marker',
                (
                    SELECT
                        SUM(q.points)
                    FROM
                        $wpdb->statistic stat
                    INNER JOIN
                        $wpdb->question q
                        ON
                            q.id = stat.question_id
                    WHERE
                        stat.statistic_ref_id = sref.statistic_ref_id
                ) AS 'max_points',
                (
                    SELECT
                        SUM(correct_count) + SUM(incorrect_count)
                    FROM
                        $wpdb->statistic stat
                    WHERE
                        stat.statistic_ref_id = sref.statistic_ref_id
                ) AS 'max_grade'
            FROM
                $wpdb->statistic_ref sref
            LEFT JOIN
                $wpdb->postmeta quiz_postmeta
                ON
                    quiz_postmeta.meta_key = 'quiz_pro_id' AND
                    quiz_postmeta.meta_value = sref.quiz_id
            INNER JOIN
                $wpdb->posts quiz_post
                ON
                    quiz_post.ID = quiz_postmeta.post_id AND
                    quiz_post.post_type = 'sfwd-quiz'
            LEFT JOIN
                $wpdb->postmeta course_postmeta
                ON
                    course_postmeta.meta_key LIKE 'ld_course_%' AND
                    course_postmeta.post_id = quiz_post.ID
            LEFT JOIN
                $wpdb->posts course_post
                ON
                    course_post.ID = course_postmeta.meta_value
            LEFT JOIN
                $wpdb->users student
                ON
                    student.ID = sref.user_id
            LEFT JOIN
                $wpdb->postmeta assessment_postmeta
                ON
                    assessment_postmeta.meta_key = '_statistic-ref-id' AND
                    assessment_postmeta.meta_value = sref.statistic_ref_id
            INNER JOIN
                $wpdb->posts assessment_post
                ON
                    assessment_post.ID = assessment_postmeta.post_id AND
                    assessment_post.post_type = 'assessment_grading'
            INNER JOIN
                $wpdb->usermeta course_usermeta
                ON
                    course_usermeta.user_id = student.ID AND
                    course_usermeta.meta_key LIKE CONCAT('course_', course_post.ID, '_access_from')
            ) a
        ";

        if ($filters && count($filters) > 0) {
            $filter = $this->build_filter_query($filters);
            $sql .= "WHERE " . $filter[0];
            $vars = array_merge($vars, $filter[1]);
        }

        if ($page != -1) {
            if ($sort) {
            } else {
                $sql .= "\nORDER BY create_time DESC";
            }

            $sql .= "\nLIMIT 10 OFFSET " . (($page - 1) * 10);
        }

        return $wpdb->prepare($sql, $vars);
    }

    public function count_pages($filters, $sort) {
        global $wpdb;
        $count = $wpdb->get_col($this->build_sql($filters, $sort, -1))[0];
        return floor($count / 10) + 1;
    }

    public function get_rows($filters, $sort, $page = 1) {
        global $wpdb;
        $results = $wpdb->get_results($this->build_sql($filters, $sort, $page));
        return $results;
    }
}