<?php
require_once(__DIR__ . '/../class-immerse-lms-grid.php');

class Vr_User_Uploads_Grid extends Immerse_Lms_Grid
{
    public function get_filters() {
        return array(
            array(
                'name' => 'type',
                'label' => 'Type',
                'type' => 'select',
                'options' => array(
                    'audio' => 'Audio Note',
                    'video' => 'Video',
                    'image' => 'Screenshot'
                )
            )
        );
    }

    public function get_columns() {
        return array(
            array(
                'label' => '',
                'func' => function($row) {
                    $attachment = wp_get_attachment_url($row['attachment_id']);
                    
                    ?>
                    <table style="width: 150px; height: 150px; background-image: url(<?= $row['vr_content_type'] != 'audio' ? ($attachment . '_thumb.jpg') : null ?>);" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td style="vertical-align: bottom; text-align: right; color: white; font-size: 24px;">
                                    <?php
                                    switch ($row['vr_content_type']) {
                                        case 'audio':
                                            ?><i class="fa fa-volume-up"></i><?php
                                            break;
                                        case 'image':
                                            ?><i class="fa fa-image"></i><?php
                                            break;
                                        case 'video':
                                            ?><i class="fa fa-video"></i><?php
                                            break;
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <?php
                },
                'width' => '160px'
            ),
            array(
                'label' => 'Name',
                'func' => function($row) {
                    echo $row['title'];
                }
            ),
            array(
                'label' => 'Class',
                'func' => function($row) {
                    echo $row['class_publicname'];
                }
            ),
            array(
                'label' => 'Download',
                'func' => function($row) {
                    $attachment = wp_get_attachment_url($row['attachment_id']);
                    ?>
                    <a href="<?= esc_attr($attachment) ?>">Download</a>
                    <?php
                },
                'width' => '5%'
            )
        );
    }

	/*
	* Returns collection of vr_user_content posts VR User Content list shortcode
	*/
	public function get_vr_user_content($user_id, $vr_content_type = null, $posts_per_page = 10, $page = 1) {
		$args = array(
			'post_type' => 'vr_user_content',
			'author__in' => array( $user_id ),
			'posts_per_page' => $posts_per_page,	
		);

		if ($vr_content_type) {
			$args['meta_query'] = array(
				array(
					'key' => 'vr_content_type',
					'value' => $vr_content_type,
					'compare' => 'LIKE'
				),
			);
		}
	
		if ($page > 1) {
			$args['paged'] = $page;
		}

		$vr_contents = new WP_Query($args);

		if ( $vr_contents->post_count > 0 ) {
			
			foreach ($vr_contents->posts as $key => $vr_content) {
				$meta = get_post_meta( $vr_content->ID );
				$class_id = get_field('class_id', $vr_content->ID);
				
				$vr_user_content[$vr_content->ID]['ID'] = $vr_content->ID;
				$vr_user_content[$vr_content->ID]['title'] = $vr_content->post_title;
				$vr_user_content[$vr_content->ID]['description'] = get_field('description', $vr_content->ID);
				$vr_user_content[$vr_content->ID]['class_id'] = $class_id;
				$vr_user_content[$vr_content->ID]['class_publicname'] = get_field('class_public_title', $class_id[0]);
				$vr_user_content[$vr_content->ID]['url'] = get_field('file', $vr_content->ID);
				$vr_user_content[$vr_content->ID]['attachment_id'] = $meta['file'][0];		
				$vr_user_content[$vr_content->ID]['vr_content_type'] = get_field('vr_content_type', $vr_content->ID);		
				$vr_user_content[$vr_content->ID]['date'] = get_the_date('M j, Y - H:ia',$vr_content->ID);		
			}
        }

        else $vr_user_content = array();

        return array(
            'results' => $vr_user_content,
            'found' => $vr_contents->found_posts
        );
    }
    
    protected $result;

    public function get_rows($filters, $sort, $page = 1) {
        $this->result = $this->get_vr_user_content(get_current_user_id(), isset($filters['type']) ? $filters['type'] : null, 10, $page);
        return $this->result['results'];
    }

    public function count_pages($filters, $sort) {
        return 1;
    }
}