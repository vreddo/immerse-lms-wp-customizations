var STRIP_COMMENTS = /((\/\/.*$)|(\/\*[\s\S]*?\*\/))/mg;
var ARGUMENT_NAMES = /([^\s,]+)/g;
function getParamNames(func) {
  var fnStr = func.toString().replace(STRIP_COMMENTS, '');
  var result = fnStr.slice(fnStr.indexOf('(')+1, fnStr.indexOf(')')).match(ARGUMENT_NAMES);
  if(result === null)
     result = [];
  return result;
}

window.ImmerseLms_Ajax = function($) {
    this.$ = $;
};

ImmerseLms_Ajax.prototype = {
    execute: function(functionName, data) {
        var $ = this.$;

        return new Promise(function (resolve, reject) {
            $.ajax({
                method: "POST",
                url: ajaxurl,
                data: Object.assign({
					action: "immerse_lms_ajax_" + functionName
				}, data),
                success: function(data) {
                    if (data.success) {
                        resolve(data.data);
                    } else {
						var returned = data;

						if (returned.data) {
							returned = returned.data;
						}

						if (returned.message) {
							returned = returned.message;
						}

                        reject(new Error(returned));
                    }
                },
                error: function(xhr) {
                    reject(new Error(xhr.responseText));
                }
            });
        });
    },
    overrideQuizScore: function(quiz_id, user_id) {
        return this.execute("override_quiz_score", { quiz_id, user_id })
    },
    postNote: function(content, location, for_user) {
        return this.execute("post_note", { content, location, for_user });
    },
    saveNote: function(id, content) {
        return this.execute("save_note", { id, content });
    },
    exportQuizData: function(user_id) {
        return this.execute("export_quiz_data", { user_id });
    },
    saveCourseFlow: function(user_id, course_id, course_flow) {
        return this.execute("save_course_flow", { user_id, course_id, course_flow: course_flow });
	},
	countStatisticRefs: function(quiz_id) {
		return this.execute("count_statistic_refs", { quiz_id });
	},
	getQuizAttempt: function(quiz_id) {
		return this.execute("get_quiz_attempt", { quiz_id });
	},
	getStudentAnswer: function(quiz_id, user_id) {
        return this.execute("get_student_answer", { quiz_id, user_id })
	},
    populateEmptyQuizStatistics: function(run) {
        return this.execute("populate_empty_quiz_statistics", { run })
    }
};

(function( $ ) {
	'use strict';
	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */
	
	  // Loads vr User content list items
	function loadVRMediaAjax(e){
		e.preventDefault();
		
		var target = $(e.currentTarget);
		var id = target.data('id');
		var type = target.data('type');
		var page = Number(target.attr('data-page'));
		var posts_per_page = Number(target.attr('data-posts-per-page'));
		var found_posts = Number(target.attr('data-found-posts'));
		
		$('.vr_content_filter_type').parent('li').removeClass('current selected');
		target.parent('li').addClass('current selected');
		target.find('.spinner-load-more').css('visibility','visible');
		
		// console.log({
		// 	"id: " : id,
		// 	"page" : page,
		// 	"type" : type,
		// 	"posts_per_page": posts_per_page,
		// 	"found_posts": found_posts

		// 	}
		// )

		var action = 'immerse_lms_ajax_load_vr_user_media';
		var loadMediaSendData = {
			action: action,
			id: id,
			page: page,
			posts_per_page: posts_per_page,
			type: type,
			nonce: $('#post #hs-media-credits-nonce').val() 
		};
		$.post(ajaxurl, loadMediaSendData, function(response) {
			if(response === '0' || response === '-1'){
				//TODO: Add Error handling (Wrong nonce, no permissions, …) here.
				alert('Load Error')
			} else {
				if (page == 1){
					$('.vr_user_content_list').html(response.data.html);
				} else $('.vr_user_content_list').append(response.data.html);

				// set event click on ajax delete button
				$('.immerse_lms_delete_media').click(deleteMediaAjax);
				if (type == undefined) type = '';
				$('.immerse_lms_btn-load-more').attr('data-type', type);
				$('.immerse_lms_btn-load-more').attr('data-page', page + 1);
				$('.immerse_lms_btn-load-more').attr('data-found-posts', response.data.found_posts)

				if (found_posts / (posts_per_page * (page)) <= 1) {
					$('.immerse_lms_btn-load-more').attr("disabled", "disabled");
					$('.immerse_lms_btn-load-more').html("All content loaded");
				}
				else {
					$('.immerse_lms_btn-load-more').attr("disabled", false);
					$('.immerse_lms_btn-load-more').html("Load More");
				}
				
			}
			target.find('.spinner-load-more').css('visibility','hidden');
		});

		
		
		
	}

	 // Removes a vr User content list item
	function deleteMediaAjax(e){
		e.preventDefault();
		
		var target = $(e.currentTarget);
		var id = target.data('id');
		
		target.hide();
		target.next('.spinner').show();
		var action = 'immerse_lms_ajax_delete_upload';
		var deleteMediaSendData = {
			action: action,
			id: id,
			//nonce: $('#post #hs-media-credits-nonce').val() 
		};
		$.post(ajaxurl, deleteMediaSendData, function(response) {
			if(response === '0' || response === '-1'){
				//TODO: Add Error handling (Wrong nonce, no permissions, …) here.
				alert('Delete Error')
			} else {
				$('#immerse-lms-media-id-'+id).css('background-color', 'lightpink');
				$('#immerse-lms-media-id-'+id).fadeOut(1000);
				// .remove();
				//console.log(response);
			}
		});
		
		
	}
	function setWeekendBtn (){
		var weekend = $('#class-calendar').fullCalendar('option', 'weekends');
		$('#class-calendar').fullCalendar('option', {
			weekends: !weekend,
		});
		// $('.fc-right .fc-button-group').append('<button type="button" class="fc-agendaWeekend-button fc-button fc-state-default">we</button>');
		// $('.fc-agendaWeekend-button').click(setWeekendBtn);
		
		if (!weekend) {
			$('.fc-agendaWeekend-button').addClass('fc-state-active');
		}
		else {
			$('.fc-agendaWeekend-button').removeClass('fc-state-active');
		}
	}

	$( window ).load(function() {

		// set event click on ajax delete button
		$('.immerse_lms_delete_media').click(deleteMediaAjax);
		
		// set event click on ajax load more button
		$('.immerse_lms_btn-load-more').click(loadVRMediaAjax);

		// set event click on filter button
		$('.vr_content_filter_type').click(loadVRMediaAjax);

		$('.spinner').hide();

		switch (php_vars.user_role) {
			case 'administrator':
				var classTextColor = '#FFFFFF';
				var classBgColor = '#012243';
				break;
			case 'trainer':
				var classTextColor = '#FFFFFF';
				var classBgColor = '#ff00ff';
				break;
			default:
				var classTextColor = '#FFFFFF';
				var classBgColor = '#F2B254';
				break;
		}

		var pastClassColor = '#FFFFFF';
		var pastClassBg = '#CCCCCC';

		// $('#legend-trainer').css ('background-color', trainerBg).css ('color', trainerColor);
		// $('#legend-student').css ('background-color', studentBg).css ('color', studentColor);
		// $('#legend-past-class').css ('background-color', pastClassBg).css ('color', pastClassColor);
		//console.log('user_role', php_vars.user_role);
		$('#class-calendar').fullCalendar({
			eventSources:[
				{
					url: ajaxurl,
					type: 'POST',
				 				  
					data: {
						action: 'immerse_lms_ajax_load_vr_classes',
						user_role: php_vars.user_role,
					},
					error: function() {
						// $('#class-calendar-overlay span').html('Error loading classes');
						// $('#class-calendar-overlay').css('visibility', 'visible');
						console.log('Error loading future classes');
					},
					success: function(data) {
						//alert('ajax success');
						//console.log(php_vars.user_role, data);
					},
					color: classBgColor,   // a non-ajax option
					textColor: classTextColor // a non-ajax option
				},
				{
					url: ajaxurl,
					type: 'POST',
										
					data: {
						action: 'immerse_lms_ajax_load_vr_classes',
						user_role: php_vars.user_role,
						past_classes: true
					},
					error: function() {
						// $('#class-calendar-overlay span').html('Error loading classes');
						// $('#class-calendar-overlay').css('visibility', 'visible');
						console.log('Error loading past classes');
					},
					success: function(data) {
						//alert('ajax success');
						//console.log(php_vars.user_role , data);
					},
					color: pastClassBg,   // a non-ajax option
					textColor: pastClassColor // a non-ajax option
				},
				
			],
				  
			timeFormat: 'HH:mm',
			eventRender: function(event, el) {
			// render the timezone offset below the event title
				if (event.start.hasZone()) {
					el.find('.fc-title').after(
					$('<div class="tzo"/>').text("GMT"+event.start.format('Z'))
					);
				}
			},
			weekends: false,
			fixedWeekCount: false,
			//defaultView: 'agendaWeek'
			//height: 'auto',
			eventLimit: 2,
			eventLimitClick: "popover",
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,agendaWeekend'
			  },
			timezone: php_vars.user_timezone,
			navLinks : true,
			height: 'auto',
			customButtons: {
				agendaWeekend: {
					text: 'we',
					click: setWeekendBtn
				}
			},
			nowIndicator: true,
			//cache: true,
			//editable: true,
			
		});

		//$('.fc-right .fc-button-group').append('<button type="button" class="fc-agendaWeekend-button fc-button fc-state-default">we</button>');
		//$('.fc-agendaWeekend-button').click(setWeekendBtn)
		
		// load the list of available timezones, build the <select> options
	  
		  // when the timezone selector changes, dynamically change the calendar option
		$('#timezone-selector').on('change', function() {
			console.log(this.value || false);
			$('#class-calendar').fullCalendar('option', 'timezone', this.value || false);
		});

		$('#list-view-switch').click(function(e){
			e.preventDefault();
			$('#list-view').show();
			$('#vr_calendar-view').hide();

		});

		$('#vr_calendar-view-switch').click(function(e){
			e.preventDefault();
			$('#vr_calendar-view').show();
			$('#list-view').hide();

		});
		var local_timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
		$('#local-timezone').val(local_timezone).html('Local (' + local_timezone + ')');
		
	});

	$(function() {
		$("input[name=startQuiz]").one("click", function() {
			/**
			 * Workaround for matrix sort answer questions. If no answers are present in the list (i.e the user
			 * has placed all of them), WP Pro Quiz will get an error as it tries to look for the parent node of
			 * one of the items there. So, this must be done in a lazy fashion.
			 */
			setTimeout(function() {
				if (window.LAST_QUIZ_ATTEMPT_DATA) {
					for (var qid in LAST_QUIZ_ATTEMPT_DATA) {
						var question = LAST_QUIZ_ATTEMPT_DATA[qid];
						var $qelem = $("[data-question_id=" + qid + "]");
	
						switch (question.type) {
							case "single":
								var selectedIndex = question.answer.indexOf(1);
	
								if (selectedIndex > -1) {
									var $selelem = $($qelem.find("input[type=radio]")[selectedIndex]);
									$selelem.prop("checked", true).change();
								}
								break;
							case "multiple":
								for (var i = 0; i < question.answer.length; i++) {
									if (question.answer[i]) {
										var $selelem = $($qelem.find("input[type=checkbox]")[i]);
										$selelem.prop("checked", true).change();
									}
								}
								break;
							case "matrix_sort_answer":
								for (var i = 0; i < question.answer.length; i++) {
									var item = $qelem.parent().find("[data-pos=" + i + "]");
									var slot = $qelem.find("[data-pos=" + question.answer[i] + "] .wpProQuiz_maxtrixSortCriterion");
	
									if (slot.length > 0) {
										item.remove().appendTo(slot);
									}
								}
								break;
							case "essay":
								var response = question.answer;
	
								switch (response.type) {
									case "text":
										$qelem.find("textarea").val(response.answer);
										break;
									case "upload":
										$qelem.find(".uploadEssayFile").val(response.answer);
										$qelem.append(
											$("<a />")
											.css({
												padding: 8,
												display: "inline-block"
											})
											.text("Click here to download your previous upload")
											.attr("href", response.answer)
											.attr("target", "_blank")
										);
										break;
								}
								break;
						}
	
						if (question.mark == "correct") {
							$qelem.before(
								$("<div />")
								.css({
									padding: 16,
									fontWeight: "bold",
									backgroundColor: "lightgreen",
									border: "4px solid green"
								})
								.text("Your previous answer to this question was marked correct.")
							);

							$qelem.hide();
						} else {
							$qelem.before(
								$("<div />")
								.css({
									padding: 16,
									fontWeight: "bold",
									backgroundColor: "pink",
									border: "4px solid red"
								})
								.text("Your previous answer to this question was marked incorrect.")
							);
						}
					}
				}
			}, 100);
		});

		$(".wpProQuiz_content").on("click", "[name=restartQuiz]", function(e) {
			window.location.reload();
			$(".wpProQuiz_content").remove();
			e.preventDefault();
			e.stopPropagation();
		});

		
		$(document).on('click', '.masquerade-link', function(ev) {
			var data = {
				action: 'immerse_lms_ajax_public_masquerade',
				uid: $(this).data('uid')
			};
			$.post(ajaxurl, data, function(response){
				if(response == '1'){
					window.location.reload();
				}
			});
		});

		$('#immerse-lms-masquerade-revert-link').click(function(ev){
			ev.preventDefault();
			var data = {
				action: 'immerse_lms_ajax_public_masquerade',
				reset: true
			};
			$.post(ajaxurl, data, function(response){
				if(response == '1'){
					location.reload();
				}
			});
		});

		ONLINE_TUTORS.map(function(user) {
			alertify.success(ONLINE_TUTORS_MESSAGE.replace(/\{name\}/, user));
		});

		// Online user indication is only valid for trainers, tutors, and administrator.
		if (ENABLE_STUDENT_ONLINE_NOTIFICATIONS) {
			var pageTitle = document.title;
			var pendingNotification = false;

			setInterval(function() {
				if (pendingNotification) {
					if (document.title == pageTitle) {
						document.title = "* " + pageTitle;
					} else {
						document.title = pageTitle;
					}
				} else {
					document.title = pageTitle;
				}
			}, 1000);

			$(window).mousemove(function() {
				pendingNotification = false;
			});

			function hookIflyChat() {
				if (window.iflychat) {
					var lastUsers = [];
					var lastSeen = {};

					if (sessionStorage.getItem("lastSeenUsers")) {
						var savedLastSeenUsers = JSON.parse(sessionStorage.getItem("lastSeenUsers"));

						// 10 minute expiry.
						if ((Date.now() - savedLastSeenUsers.saved) < 600000) {
							lastSeen = savedLastSeenUsers.lastSeen;
						}
					}

					setInterval(function() {
						lastUsers.forEach(function(user) {
							lastSeen[user] = Date.now();
						})
					}, 1000);

					iflychat.on("user-list-update", function(data) {
						var currentUsers = data.users.map(function(user) { return user.name; });

						currentUsers.forEach(function(user) {
							// User is online. Only notify if they haven't been seen for 10 minutes or at all.
							if (lastSeen[user] != -1 && (!lastSeen[user] || (Date.now() - lastSeen[user]) > 600000)) {
								// Get the user's details.
								$.ajax({
									method: "POST",
									url: ajaxurl,
									data: {
										action: "immerse_lms_ajax_get_user_notification_message",
										username: user
									},
									success: function(data) {
										if (data.success) {
											alertify.success(data.data, 30000);
											pendingNotification = true;
											var audio = new Audio("/wp-content/plugins/immerse-lms-wp-customizations/public/audio/notification.mp3");
											audio.play();
										}
									}
								});
							}
						});

						lastUsers = currentUsers;

						sessionStorage.setItem("lastSeenUsers", JSON.stringify({
							saved: Date.now(),
							lastSeen: lastSeen
						}));
					});
				} else {
					setTimeout(hookIflyChat, 100);
				}
			}

			hookIflyChat();
		}

		/**
		 * Perform a heartbeat to let the server know this user is online.
		 */
		if (USER_LOGGED_IN) {
			var completed_last_heartbeat = false;

			function immerse_lms_heartbeat() {
				$.ajax({
					method: "POST",
					url: ajaxurl,
					data: {
						action: "immerse_lms_ajax_heartbeat"
					},
					success: function(data) {
						completed_last_heartbeat = true;
					},
					error: function() {
						// If it fails we need to try again even before the 30 second timer is up.
						setTimeout(immerse_lms_heartbeat, 1000);
					}
				});
			}

			setInterval(function() {
				if (completed_last_heartbeat) {
					completed_last_heartbeat = false;
					immerse_lms_heartbeat();
				}
			}, 30000);

			immerse_lms_heartbeat();
		}
	});
})( jQuery );
