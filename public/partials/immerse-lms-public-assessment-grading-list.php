<?php
global $wpdb;
$wpdb->statisticref = $wpdb->prefix . 'wp_pro_quiz_statistic_ref';
$joins = "";

$joins .= "
LEFT JOIN
    $wpdb->postmeta statistic_ref_meta
    ON
        statistic_ref_meta.post_id = post.ID AND
        meta_key = '_statistic-ref-id'
LEFT JOIN
    $wpdb->statisticref statistic_ref
    ON
        statistic_ref.statistic_ref_id = statistic_ref_meta.meta_value
INNER JOIN
    (
        SELECT
            user_id,
            quiz_id,
            MAX(statistic_ref_id) AS last
        FROM
            $wpdb->statisticref
        GROUP BY
            user_id,
            quiz_id
    ) AS last_refs
    ON
        last_refs.last = statistic_ref_id
    ";

// Retrieve all the assessments.
if ($is_marker) {
    $joins .= "
INNER JOIN
    $wpdb->usermeta usermeta
    ON
        usermeta.meta_key LIKE 'marker_students_%' AND
        usermeta.meta_value = statistic_ref.user_id AND
        usermeta.user_id = " . wp_get_current_user()->ID . "
    ";
}

$sql = "
SELECT
    post.ID
FROM $wpdb->posts post
$joins
WHERE post_type = 'assessment_grading'";

if (isset($_GET['date_from']) && $_GET['date_from']) {
    $sql .= ' AND post.post_modified >= \'' . esc_sql($_GET['date_from']) . '\'';
}

if (isset($_GET['date_to']) && $_GET['date_to']) {
    $sql .= ' AND post.post_modified <= \'' . esc_sql($_GET['date_to']) . '\'';
}

$ids = $wpdb->get_col($sql);
$assessments = array();
$quizzes = array();

foreach ($ids as $id) {
    $assessment = get_assessment_grading($id);

    if (!$assessment) {
        continue;
    }

    if (
        (!isset($_GET['quizzes']) || in_array($assessment['quiz']->ID, $_GET['quizzes'])) &&
        (!isset($_GET['student']) || !$_GET['student'] || strpos(strtolower($assessment['student']->display_name), strtolower($_GET['student'])) > -1) &&
        (!isset($_GET['grade']) || !$_GET['grade'] || $assessment['grade'] == $_GET['grade'])
    ) {
        $assessments[] = $assessment;
    }

    if (!isset($quizzes[$assessment['quiz']->ID])) {
        $quizzes[$assessment['quiz']->ID] = $assessment['quiz'];
    }
}

function get_sort_by_link($params, $column, $direction) {
    $parameters = array_slice($params, 0);
    $parameters['sort_by'] = $column;
    $parameters['direction'] = $direction;
    return '?' . http_build_query($parameters);
}

$sort = isset($_GET['sort_by']) ? $_GET['sort_by'] : 'last_updated';
$sort_direction = (isset($_GET['direction']) && ($_GET['direction'] == 'asc' || $_GET['direction'] == 'desc')) ? $_GET['direction'] : 'desc';

function sort_by_link($sort, $sort_direction, $column) {
    global $_GET;

    if (isset($_GET['sort_by']) && $_GET['sort_by'] && $column == $sort) {
        if ($sort_direction == 'asc') {
            return get_sort_by_link($_GET, $column, 'desc');
        } else {
            $params = array_slice($_GET, 0);
            unset($params['sort_by']);
            unset($params['direction']);
            return '?' . http_build_query($params);
        }
    }

    return get_sort_by_link($_GET, $column, 'asc');
}

function sort_by_icon($sort, $sort_direction, $column) {
    if (!isset($_GET['sort_by']) || !$_GET['sort_by']) {
        return;
    }

    if ($column == $sort) {
        if ($sort_direction == 'asc') {
            ?><i class="fa fa-arrow-up"></i><?php
        } else {
            ?><i class="fa fa-arrow-down"></i><?php
        }
    }
}

// Perform sorting.
uasort($assessments, function($a, $b) use ($sort, $sort_direction) {
    $va = null;
    $vb = null;

    switch ($sort) {
        case 'quiz':
            $va = $a['quiz']->post_title;
            $vb = $b['quiz']->post_title;
            break;
        case 'student':
            $va = $a['student']->display_name;
            $vb = $b['student']->display_name;
            break;
        case 'last_updated':
            $va = $a['date'];
            $vb = $b['date'];
            break;
        case 'last_marked':
            $va = $a['last_graded_date'];
            $vb = $b['last_graded_date'];
            break;
        case 'marked_by':
            $va = $a['trainer_name'];
            $vb = $b['trainer_name'];
            break;
        case 'grade':
            $va = $a['grade'];
            $vb = $b['grade'];
            break;
        case 'status':
            $va = $a['status'];
            $vb = $b['status'];
            break;
    }

    if ($va == $vb) {
        return 0;
    }
    
    return $va > $vb ?
        ($sort_direction == 'asc' ? 1 : -1) :
        ($sort_direction == 'asc' ? -1 : 1);
});
?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<script>
    jQuery(function($) {
        $("input[name=date_from]").datepicker({ autoHide: true, format: 'yyyy-mm-dd' });
        $("input[name=date_to]").datepicker({ autoHide: true, format: 'yyyy-mm-dd' });

        $("#exportCsv").click(function(e) {
            var data = [];

            $("table#assessments").first().find("tbody").find("tr").each(function() {
                var $columns = $(this).find("td");
                data.push({
                    "quiz": $($columns[0]).text().trim(),
                    "student": $($columns[1]).text().trim(),
                    "last_updated": $($columns[2]).text().trim(),
                    "last_marked": $($columns[3]).text().trim(),
                    "status": $($columns[4]).text().trim(),
                    "grade": $($columns[5]).text().trim(),
                    "marked_by": $($columns[6]).text().trim()
                });
            });

            downloadCsv(['quiz', 'student', 'last_updated', 'last_marked', 'status', 'grade', 'marked_by'], data, "assessments");
            e.preventDefault();
            e.stopPropagation();
        });
    });
</script>

<script>
jQuery(function($) {

    $('.quiz-input').flexdatalist();

    $('.quiz-input').on('select:flexdatalist', function(event, set, options) {
        var the_value = set.label;
        $('.form-control-quizzes').children('option:contains(' + the_value + ')').prop("selected", true);
    });

    $('.quiz-input').on('after:flexdatalist.remove', function(event, set, options) {
        var inner_text = set[0]['innerText']
        var the_value = inner_text.slice(0,-1);
        $('.form-control-quizzes').children('option:contains(' + the_value + ')').prop("selected", false);
    });

    var assessment_quizzes_string = '';

    <?php if( isset( $_GET['quizzes'] ) ) : $param_quizzes = array_reverse($_GET['quizzes']); ?>

        <?php foreach($param_quizzes as $item) : ?>
            
            var each = $('#assessment-quizzes').find('option[value="' + <?php echo $item; ?> + '"]').html();
            assessment_quizzes_string += each + ',';
        
        <?php endforeach; ?>

    <?php endif; ?>

    $('.quiz-input').val(assessment_quizzes_string);

});
</script>

<form method="GET" action="" class="clearfix assessment-grading-list-search-form ilms-custom-form">
    <div class="ilms-flex-parent">
        <div class="ilms-flex-child ilms-flex-child-4">
            <div class="form-group">
                <label>Student</label>
                <input type="text" name="student" value="<?= isset($_GET['student']) ? esc_attr($_GET['student']) : '' ?>" class="form-control" autocomplete="off">
            </div>
        </div>
        <div class="ilms-flex-child ilms-flex-child-4">
            <div class="form-group">
                <label>Date From</label>
                <input type="text" name="date_from" value="<?= isset($_GET['date_from']) ? esc_attr($_GET['date_from']) : '' ?>" class="form-control" autocomplete="off">
            </div>
        </div>
        <div class="ilms-flex-child ilms-flex-child-4">
            <div class="form-group">
                <label>Date To</label>
                <input type="text" name="date_to" value="<?= isset($_GET['date_to']) ? esc_attr($_GET['date_to']) : '' ?>" class="form-control" autocomplete="off">
            </div>
        </div>
    </div>
    <div class="ilms-form-section">
        <div class="form-group">
            <label>Quizzes</label>
            <select name="quizzes[]" class="form-control form-control-quizzes" multiple>
                <?php foreach ($quizzes as $id => $quiz) { ?>
                    <option value="<?= $id ?>" <?= (isset($_GET['quizzes']) && in_array($id, $_GET['quizzes'])) ? 'selected' : '' ?>><?= $quiz->post_title ?></option>
                <?php } ?>
            </select>

            <input type="text" class="quiz-input" list='assessment-quizzes' data-min-length='1' multiple='multiple' data-search-by-word='true'>

            <datalist id="assessment-quizzes">
                <?php foreach ($quizzes as $id => $quiz) { ?>
                    <option value="<?= $id ?>"><?= $quiz->post_title ?></option>
                <?php } ?>
            </datalist>

        </div>
    </div>
    <div class="ilms-form-section ilms-form-section-grade">
        <div class="form-group">
            <label>Grade</label>
            <select name="grade" class="form-control">
                <option value=""></option>
                <option value="ungraded" <?= (isset($_GET['grade']) && $_GET['grade'] == 'ungraded') ? 'selected' : '' ?>>Ungraded</option>
                <option value="competent" <?= (isset($_GET['grade']) && $_GET['grade'] == 'competent') ? 'selected' : '' ?>>Competent</option>
                <option value="notcompetent" <?= (isset($_GET['grade']) && $_GET['grade'] == 'notcompetent') ? 'selected' : '' ?>>Not Yet Competent</option>
            </select>
        </div>
    </div>
    <div class="ilms-form-section ilms-form-section-buttons">
        <button type="submit">
            <i class="fa fa-search"></i> Search
        </button>    
        <a class="button" href="?">Clear Filters</a>
        <a href="#" class="button" id="exportCsv">Export to CSV</a>
    </div>
</form>

<table id="assessments" class="assessment-grading-list-search-results">
    <thead>
        <tr>
            <th>
                <a href="<?= sort_by_link($sort, $sort_direction, 'quiz') ?>">Quiz <?php sort_by_icon($sort, $sort_direction, 'quiz') ?></a>
            </th>
            <th>
                <a href="<?= sort_by_link($sort, $sort_direction, 'student') ?>">Student <?php sort_by_icon($sort, $sort_direction, 'student') ?></a>
            </th>
            <th>
                <a href="<?= sort_by_link($sort, $sort_direction, 'last_updated') ?>">Date Last Updated <?php sort_by_icon($sort, $sort_direction, 'last_updated') ?></a>
            </th>
            <th>
                <a href="<?= sort_by_link($sort, $sort_direction, 'last_marked') ?>">Date Last Marked <?php sort_by_icon($sort, $sort_direction, 'last_marked') ?></a>
            </th>
            <th>
                <a href="<?= sort_by_link($sort, $sort_direction, 'status') ?>">Status <?php sort_by_icon($sort, $sort_direction, 'status') ?></a>
            </th>
            <th>
                <a href="<?= sort_by_link($sort, $sort_direction, 'grade') ?>">Grade <?php sort_by_icon($sort, $sort_direction, 'grade') ?></a>
            </th>
            <th>
                <a href="<?= sort_by_link($sort, $sort_direction, 'marked_by') ?>">Marked By <?php sort_by_icon($sort, $sort_direction, 'marked_by') ?></a>
            </th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($assessments as $assessment) {
            ?>
            <tr>
                <td><a href="/assessment-grading-sheet/?gid=<?= $assessment['grading']->ID ?>"><?= $assessment['quiz']->post_title ?></a></td>
                <td><?= $assessment['student'] ? $assessment['student']->display_name : '' ?></td>
                <td><?= $assessment['date'] ?></td>
                <td><?= $assessment['last_graded_date'] ?></td>
                <td><?= $assessment['status'] ?></td>
                <td>
                    <?= $assessment['grade'] == 'ungraded' ? 'Ungraded' : '' ?>
                    <?= $assessment['grade'] == 'notcompetent' ? 'Not Yet Competent' : '' ?>
                    <?= $assessment['grade'] == 'competent' ? 'Competent' : '' ?>
                </td>
                <td><?= $assessment['trainer_name'] ?></td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<style>
.form-control-quizzes {
    display: none !important;
}
.flexdatalist-multiple {
    margin-top: 5px !important;
    padding: 5px 5px;
    box-shadow: inset 0 1px 1px 1px rgba(0,0,0,.05);
    outline: none !important;
    box-sizing: border-box;
}
.flexdatalist-multiple li {
    margin: 5px;
}
.flexdatalist-multiple li.input-container input {
    background: none;
    box-shadow: none;
    margin-left: -8px;
}
.flexdatalist-results {
    border: none;
    border-radius: 5px;
    box-shadow: none;
    margin-top: 5px;
}
.flexdatalist-results li {
    border-bottom: 1px solid #e3e9f0;
    padding: 7px 13px;
}
.flexdatalist-results li.active {
    background: #c11f26;
}
</style>