<?php
if (!$assessment_grading_id) {
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if ($_POST['type'] == 'update') {
        if (isset($_FILES['trainer_signature'])) {
            $_POST['trainer_signature'] = $_FILES['trainer_signature'];
        }

        $assessment = update_assessment_grading($assessment_grading_id, $_POST);
    } else {
        $assessment = get_assessment_grading($assessment_grading_id);
    }

    if ($_POST['type'] == 'lock') {
        update_user_meta($assessment['student']->ID, '_quiz-locked-' . apply_filters('immerse_lms_get_quiz_pro_id', $assessment['quiz']->ID), wp_get_current_user()->ID);
    }

    if ($_POST['type'] == 'unlock') {
        delete_user_meta($assessment['student']->ID, '_quiz-locked-' . apply_filters('immerse_lms_get_quiz_pro_id', $assessment['quiz']->ID));
    }
} else {
    $assessment = get_assessment_grading($assessment_grading_id);
}

function is_disabled($assessment) {
    if (!$assessment['is_latest']) {
        ?>disabled title="You can only grade the latest attempt for a quiz."<?php
    }
}

// Retrieve previous assessments and their notes.
global $wpdb;
$previous_notes = array();

$previous_assessment_notes = $wpdb->get_results("
SELECT
    posts.ID AS 'grading_id',
    posts.post_content AS 'notes'
FROM
    $wpdb->posts posts
INNER JOIN
    $wpdb->prefix"."wp_pro_quiz_statistic_ref assessment_sref
    ON
        assessment_sref.statistic_ref_id = '" . $assessment['statistic_ref_id'] . "'
INNER JOIN
    $wpdb->postmeta statistic_ref_meta
    ON
        statistic_ref_meta.post_id = posts.ID AND
        statistic_ref_meta.meta_key = '_statistic-ref-id'
INNER JOIN
    $wpdb->prefix"."wp_pro_quiz_statistic_ref post_sref
    ON
        post_sref.statistic_ref_id = statistic_ref_meta.meta_value
WHERE
    posts.ID <> '" . $assessment['grading']->ID . "' AND
    posts.post_type = 'assessment_grading' AND
    post_sref.user_id = assessment_sref.user_id AND
    post_sref.quiz_id = assessment_sref.quiz_id AND
    posts.post_content <> ''
");

?>
<?php if (!$assessment['is_latest']) : ?>
<div style="padding: 10px; border: 1px solid red; background-color: pink; color: red; text-align: center;">
    You can only grade the latest attempt for a quiz.
</div>
<?php endif ?>
<form action="" method="POST">
    <?php
    $quiz_locked = apply_filters('immerse_lms_quiz_locked', $assessment['quiz'], $assessment['student']);
    ?>
    Current quiz lock status: <b><?= $quiz_locked ? ($quiz_locked . ' (locked)') : 'This quiz is open.' ?></b><br />
    <?php if (!$quiz_locked) : ?>
        <input type="hidden" name="type" value="lock" />
        <button class="button" type="submit">Lock Quiz</button>
    <?php else : ?>
        <?php if (strpos($quiz_locked, 'This quiz has been temporarily locked') === 0) : ?>
            <input type="hidden" name="type" value="unlock" />
            <button class="button" type="submit">Unlock Quiz</button>
        <?php endif ?>
    <?php endif ?>
</form>
<form action="" method="POST" enctype="multipart/form-data" class="assessment-grading-sheet-form">
    
    <input type="hidden" name="type" value="update" />
    
    <div class="ilms-flex-parent">
        <div class="ilms-flex-child ilms-flex-child-2">
            <h3><?= $assessment['student']->display_name ?></h3>
        </div>
        <div class="ilms-flex-child ilms-flex-child-2">
            <h5><?= $assessment['date'] ?></h5>
        </div>
    </div>
    
    <div class="ilms-flex-parent">
        <div class="ilms-flex-child ilms-flex-child-2">
            <h3><?= $assessment['course']->post_title ?></h3>
        </div>
        <div class="ilms-flex-child ilms-flex-child-2">
            <h3><?= $assessment['quiz']->post_title ?></h3>
        </div>
    </div>

    <div class="ilms-flex-parent">
        <div class="ilms-flex-child ilms-flex-child-2">
            <h3>
                <b>Status: <?php
            switch ($assessment['status']) {
                case 'unread':
                    echo 'Unread';
                    break;
                case 'read':
                    echo 'Read';
                    break;
                case 'graded':
                    echo 'Graded';
                    break;
            }
            ?></b>
            </h3>
        </div>
        <div class="ilms-flex-child ilms-flex-child-2">
            <h3>
                <b>Grade: <?php
            switch ($assessment['grade']) {
                case 'ungraded':
                    echo 'Ungraded';
                    break;
                case 'competent':
                    echo 'Competent';
                    break;
                case 'notcompetent':
                    echo 'Not Yet Competent';
                    break;
            }
            ?></b>
            </h3>
        </div>
    </div>

    <ul class="assessment-grading-sheet-list">
    <?php foreach ($assessment['answers'] as $i => $answer) : ?>

        <li>

            <div class="assessment-grading-sheet-title"><?= $answer['question']->getTitle(); ?></div>
            <div class="assessment-grading-sheet-question"><?= $answer['question']->getQuestion(); ?></div>
            
            <?php if($answer['question']->getAnswerType() == 'essay') : ?>
                <div class="assessment-grading-sheet-student-answer">Student Answer:</div>
            <?php endif; ?>

            <div class="assessment-grading-sheet-answer<?php if($answer['question']->getAnswerType() == 'multiple' || $answer['question']->getAnswerType() == 'single') { ?> assessment-grading-sheet-answer-adjust<?php } ?>"><?php assessment_render_answer($answer) ?></div>
            <div class="assessment-grading-sheet-grade">
                <?php if ($answer['mark'] == 'none' || $answer['question']->getAnswerType() == 'essay') : ?>
                    <input type="radio" id="question_<?= $i ?>_mark_correct" name="answers[<?= $i ?>][mark]" for="question_<?= $i ?>_mark_correct" value="correct" <?= $answer['mark'] == 'correct' ? 'checked' : '' ?> <?php is_disabled($assessment) ?> />
                    <label for="question_<?= $i ?>_mark_correct">Correct</label><br />
                    <input type="radio" id="question_<?= $i ?>_mark_incorrect" name="answers[<?= $i ?>][mark]" for="question_<?= $i ?>_mark_incorrect" value="incorrect" <?= $answer['mark'] == 'incorrect' ? 'checked' : '' ?> <?php is_disabled($assessment) ?> />
                    <label for="question_<?= $i ?>_mark_incorrect">Incorrect</label>
                <?php else : ?>
                    <?= $answer['mark'] == 'correct' ? '<div class="assessment-grading-sheet-grade-main assessment-grading-sheet-grade-main-correct"><i class="fa fa-check"></i><span>Correct</span></div>' : '<div class="assessment-grading-sheet-grade-main assessment-grading-sheet-grade-main-incorrect"><i class="fa fa-times"></i><span>Incorrect</span></div>' ?>
                <?php endif ?>
            </div>
            <div class="assessment-grading-sheet-last-graded">Last Graded: <?= isset($answer['last_graded']) ? $answer['last_graded'] : '' ?></div>

        </li>

    <?php endforeach ?>

    </ul>

    <div>
        <p>
            <label for="trainer_name">Trainer's Name</label><br />
            <input type="text" name="trainer_name" for="trainer_name" value="<?= htmlentities($assessment['trainer_name'] ? $assessment['trainer_name'] : wp_get_current_user()->display_name) ?>" <?php is_disabled($assessment) ?> />
        </p>
        <p>
            <label for="trainer_signature">Trainer's Signature</label><br />
            <input type="file" name="trainer_signature" for="trainer_signature" <?php is_disabled($assessment) ?> />
            <?php if ($assessment['trainer_signature']) : ?>
                <br />
                <img src="<?= $assessment['trainer_signature'] ?>" width="200" />
            <?php endif ?>
        </p>
        <label for="trainer_notes">Trainer's Notes</label><br />
        <?php
        if ($assessment['is_latest']) {
            wp_editor($assessment['trainer_notes'], 'trainer_notes');
        } else {
            ?>
            <div title="You can only grade the latest attempt for a quiz.">
                <?= $assessment['trainer_notes'] ?>
            </div>
            <?php
        }
        ?>
        <br />
        <label for="trainer_previous_notes">Previous Trainer Notes</label>
        <?php foreach ($previous_assessment_notes as $note) : ?>
            <div style="background-color: white; padding: 16px; margin-top: 8px;">
                <?php echo $note->notes; ?>
                <div>
                    <a href="/assessment-grading-sheet/?gid=<?= $note->grading_id ?>">View Sheet</a>
                </div>
            </div>
        <?php endforeach ?>
    </div>
    <br />
    <div style="float: left; width: 100%;">
        <button type="submit" style="float: right; width: 150px !important;" class="button" <?php is_disabled($assessment) ?>>
            Submit Grading
        </button>
    </div>
</form>