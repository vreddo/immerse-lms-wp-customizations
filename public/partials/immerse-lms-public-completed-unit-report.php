<style>
    td.detail {
        padding-left: 20px;
        font-size: 14px !important;
    }
</style>
<table>
    <?php
    foreach ($structure as $cid => $courseLessons) {
        $course = $courses[$cid];
    ?>
    <tr>
        <th><a href="<?= get_the_permalink($course) ?>"><?= $course->post_title ?></a></th>
    </tr>
    <?php
        foreach ($courseLessons as $lid => $quizzes) {
            $lesson = $lessons[$lid];

            ?>
    <tr>
        <td><a href="<?= get_the_permalink($lesson) ?>"><?= $lesson->post_title ?></a></td>
    </tr>
            <?php
            foreach ($quizzes as $quizAttempt) {
                $quiz = $quizMapper->fetch($quizAttempt->getQuizId());
                
                ?>
    <tr>
        <td class="detail"><a href="<?= get_the_permalink(learndash_get_quiz_id_by_pro_quiz_id($quiz->getId())) ?>"><?= $quiz->getName() ?></a></td>
    </tr>
                <?php
            }
        }
    }
    ?>
</table>