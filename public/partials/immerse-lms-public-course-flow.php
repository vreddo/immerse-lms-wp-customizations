<?php
foreach ($userCourses as $courseId) {
    $course = get_post($courseId);
    $hierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $course->ID, $user->ID);
    $originalHierarchy = apply_filters('immerse_lms_get_course_steps_hierarchy', $course->ID);
    ?>
    <form method="POST" data-course-id="<?= $courseId ?>">
        <h2><?= $course->post_title ?></h2>
        <a href="javascript:;" onclick="$(this).parent().find('input[type=checkbox]').prop('checked', true);">Select All</a> /
        <a href="javascript:;" onclick="$(this).parent().find('input[type=checkbox]').prop('checked', false);">Deselect All</a>
        <?php
        foreach ($originalHierarchy as $lesson) {
            $selected = count(array_filter($hierarchy, function($l) use ($lesson) { return $l['lesson']->ID == $lesson['lesson']->ID; }));

        ?>
            <div data-lesson-id="<?= $lesson['lesson']->ID ?>">
                <input type="checkbox" <?php if($selected): ?>checked<?php endif ?> /> <?= $lesson['lesson']->post_title; ?>
                <input type="hidden" value="<?= esc_attr(json_encode(array_map(function($child) { return $child->ID; }, $lesson['children']))) ?>" />
            </div>
        <?php
        }
        ?>
        <button type="button" class="button"
        data-ajax-click="saveCourseFlow"
        data-ajax-arg-course-id="<?= $course->ID ?>"
        data-ajax-arg-user-id="<?= $user->ID ?>"
        data-ajax-arg-course-flow="js:getHierarchyData(<?= $course->ID ?>)"
        data-ajax-success="alert('Saved.')"
        >Save</button>
    </form>
<?php
}
?>