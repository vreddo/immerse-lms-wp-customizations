<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.js"></script>
<?php if (count($filters) > 0) : ?>
<h4>Filters</h4>
<form action="?">
    <div class="clearfix">
        <?php foreach ($filters as $filter) : ?>
        <div style="width: 33%; float: left; box-sizing: border-box; padding: 8px;">
            <b><?= $filter['label'] ?></b><br />
            <?= $grid->render_filter($filter) ?>
        </div>
        <?php endforeach ?>
    </div>
    <input type="submit" value="Filter" class="button" />
    <a href="?" class="button">Clear Filters</a>
</form>
<?php endif ?>
<h4>Data</h4>
<table>
    <thead>
        <tr>
            <?php foreach ($columns as $column) : ?>
            <th style="<?= isset($column['width']) ? ('width: ' . $column['width'] . ';') : '' ?>"><?= $column['label'] ?></th>
            <?php endforeach ?>
        </tr>
    </thead>
    <tbody>
        <?php if (!$rows || count($rows) == 0) : ?>
        <tr>
            <td colspan="<?= count($columns) ?>">
                Nothing found.
            </td>
        </tr>
        <?php else : ?>
        <?php foreach ($rows as $row) : ?>
        <tr>
            <?php foreach ($columns as $column) : ?>
            <td style="vertical-align: middle;"><?php $grid->render_cell($column, $row) ?></td>
            <?php endforeach ?>
        </tr>
        <?php endforeach ?>
        <?php endif ?>
    </tbody>
</table>
<div>
    Page:
    <?php for ($i = 0; $i < min($pages, 50); $i++):
    
    $new_query = array_merge(array(), $_GET);
    $new_query['filter_page'] = $i + 1;

    if ($page == $i + 1):
    ?>
    <b><?= $i + 1 ?></b>
    <?php else: ?>
    <a href="?<?= http_build_query($new_query) ?>"><?= $i + 1 ?></a>
    <?php endif ?>
    <?php endfor ?>

    <?php if ($pages > 50) : ?>
    ...<br />
    <small>There are more pages for this query that aren't displayed.</small>
    <?php endif ?>
</div>