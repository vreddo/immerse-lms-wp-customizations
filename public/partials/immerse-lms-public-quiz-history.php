<?php
function decode_duration($duration) {
    preg_match("/(?:(\d+)(m|h|s) ?)?(?:(\d+)(m|h|s) ?)?(?:(\d+)(m|h|s) ?)/", $duration, $matches);
    $decoded = 0;

    if ($matches[1] && $matches[2]) {
        $value = $matches[1];
        $multiplier = 1;

        switch ($matches[2]) {
            case 'h':
                $multiplier = 60 * 60;
                break;
            case 'm':
                $multiplier = 60;
                break;
        }

        $decoded += $value * $multiplier;
    }

    if ($matches[3] && $matches[4]) {
        $value = $matches[3];
        $multiplier = 1;

        switch ($matches[4]) {
            case 'h':
                $multiplier = 60 * 60;
                break;
            case 'm':
                $multiplier = 60;
                break;
        }

        $decoded += $value * $multiplier;
    }

    if ($matches[5] && $matches[6]) {
        $value = $matches[5];
        $multiplier = 1;

        switch ($matches[6]) {
            case 'h':
                $multiplier = 60 * 60;
                break;
            case 'm':
                $multiplier = 60;
                break;
        }

        $decoded += $value * $multiplier;
    }

    return $decoded;
}

function render_question($question, $answerData = null) {
    if ($answerData) {
        $userAnswers = json_decode($answerData);
    } else {
        $userAnswers = [];
    }

    ?>

    <div class="assessment-grading-sheet-title"><?= $question->getTitle(); ?></div>
    <div class="assessment-grading-sheet-question"><?= $question->getQuestion(); ?></div>

    <div class="assessment-grading-sheet-answer">

    <?php
    switch ($question->getAnswerType()) {
        case 'multiple':
        case 'single':
            ?>

            <?php foreach ($question->getAnswerData() as $i => $answer): ?>

                <div class="options" <?php if($answer->isCorrect()): ?>style="background-color: #abffab;"<?php endif; ?>>
                    <div class="option-user-answer <?php if($userAnswers[$i]): ?>option-user-answered<?php endif; ?>"></div>
                    <div class="option-choices"><?= $answer->getAnswer() ?></div>
                </div>

            <?php endforeach; ?>

            <?php
            break;
        case 'matrix_sort_answer':
            ?>
            <div class="matrix_sort_answer_section matrix_sort_answer_section_correct">
                <div class="matrix_sort_answer_section_header">Correct Answer:</div>
                <table class="matrix_sort_answer">
                    <tbody>
                        <?php
                        foreach ($question->getAnswerData() as $answer) {
                            ?>
                            <tr>
                                <td style="border-right: 1px solid rgba(0, 0, 0, 0.11);"><?= $answer->getAnswer() ?></td>
                                <td style="width: 70%;"><?= $answer->getSortString() ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <?php
            break;
        case 'essay': ?>

            <div class="essay_answer_section_header">Student Answer:</div>

            <?php

            $answer_id = $userAnswers->graded_id;

            $answer = get_post_field('post_content', $answer_id);

            if ($answer != 'See upload below.') {

                echo $answer;

            } else {

                $uploaded_file = get_post_meta( $answer_id, 'upload', true );

                ?>

                <a href="<?= $uploaded_file; ?>">Download answer</a>
                
                <?php
            }
            break;
    }
}

$stats = apply_filters('immerse_lms_get_quiz_data', $user->ID);
$statsGrouped = [];

foreach ($stats as $stat) {
    if (!isset($statsGrouped[$stat['quiz_post_id']])) {
        $statsGrouped[$stat['quiz_post_id']] = [];
    }

    $stat['time_spent'] = decode_duration($stat['time_spent']);
    $statsGrouped[$stat['quiz_post_id']][] = $stat;
}

$stats = $statsGrouped;

$statMapper = new ImmerseLms_Model_StatisticRefMapper();
$statsMapper = new WpProQuiz_Model_StatisticMapper();
$quizAttempts = $statMapper->getUserQuizAttempts($user->ID);
$quizMapper = new WpProQuiz_Model_QuizMapper();

if (count($quizAttempts)) {
    $lastQuiz = get_post(learndash_get_quiz_id_by_pro_quiz_id($quizAttempts[0]->getQuizId()));
    $questionMapper = new WpProQuiz_Model_QuestionMapper();
    $lastQuizStatistics = $statsMapper->fetchAllByRef($quizAttempts[0]->getStatisticRefId());
    $lastStat = $stats[learndash_get_quiz_id_by_pro_quiz_id($quizAttempts[0]->getQuizId())][0];
}
?>

<div class="quiz-history-content">

<?php if (count($quizAttempts)): ?>

<div class="quiz-history-latest-quiz">

<h2>Latest Quiz</h2>

<div class="ilms-flex-parent">
    <div class="ilms-flex-child ilms-flex-child-2">
        <h3>Title: <a href="<?= get_the_permalink($lastQuiz->ID); ?>"><?= $lastQuiz->post_title; ?></a></h3>
    </div>
    <div class="ilms-flex-child ilms-flex-child-2">
        <h5>Score: <?= $lastStat['score'] ?>/<?= $lastStat['total'] ?></h5>
    </div>
</div>

<div class="ilms-flex-parent">
    <div class="ilms-flex-child ilms-flex-child-2">
        <h5>Incorrect Items:</h5>
    </div>
    <div class="ilms-flex-child ilms-flex-child-2"></div>
</div>

<ul class="assessment-grading-sheet-list">
<?php

foreach ($lastQuizStatistics as $stat) {
    
    $question = $questionMapper->fetchById($stat->getQuestionId(), false);
    
    if ($question &&
            (
                isset($lastStat['graded']) && isset($lastStat['graded'][$stat->getQuestionId()]) ?
                $lastStat['graded'][$stat->getQuestionId()]['points_awarded'] == 0
                :
                $stat->getIncorrectCount() > 0
            )
        ) { ?>
    
    <li><?php render_question($question, $stat->getAnswerData()); ?></li>

    <?php

    }

}

?>
</ul>

</div>

<?php endif ?>

<button class="button"
    data-ajax-click="exportQuizData"
    data-ajax-arg-user-id="<?= $user->ID ?>"
    data-ajax-success="downloadCsv(['user_id', 'name', 'email', 'quiz_id', 'quiz_title', 'score', 'total', 'date', 'percentage', 'time_spent', 'passed', 'course_id', 'course_title'], data, <?= htmlentities(json_encode($user->user_login)) ?>)"
    >Export CSV</button>

<style>
    .attempts th, .attempts td {
        text-align: center;
    }

    .attempts td:first-child {
        text-align: left;
    }

    .attempts td.detail {
        padding-left: 20px;
    }
</style>
<h2>Previous Quizzes &amp; Attempts</h2>

<div class="attempts-wrapper">
    <table class="attempts">
        <thead>
            <tr>
                <th>Quiz Title</th>
                <th>Score</th>
                <th>Grade</th>
                <th>Number of Attempts</th>
                <th>Time Spent</th>
                <th>Last Attempt Date</th>
                <th>Total Time Spent</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php
        foreach ($stats as $id => $attempts) {
            $totalTimeSpent = 0;
            $totalAttempts = 0;

            foreach ($attempts as $i => $stat) {
                $totalTimeSpent += $stat['time_spent'];
                $totalAttempts++;
            }

            foreach ($attempts as $i => $stat) {
                if ($i == 0) {
                    ?>
                    <tr data-row-type="header" data-row-id="<?= $stat['quiz_id'] ?>">
                        <td>
                            <a href="<?= get_the_permalink($stat['quiz_post_id']); ?>"><?= $stat['quiz_title'] ?></a><br />
                            <a class="view-buttons view-incorrect-items" href="javascript:;" onclick="
                                    jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).parent().parent().data('row-id') + '][data-row-detail-type!=incorrect]').hide();
                                    jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).parent().parent().data('row-id') + '][data-row-detail-type=incorrect]').toggle();
                                    ">View Incorrect Items</a>
                            <a class="view-buttons view-previous-attempts" href="javascript:;" onclick="
                                    jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).parent().parent().data('row-id') + '][data-row-detail-type!=history]').hide();
                                    jQuery('tr[data-row-type=detail][data-row-id=' + jQuery(this).parent().parent().data('row-id') + '][data-row-detail-type=history]').toggle();
                                ">View Previous Attempts</a>
                        </td>
                        <td><span class="points"><?= $stat['score'] ?></span>/<?= ($stat['total'] ? $stat['total'] : '0') ?></td>
                        <td><span class="percentage"><?= $stat['percentage'] ?></span>%</td>
                        <td><?= $totalAttempts ?></td>
                        <td><?= apply_filters('immerse_lms_format_duration', $stat['time_spent']) ?></td>
                        <td><?= $stat['date'] ?></td>
                        <td><?= apply_filters('immerse_lms_format_duration', $totalTimeSpent) ?></td>
                        <td>
                            <?php if ($stat['percentage'] != 100): ?>
                            <button class="button" type="button"
                                data-ajax-click="overrideQuizScore"
                                data-ajax-success="$this.parent().parent().find('.percentage').text('100'); $this.remove();"
                                data-ajax-arg-quiz-id="<?= $stat['quiz_id']; ?>"
                                data-ajax-arg-user-id="<?= $user->ID; ?>">Mark 100%</button>
                            <?php endif ?>
                        </td>
                    </tr>
                    <?php
                } else {
                    ?>
                    <tr data-row-type="detail" data-row-id="<?= $stat['quiz_id'] ?>" style="display: none;" data-row-detail-type="history">
                        <td class="detail"><?= $stat['quiz_title']; ?></td>
                        <td><?= $stat['score'] ?>/<?= ($stat['total'] ? $stat['total'] : '0') ?></td>
                        <td><?= $stat['percentage'] ?>%</td>
                        <td><?= $totalAttempts ?></td>
                        <td><?= apply_filters('immerse_lms_format_duration', $stat['time_spent']) ?></td>
                        <td><?= $stat['date'] ?></td>
                        <td><?= apply_filters('immerse_lms_format_duration', $totalTimeSpent) ?></td>
                        <td></td>
                    </tr>
                    <?php
                }

                $totalTimeSpent -= $stat['time_spent'];
                $totalAttempts--;
            }

            $questions = $statsMapper->fetchAllByRef($attempts[0]['statistic_ref_id']);
           
            foreach ($questions as $questionStat) {
                $question = $questionMapper->fetchById($questionStat->getQuestionId(), false);
                
                if ($question &&
                    isset($attempts[0]['graded']) && isset($attempts[0]['graded'][$questionStat->getQuestionId()]) ?
                    $attempts[0]['graded'][$questionStat->getQuestionId()]['points_awarded'] == 0
                    :
                    $questionStat->getIncorrectCount() > 0
                    ) {
                    ?>
                    <tr data-row-type="detail" data-row-id="<?= $attempts[0]['quiz_id'] ?>" style="display: none;" data-row-detail-type="incorrect">
                        <td colspan="8">
                            <?php render_question($question, $questionStat->getAnswerData()); ?>
                        </td>
                    </tr>
                    <?php
                }
            }
        }
        ?>
        </tbody>
    </table>
</div>

</div>