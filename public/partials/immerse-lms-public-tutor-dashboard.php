<?php

echo do_shortcode('[vc_wp_search title="Search Student"]');

?>

<div class="tutor-dashboard-accordion">

    <div class="tutor-dashboard-accordion-item">
        <h4 class="beefup__head">LMS Overview</h4>
        <div class="beefup__body">
            <?php echo do_shortcode('[ld_propanel widget="overview"]'); ?>
        </div>
    </div>

    <div class="tutor-dashboard-accordion-item">
        <h4 class="beefup__head">Assignments and Submitted Essays</h4>
        <div class="beefup__body">
            <ul class="tutor-dashboard-list">
                <li>
                    <a href="<?php echo site_url(); ?>/wp-admin/edit.php?post_type=sfwd-assignment">View all submitted assignments</a>
                </li>
                <li>
                    <a href="<?php echo site_url(); ?>/submitted-quizzes/">View all Submitted Essays (Grading Sheets)</a>
                </li>
            </ul>
        </div>
    </div>

    <div class="tutor-dashboard-accordion-item">
        <h4 class="beefup__head">Quiz Statistics</h4>
        <div class="beefup__body">
            <?php echo do_shortcode('[table id=QuizStat /]'); ?>
        </div>
    </div>

    <div class="tutor-dashboard-accordion-item">
        <h4 class="beefup__head">Filter Report</h4>
        <div class="beefup__body">
            <?php echo do_shortcode('[ld_propanel widget="filtering"]'); ?>
            <?php echo do_shortcode('[ld_propanel widget="reporting"]'); ?>
        </div>
    </div>

    <div class="tutor-dashboard-accordion-item">
        <h4 class="beefup__head">User Activity</h4>
        <div class="beefup__body">
            <?php echo do_shortcode('[ld_propanel widget="activity"]'); ?>
        </div>
    </div>

    <div class="tutor-dashboard-accordion-item">
        <h4 class="beefup__head">Support Request</h4>
        <div class="beefup__body">
            <?php echo do_shortcode('[gravityform id="6" title="true" description="false" ajax="true"]'); ?>
        </div>
    </div>

    <div class="tutor-dashboard-accordion-item">
        <h4 class="beefup__head">Student Comments and Enquiries</h4>
        <div class="beefup__body">
            <ul class="tutor-dashboard-list">
                <li>
                    <a href="<?php echo site_url(); ?>/wp-admin/edit-comments.php">Comments</a>
                </li>
                <li>
                    <a href="<?php echo site_url(); ?>/wp-admin/admin.php?page=gf_entries&amp;id=4">Enquiries</a>
                </li>
            </ul>
        </div>
    </div>

</div>

<script>
(function($) {

    // tutor dashboard accordion
    var $beefup = $('.tutor-dashboard-accordion-item').beefup({
        openSingle: true
    });

})( jQuery );
</script>