<div class="vr_content_list_item" id="immerse-lms-class-id-<?= $vr_user_class['ID'] ?>">

	<div class='content-col'>
	<h4><?= $vr_user_class['class_public_title'] ?></h4>			
		
	</div>

	<div class='content-col'>
		
		Date: <?= $vr_user_class['vr_class_datetime_start'] ?>	<?= $vr_user_class['class_vr_repeat_next_date'] ?>
		<br/>
		Trainer: <em><?= $vr_user_class['class_trainer']['display_name'] ?></em>	
		<br/>
		Course: <?= $vr_user_class['class_course'][0]->post_title; ?>	
		
		
	
		<?php if ($vr_user_class['class_vr_content_count'] > 0) : ?>
			<br/>
			<?php
				for ($i=0; $i < $vr_user_class['class_vr_content_count'] ; $i++) { 
					$vr_content_id = get_post_meta($vr_user_class['ID'], 'vr_content_'.$i.'_vr_content_file', ARRAY_A);
					echo wp_get_attachment_image( $vr_content_id, array(30,30), '', array( "title" => basename(get_attached_file($vr_content_id))));
				}
			?>

		<?php endif;?>
	</div>
</div>