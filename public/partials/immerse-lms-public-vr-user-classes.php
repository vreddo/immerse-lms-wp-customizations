<div id="">
	<div id="vr_calendar-view"> 
		<div class="learndash_profile_heading no_radius clear_both">
			<span class="title">
				VR Classes Calendar
			</span>
			
			<div class="actions alignright">
				<a href='#' id="list-view-switch"> <i href='#' class="fa fa-list "></i></a> 
			</div>
			
			<div class="clearboth"></div>
		</div>

		
		<div class="vr_user_content_wrapper clear_both">
			
			<div class='alignleft'>
			</div>

			<div id='legend' class='alignright'>
				<!-- Legend:
				<span id="legend-trainer">Trainer</span>
				<span id="legend-student">Student</span>
				<span id="legend-past-class">Past Class</span> -->
			</div>

			<div id='class-calendar-wrapper'>
				<div id='class-calendar'></div>
			</div>
		
			<select id='timezone-selector' class=''>
					<option value=''>Select timezone</option>
					<option id="local-timezone" value='local'>local</option>
					<option value='UTC'>UTC</option>
					<?php 
						/*
						*  Create a select dropdown with all available timezones
						*/
						$utc = new DateTimeZone('UTC');
						$dt = new DateTime('now', $utc);
						// $fieldValue = trim($field['value']);
						// if(!$fieldValue && $field['default_time_zone']){
						// 	$fieldValue = trim($field['default_time_zone']);
						// }
							
						$user_timezone =  xprofile_get_field_data('Timezone',$user_id);
						if ($user_timezone) {
							$user_timezone = explode(' ', $user_timezone);
							$user_timezone = $user_timezone[0];
						}
						else $user_timezone = 'UTC';
					?>
					<?php foreach (\DateTimeZone::listIdentifiers() as $tz) :
							$current_tz = new \DateTimeZone($tz);
							$transition = $current_tz->getTransitions($dt->getTimestamp(), $dt->getTimestamp());
							$abbr = $transition[0]['abbr'];
							$is_selected = $user_timezone === trim($tz) ? ' selected="selected"' : '';
							?>
							<option <?= $is_selected ?> value="<?php echo $tz; ?>"><?php echo $tz . ' (' . $abbr . ')'; ?></option>
					<?php endforeach; ?>
				</select>
		</div>
	</div>

	<div id="list-view">
		<div class="learndash_profile_heading no_radius clear_both">
			<span class="title">
				VR Classes List
			</span>
			<div class="actions alignright">
				<!-- <a href="#"><i href='#' class="fa fa-list "></i> View List</a> --><a href='#' id="vr_calendar-view-switch"> <i href='#' class="fa fa-calendar "></i></a> 
			</div>
		</div>

		<div class="vr_user_content_wrapper clear_both">
			<div class="vr_user_content_list">
				
				<?php if (isset($vr_user_classes_upcoming)) : ?>
					<h3>Upcoming Classes</h3>
					<?php foreach ($vr_user_classes_upcoming as $key => $vr_user_class) : ?>
						<?php require plugin_dir_path( dirname( __FILE__ ) ) . 'partials/immerse-lms-public-vr-user-classes-item.php';	?>
					<?php endforeach;?>
				<?php else : ?>
					No Upcoming Classes found
				<?php endif; ?>

				
				<?php if (isset($vr_user_classes_past)) : ?>
					<h3>Past Classes</h3>
					<?php foreach ($vr_user_classes_past as $key => $vr_user_class) : ?>
						<?php require plugin_dir_path( dirname( __FILE__ ) ) . 'partials/immerse-lms-public-vr-user-classes-item.php';	?>
					<?php endforeach;?>
				<?php else : ?>
					No Past Classes found
				<?php endif; ?>

			</div> 
		</div>
	</div>

</div>
