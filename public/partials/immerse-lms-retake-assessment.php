<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<?php
if (!$assessment_grading_id) {
    exit;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_FILES['trainer_signature'])) {
        $_POST['trainer_signature'] = $_FILES['trainer_signature'];
    }

    $assessment = update_assessment_grading($assessment_grading_id, $_POST);
} else {
    $assessment = get_assessment_grading($assessment_grading_id);
}

if (!$assessment['is_latest'] || $assessment['status'] != 'graded') {
    return;
}

function render_answer($assessment, $questionId, $question, $answer = null, $disabled = false) {
    switch ($question->getAnswerType()) {
        case 'multiple':
            ?>
            <div <?php if (!$disabled): ?>data-question-type="<?= $question->getAnswerType() ?>" data-question-id="<?= $questionId ?>"<?php endif ?>>
            <?php
            foreach ($question->getAnswerData() as $i => $a) {
                ?>
                <input type="checkbox" <?= $disabled ? 'disabled' : '' ?> <?= ($answer && $answer[$i]) ? 'checked' : '' ?> /> <?= $a->getAnswer() ?><br />
                <?php
            }
            ?>
            </div>
            <?php
            break;
        case 'single':
            ?>
            <div <?php if (!$disabled): ?>data-question-type="<?= $question->getAnswerType() ?>" data-question-id="<?= $questionId ?>"<?php endif ?>>
            <?php
            foreach ($question->getAnswerData() as $i => $a) {
                if ($disabled) {
                    if ($answer[$i]) {
                        ?>
                        <i class="fa fa-circle"></i>
                        <?php
                    } else {
                        ?>
                        <i class="fa fa-circle-o"></i>
                        <?php
                    }
                } else {
                    ?>
                    <input type="radio" name="<?= $question->getId() ?>" <?= ($answer && $answer[$i]) ? 'checked' : '' ?> />
                    <?php
                }
                ?>
                <?= $a->getAnswer() ?><br />
                <?php
            }
            ?>
            </div>
            <?php
            break;
        case 'matrix_sort_answer':
            ?>
            <div <?php if (!$disabled): ?>data-question-type="<?= $question->getAnswerType() ?>" data-question-id="<?= $questionId ?>" data-matrix-sort<?php endif ?>>
            <?php
            $view = new WpProQuiz_View_FrontQuiz();
            $quizMapper = new WpProQuiz_Model_QuizMapper();
            $categoryMapper = new WpProQuiz_Model_CategoryMapper();
            $questionMapper = new WpProQuiz_Model_QuestionMapper();

            $questionModels = $questionMapper->fetchAll( apply_filters( 'immerse_lms_get_quiz_pro_id', $assessment['quiz']->ID ) );
            $quiz = $quizMapper->fetch( apply_filters( 'immerse_lms_get_quiz_pro_id', $assessment['quiz']->ID ) );

            $view->quiz = $quiz;
            $view->question = $questionModels;
            $view->category = $categoryMapper->fetchByQuiz( $quiz->getId() );

            $question_count = count( $questionModels );

            ob_start();
            $quizData = $view->showQuizBox( $question_count );
            ob_get_clean();

            $json = $quizData['json'][$question->getId()];
            $correctAnswers = LD_QuizPro::datapos_array( $question->getId(), count( $json['correct'] ) );
            $answerData = $question->getAnswerData();

            if ($disabled) {
                ?>
                <table>
                    <tbody>
                        <?php
                        foreach ($answerData as $i => $a) {
                            ?>
                            <tr>
                                <td style="border-right: 1px solid rgba(0, 0, 0, 0.11);"><?= $a->getAnswer() ?></td>
                                <td style="width: 33%;"><?= $answerData[array_search($answer[$i], $correctAnswers)]->getSortString() ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <?php
            } else {
                ?>
                <table>
                    <tbody>
                        <?php
                        foreach ($answerData as $i => $a) {
                            ?>
                            <tr>
                                <td style="border-right: 1px solid rgba(0, 0, 0, 0.11);"><?= $a->getAnswer() ?></td>
                                <td style="width: 33%;" class="slot" data-question-id="<?= $questionId ?>">
                                    <div style="display: inline-block; box-sizing: border-box; padding: 8px; background-color: white; border: 1px solid rgb(100, 100, 100);" data-question-id="<?= $questionId ?>" data-hash="<?= $answer[$i] ?>">
                                        <?= $answerData[array_search($answer[$i], $correctAnswers)]->getSortString() ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <?php
            }
            ?>
            </div>
            <?php
            break;
        case 'essay':
            if ($disabled) {
                switch ($answer['type']) {
                    case 'upload':
                        ?><a href="<?= $answer['answer'] ?>" target="_blank">Download</a><?php
                        break;
                    case 'text':
                        echo $answer['answer'];
                        break;
                }
            } else {
                switch ($answer['type']) {
                    case 'upload':
                        ?>
                        <div class="assessment-file-upload">
                            <input type="hidden" data-question-type="<?= $answer['type'] ?>" data-question-id="<?= $questionId ?>" value="<?= esc_attr($answer['answer']) ?>" />
                            <input type="hidden" id="_uploadEssay_nonce_<?php echo $questionId; ?>" name="_uploadEssay_nonce" value="<?php echo wp_create_nonce('learndash-upload-essay-' . $questionId ); ?>" />
                            <input type="file" />
                            <button type="button" href="<?= $answer['answer'] ?>" class="button">Upload</a>
                        </div>
                        <?php
                        break;
                    case 'text':
                        ?>
                        <div data-question-type="<?= $answer['type'] ?>" data-question-id="<?= $questionId ?>">
                            <?php wp_editor($answer['answer'], 'question-' . $questionId); ?>
                        </div>
                        <?php
                        break;
                }
            }
            break;
    }
}
?>
<script>
    var QUIZ_ID = <?= json_encode(apply_filters( 'immerse_lms_get_quiz_pro_id', $assessment['quiz']->ID )) ?>;
    var QUIZ_POST_ID = <?= json_encode($assessment['quiz']->ID) ?>;
    var QUIZ_NONCE = <?= json_encode(wp_create_nonce( 'sfwd-quiz-nonce-' . $assessment['quiz']->ID . '-'. apply_filters( 'immerse_lms_get_quiz_pro_id', $assessment['quiz']->ID ) .'-' . get_current_user_id())) ?>;
    var START_DATE = Date.now();
</script>
<form action="" method="POST" enctype="multipart/form-data">
    <div style="width: 100%;">
        <div style="float: left; width: 33%;"><h3><?= $assessment['course']->post_title ?></h3></div>
        <div style="float: left; width: 33%;"><h3><?= $assessment['quiz']->post_title ?></h3></div>
    </div>
    <?php foreach ($assessment['answers'] as $i => $answer) : ?>
        <table>
            <thead>
                <tr>
                    <th colspan="2">
                        <?= $answer['question']->getTitle(); ?></b><br />
                    </th>
                </tr>
                <tr>
                    <th colspan="2">
                        <?= $answer['question']->getQuestion(); ?>
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr class="old-answer">
                    <td colspan="2">
                        <h4>Original Answer:</h4>
                        <?php
                        render_answer($assessment, '', $answer['question'], $answer['answer'], true);
                        ?>
                    </td>
                </tr>
                <tr class="new-answer" style="display: none;">
                    <td colspan="2">
                        <h4>New Answer:</h4>
                        <?php
                        render_answer($assessment, $answer['question']->getId(), $answer['question'], $answer['answer']);
                        ?>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%;">
                        <?= $answer['mark'] == 'correct' ? '<span style="color: green;">Correct</span>' : '<span style="color: red;">Incorrect</span>' ?>
                    </td>
                    <td style="width: 50%; text-align: right;">
                        <?php if ($answer['mark'] == 'incorrect'): ?>
                            <button type="button" class="button" onclick="jQuery(this).parent().parent().parent().find('.new-answer').show(); jQuery(this).remove();">Edit Answer</button>
                        <?php endif ?>
                    </td>
                </tr>
            </tbody>
        </table>
    <?php endforeach ?>
    <div style="float: left; width: 100%;">
        <button type="button" style="float: right; width: 150px !important;" class="button" id="submitQuiz">
            Submit
        </button>
    </div>
</form>