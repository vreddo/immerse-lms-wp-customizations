<?php
global $wpdb;
$wpdb->statistic_ref = $wpdb->prefix . 'wp_pro_quiz_statistic_ref';
$wpdb->statistic = $wpdb->prefix . 'wp_pro_quiz_statistic';

echo "Finding submitted essays with inconsistencies...\n";

$user_quizzes = $wpdb->get_results("
SELECT
    user_id,
    meta_value
FROM
    $wpdb->usermeta usermeta
WHERE
    meta_key = '_sfwd-quizzes'
    AND user_id = 468
");
$essays_stored = array();

foreach ($user_quizzes as $data) {
    $user = $data->user_id;
    echo "Checking user $user...\n";
    $original = $data->meta_value;
    $quizzes = unserialize($data->meta_value);

    foreach ($quizzes as $quiz) {
        if (isset($quiz['graded']) && is_array($quiz['graded'])) {
            $sref = $quiz['statistic_ref_id'];

            echo "Checking statistic $sref...\n";
            $stats = $wpdb->get_row($wpdb->prepare("
            SELECT
                COALESCE(SUM(correct_count), 0) AS total_correct,
                COALESCE(SUM(incorrect_count), 0) AS total_incorrect,
                COALESCE(SUM(points), 0) AS total_points
            FROM
                $wpdb->statistic
            WHERE
                answer_data NOT LIKE '{\"graded_id\":%' AND
                statistic_ref_id = %d
            ", $sref));

            $score = $stats->total_correct;
            $count = $stats->total_correct + $stats->total_incorrect;
            $points = $stats->total_points;

            foreach ($quiz['graded'] as $val) {
                $points += $val['points_awarded'];

                if ($val['points_awarded'] > 0) {
                    $score++;
                }

                $count++;
            }

            $pass = $points == $quiz['total_points'];
            $quiz['score'] = $score;
            $quiz['count'] = $count;
            $quiz['points'] = $points;
        }
    }

    $new_quizzes = serialize($quizzes);
}

die();
?>
<?php
$essays = $wpdb->get_results("
SELECT
    stat.*
FROM
    $wpdb->posts post
INNER JOIN
    $wpdb->statistic stat ON
        stat.answer_data LIKE CONCAT('{\"graded_id\":', post.ID, '}')
WHERE
    post.post_type = 'sfwd-essays'
LIMIT 10
");

print_r($essays);
?>