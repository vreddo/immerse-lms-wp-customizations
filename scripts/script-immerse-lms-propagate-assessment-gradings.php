<?php
global $wpdb;

// Retrieve statistic ref IDs linked to assessments.
$wpdb->statistic_ref = $wpdb->prefix . 'wp_pro_quiz_statistic_ref';

echo 'Retrieving statistic ref IDs of assessments without grading sheets...<br />';

$ids = $wpdb->get_col("
SELECT
    statistic_ref_id
FROM
    $wpdb->statistic_ref sf
INNER JOIN
    $wpdb->postmeta quiz_post_meta
    ON
        quiz_post_meta.meta_value = sf.quiz_id AND
        quiz_post_meta.meta_key = 'quiz_pro_id'
INNER JOIN
    $wpdb->posts quiz_post
    ON
        quiz_post.ID = quiz_post_meta.post_id
LEFT JOIN $wpdb->term_relationships tr ON (quiz_post.id = tr.object_id)
LEFT JOIN $wpdb->term_taxonomy tt ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
INNER JOIN $wpdb->terms t ON (tt.term_id = t.term_id AND t.slug = 'assessments')
LEFT JOIN
    $wpdb->postmeta statistic_ref_meta
    ON
        statistic_ref_meta.meta_value = sf.statistic_ref_id AND
        statistic_ref_meta.meta_key = '_statistic-ref-id'
WHERE
    statistic_ref_meta.meta_value IS NULL
ORDER BY sf.create_time DESC
");

if (count($ids) > 0) {
    echo 'Found ' . count($ids) . '<br />';
    echo 'Creating grading sheets...<br />';

    foreach ($ids as $id) {
        echo 'Creating sheet for statistic ref id ' . $id . '... ';
        $assessment_grading = create_assessment_grading($id);

        if (is_wp_error($assessment_grading_id)) {
            echo 'Error: ' . $assessment_grading_id->get_error_message() . '<br />';
        } else {
            echo 'Successful: <a href="/assessment-grading-sheet/?gid=' . $assessment_grading['grading']->ID . '">Link</a><br />';
        }
    }
} else {
    echo 'None found.';
}