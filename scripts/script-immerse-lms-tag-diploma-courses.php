<?php
echo "Finding diploma courses...<br />\n";

$courses = get_posts(array(
    'post_type' => 'sfwd-courses',
    'numberposts' => -1,
    's' => 'diploma of'
));

foreach ($courses as $course) {
    echo "Found \"$course->post_title\". Tagging... ";

    try {
        wp_set_object_terms($course->ID, array('diploma-course'), 'category', true);
        echo "Done.";
    } catch (\Exception $e) {
        echo $e->getMessage();
    }

    echo "<br />";
}