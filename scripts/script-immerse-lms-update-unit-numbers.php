<?php
$lessons = get_posts(array(
    'post_type' => 'sfwd-lessons',
    'numberposts' => -1
));

foreach ($lessons as $lesson) {
    $unit_number = get_field('unit_number', $lesson->ID);

    if (!$unit_number) {
        $parsed = explode(' ', $lesson->post_title)[0];
        $message = "$lesson->post_title ($lesson->ID) has no unit number! ";
        $matches = preg_match('/^[A-Z]+\d+/', $parsed);

        if ($matches) {
            $message .= "Setting it to " . $parsed . "...";
            update_field('unit_number', $parsed, $lesson->ID);
        } else {
            $message .= "$parsed doesn't seem to work as a proper unit number.";
        }

        echo $message . "\n";
    }
}